//
//  CometChatViewController.h
//  CLF
//
//  Created by Puneeth Uchil on 2/3/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

#ifndef CometChatViewController_h
#define CometChatViewController_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CometChatProtocol
- (void)cometChatCallback;
@end


@interface CometChatViewController : UIViewController <CometChatProtocol>

@property (strong, nonatomic) id someProperty;
@property (weak,nonatomic) id delegate;

+ (id)sharedInstance;
- (void) initCometChat;
- (void)login:(NSString*)username password:(NSString*)password;
- (void)loginwithUserID:(NSString*)userID;
- (void)launchChat;
- (void)logout;
@end





#endif /* CometChatViewController_h */
