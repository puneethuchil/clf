//
//  CometChatViewController.m
//  CLF
//
//  Created by Puneeth Uchil on 2/3/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MessageSDKFramework/CometChat.h"
#import "cometchat-ui/readyUIFile.h"
#import "CometChatViewController.h"

@implementation CometChatViewController {
    CometChat *cometchat;
    readyUIFIle *readyUI;
}

//static let sharedInstance: LoadingView = {
//    let instance = LoadingView.instanceFromNib()
//    // setup code
//    return instance
//}()

+ (id)sharedInstance {
    static CometChatViewController *sharedinstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedinstance = [[self alloc] init];
    });
    return sharedinstance;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"ChatLogin"];
}

- (void)initCometChat {
    
    cometchat = [[CometChat alloc]init];
    
    [cometchat checkCometChatUrl:@"https://clfchat.platinumclubnet.com/cometchat" success:^(NSDictionary *response) {
        
        NSLog(@"checkCometChatUrl Success: %@",response);
        
        [cometchat initializeCometChat:[response objectForKey:@"cometchat_url"] licenseKey:@"7UMRJ-1NIPU-HOB3A-W3XPU-NXKH7" apikey:@"f76e7e423cda8d4884f73d15fecb22f8" isCometOnDemand:NO success:^(NSDictionary *response) {
            NSLog(@"initializeCometChat Success %@",response);

        } failure:^(NSError *error) {
            NSLog(@"initializeCometChat Error %@",error);
        }];
        
        [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"cometchat_url"] forKey:@"Website_url"];
        
        
    } failure:^(NSError *error) {
        NSLog(@"checkCometChatUrl Error %@",error);
    }];
}

- (void)login:(NSString*)username password:(NSString*)password
{    
    cometchat = [[CometChat alloc]init];
    
    [cometchat checkCometChatUrl:@"https://clfchat.platinumclubnet.com/cometchat" success:^(NSDictionary *response) {
        
        NSLog(@"checkCometChatUrl Success: %@",response);
        
        [cometchat initializeCometChat:[response objectForKey:@"cometchat_url"] licenseKey:@"7UMRJ-1NIPU-HOB3A-W3XPU-NXKH7" apikey:@"f76e7e423cda8d4884f73d15fecb22f8" isCometOnDemand:NO success:^(NSDictionary *response) {
            NSLog(@"initializeCometChat Success %@",response);
            
//            [cometchat login:@"1" success:^(NSDictionary *response) {
//                NSLog(@"Success login %@", response);
//            } failure:^(NSError *error) {
//                NSLog(@"Error data %@",error );
//            }];
            
            [cometchat login:username password:password success:^(NSDictionary *response) {
                NSLog(@"Success login %@", response);

            } failure:^(NSError *error) {
                NSLog(@"Error data %@",error );
            }];
            
        } failure:^(NSError *error) {
            NSLog(@"initializeCometChat Error %@",error);
        }];
        
        [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"cometchat_url"] forKey:@"Website_url"];
        
        
    } failure:^(NSError *error) {
        NSLog(@"checkCometChatUrl Error %@",error);
    }];
    
}

- (void)loginwithUserID:(NSString*)userID
{
    [cometchat login:userID success:^(NSDictionary *response) {
        NSLog(@"Login Successful %@",response);

    } failure:^(NSError *error) {
        NSLog(@"Login Error %@",error);
    }];
}

- (void)launchChat
{
    NSLog(@"launchCometChat");
    readyUI = [[readyUIFIle alloc]init];

    [readyUI launchCometChat:YES observer:self userInfo:^(NSDictionary *response) {
        NSLog(@"launchCometChat : userInfo %@",response);
        [self.delegate cometChatCallback];
    } groupInfo:^(NSDictionary *response) {
        NSLog(@"launchCometChat : groupInfo %@",response);
        [self.delegate cometChatCallback];
    } onMessageReceive:^(NSDictionary *response) {
        NSLog(@"launchCometChat : onMessageReceive %@",response);
        [self.delegate cometChatCallback];
    } success:^(NSDictionary *response) {
        NSLog(@"launchCometChat : success %@",response);
        [self.delegate cometChatCallback];
    } failure:^(NSError *error) {
        NSLog(@"launchCometChat : failure %@",error);
//        NSDictionary *response = @{ @"error" : @"value"};
        [self.delegate cometChatCallback];
    } onLogout:^(NSDictionary *response) {
        NSLog(@"launchCometChat : onLogout %@",response);
        [self.delegate cometChatCallback];
    }];
}

- (void)logout
{
    [cometchat logout:^(NSDictionary *response) {
        NSLog(@"CometChat logout : Success %@",response);
    } failure:^(NSError *error) {
        NSLog(@"CometChat logout: failure %@",error);

    }];
}

@end


