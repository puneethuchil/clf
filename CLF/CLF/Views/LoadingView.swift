//
//  LoadingView.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loadingImage: UIImageView!

    class func instanceFromNib() -> LoadingView {
        return UINib(nibName: "LoadingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LoadingView
    }
    
    static let sharedInstance: LoadingView = {
        let instance = LoadingView.instanceFromNib()
        // setup code
        return instance
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func displayonview(view:UIView, show:Bool){
        self.frame=CGRect(x: 0, y: 0, width: 100, height: 120)
        self.center = view.center
        view.addSubview(self)
        view.bringSubview(toFront: self)
        
        if show == true {
            self.alpha=0.0
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 0.5, 0.5, 0.5)
            UIView.animate(withDuration: 0.25, animations: {
                self.layer.transform = CATransform3DIdentity
                self.alpha=1.0
                let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
                rotateAnimation.fromValue = 0.0
                rotateAnimation.toValue = CGFloat(Double.pi * 2)
                rotateAnimation.isRemovedOnCompletion = false
                rotateAnimation.duration = 2.0
                rotateAnimation.repeatCount=Float.infinity
                self.loadingImage.layer.add(rotateAnimation, forKey: nil)
            })
        }
        else {
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha=0.0
            }) { (finished) in
                self.removeFromSuperview()
                self.loadingImage.layer.removeAllAnimations()
            }
        }
    }
    
    func display(show:Bool){
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        self.frame=CGRect(x: 0, y: 0, width: 100, height: 120)
        self.center = keyWindow.center
        keyWindow.addSubview(self)
        keyWindow.bringSubview(toFront: self)
        
        if show == true {
            self.alpha=0.0
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 0.5, 0.5, 0.5)
            UIView.animate(withDuration: 0.25, animations: {
                self.layer.transform = CATransform3DIdentity
                self.alpha=1.0
                let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
                rotateAnimation.fromValue = 0.0
                rotateAnimation.toValue = CGFloat(Double.pi * 2)
                rotateAnimation.isRemovedOnCompletion = false
                rotateAnimation.duration = 2.0
                rotateAnimation.repeatCount=Float.infinity
                self.loadingImage.layer.add(rotateAnimation, forKey: nil)
            })
        }
        else {
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha=0.0
            }) { (finished) in
                self.removeFromSuperview()
                self.loadingImage.layer.removeAllAnimations()
            }
        }
    }
    
}

