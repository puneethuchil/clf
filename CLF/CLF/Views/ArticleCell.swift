//
//  ArticleCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

protocol articleDelegate {
    func articleSelected(index:Int)
}

class ArticleCell: UITableViewCell{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl : UIPageControl!
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var counter:Int = 0
    var totalDeals:Int = 0
    var delegate:articleDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Timer.scheduledTimer(timeInterval: 3,
                             target: self,
                             selector: #selector(self.scrollToPage),
                             userInfo: nil,
                             repeats: true)
        
    }
    
    func configure(articles:NSArray){
        self.totalDeals = articles.count
        self.configureScrollview(articles:articles )
        self.configurePageControl(pageCount:articles.count )
    }
    
    
    func configurePageControl(pageCount:Int) {
        // The total number of pages that are available is based on how many available deals we have.
        self.pageControl.numberOfPages = pageCount
        self.pageControl.currentPage = 0
    }
    
    func configureScrollview(articles:NSArray){
        self.scrollView.isPagingEnabled = true
        
        for index in 0..<articles.count {
            var mFrame = self.bounds
            mFrame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
            mFrame.size = self.scrollView.frame.size
            let subView = ArticleCardView.instanceFromNib()
            subView.frame=mFrame
            let mObject:Article = articles[index] as! Article
            subView.cellImage.loadImage(urlString: self.imageurl+mObject.image)
            subView.titleLabel.text = mObject.name + " - By -" + mObject.author
            subView.subTitleLabel.text = mObject.designation + ", " +  mObject.clubName
            subView.showDetailButton.tag = index
            subView.showDetailButton.addTarget(self, action: #selector(self.showDetails), for: UIControlEvents.touchUpInside)
            subView.setUpStyles()
            self.scrollView .addSubview(subView)
        }
        
        self.scrollView.contentSize = CGSize(width:(self.scrollView.frame.size.width * CGFloat.init(articles.count)),height: self.scrollView.frame.size.height)
    }
    
    @objc  func scrollToPage(page: Int) {
        self.counter += 1
        self.counter = (self.counter>self.totalDeals) ? 0 : self.counter
        
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(self.counter);
        frame.origin.y = 0;
        if self.counter == 0{
            self.scrollView.scrollRectToVisible(frame, animated: false)
        }
        else{
            self.scrollView.scrollRectToVisible(frame, animated: true)
        }
    }
    
    @IBAction func showDetails(_ sender: UIButton) {
        self.delegate?.articleSelected(index: sender.tag)
    }
    
}

