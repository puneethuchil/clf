//
//  SegmentCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

protocol SegmentDelegate {
    func segmentSelected(index:Int)
}

class SegmentCell: UITableViewCell{
    @IBOutlet weak var segment: UISegmentedControl!
    var delegate:SegmentDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpStyles(){
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        self.delegate?.segmentSelected(index: sender.selectedSegmentIndex)
    }
}
