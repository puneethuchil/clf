//
//  JobToggleCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class JobToggleCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var jobSwitch: UISwitch!

    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
    }
}

