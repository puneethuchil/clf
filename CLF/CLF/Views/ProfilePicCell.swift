//
//  ProfilePicCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
import UIKit

class ProfilePicCell: UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var clubLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profileImage.roundedCorner()
        
    }
    
    func setUpStyles(){
        if self.nameLabel != nil {
            self.nameLabel.titleLabel()
        }
        if self.designationLabel != nil {
            self.designationLabel.subTitleLabel()
        }
        if self.clubLabel != nil {
            self.clubLabel.subTitle1Label()
            self.clubLabel.text = self.clubLabel.text?.uppercased();
        }
        
        if self.profileImage != nil {
            self.profileImage.shadow()
        }
    }
}

