//
//  TextViewCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/1/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell{
    @IBOutlet weak var descriptionText: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
