//
//  TopClubCategoryCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/19/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class TopClubCategoryCell: UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    func setUpStyles(){
        if self.nameLabel != nil {
            self.nameLabel.titleLabel()
        }
        if self.bgView != nil {
            self.bgView.shadow()
        }
    }
}
