//
//  ButtonCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell{
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var actionButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        
        if self.bgview != nil {
            self.bgview.shadow()
        }
    }
}

