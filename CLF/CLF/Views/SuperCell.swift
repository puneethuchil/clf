//
//  SuperCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/1/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class SuperCell: UITableViewCell{
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var subTitle1Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        if self.subTitleLabel != nil {
            self.subTitleLabel.subTitleLabel()
        }
        if self.subTitle1Label != nil {
            self.subTitle1Label.subTitle1Label()
        }
    }
}

class SuperCollectionCell: UICollectionViewCell{
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var subTitle1Label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        if self.subTitleLabel != nil {
            self.subTitleLabel.subTitleLabel()
        }
        if self.subTitle1Label != nil {
            self.subTitle1Label.subTitle1Label()
        }
    }

    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        if self.subTitleLabel != nil {
            self.subTitleLabel.subTitleLabel()
        }
        if self.subTitle1Label != nil {
            self.subTitle1Label.subTitle1Label()
        }
    }

}
