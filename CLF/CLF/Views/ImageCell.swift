//
//  ImageCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell{
    @IBOutlet weak var imageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setUpStyles(){
        if self.imageview != nil {
            self.imageview.shadow()
        }
    }
}

