//
//  TopClubCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/19/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class TopClubCell: UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var bgView: UIView!

    func setUpStyles(){
        if self.nameLabel != nil {
            self.nameLabel.titleLabel()
        }
        if self.locationLabel != nil {
            self.locationLabel.subTitle1Label()
        }
        if self.pointsLabel != nil {
            self.pointsLabel.subTitle1Label()
        }
        if self.rankLabel != nil {
            self.rankLabel.titleLabel()
            self.rankLabel.backgroundColor = AppColor.ButtonColor.actionButton
        }
        if self.bgView != nil {
            self.bgView.shadow()
        }
    }
}

