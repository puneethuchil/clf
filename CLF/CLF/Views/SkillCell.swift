//
//  SkillCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class SkillCell: UICollectionViewCell{
    @IBOutlet weak var skillName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.skillName.border()
        self.skillName.radius()
    }
}

