//
//  ServicesCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/23/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ServicesCell: SuperCell{
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.viewBorder()
        bgView.shadow()
    }
}
