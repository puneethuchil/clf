//
//  ServicesDetailsCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/23/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ServicesDetailsCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var descWebView: UIWebView!
    @IBOutlet weak var priceCollection: UICollectionView!


    func loadHTML(htmlString:String){
        let updatedString = "<style>* {font-weight: 500;font-family: 'PingFang SC';font-size: 12px;color: rgb(150, 150, 150)}</style>"+htmlString
        descWebView.loadHTMLString(updatedString, baseURL: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        if self.buyButton != nil {
            self.buyButton.setStyleBlue()
        }
    }
    
    @IBAction func toggle(_ sender: UIButton) {
    }
}



