//
//  CommentCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/25/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell{
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
