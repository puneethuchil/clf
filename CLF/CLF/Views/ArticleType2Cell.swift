//
//  ArticleType2Cell.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/7/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class ArticleType2Cell: SuperCollectionCell{
    @IBOutlet weak var articleTypeLabel: UILabel!
}
