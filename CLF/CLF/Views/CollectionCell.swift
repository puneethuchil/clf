//
//  CollectionCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class CollectionCell: UITableViewCell{
    @IBOutlet weak var collection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
