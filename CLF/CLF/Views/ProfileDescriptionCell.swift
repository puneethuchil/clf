//
//  ProfileDescriptionCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ProfileDescriptionCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        if self.descriptionLabel != nil {
            self.descriptionLabel.subTitle1Label()
        }
    }
}


