//
//  ProductCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/25/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ProductCell: SuperCollectionCell{
    @IBOutlet weak var interest: UIButton!
    @IBOutlet weak var descriptionWebView: UIWebView!
    @IBOutlet weak var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.shadow()
    }
    
    func loadHTML(htmlString:String){
        let updatedString = "<style>* {font-weight: 500;font-family: 'PingFang SC';font-size: 12px;color: rgb(150, 150, 150)}</style>"+htmlString
        descriptionWebView.loadHTMLString(updatedString, baseURL: nil)
    }
    
}

