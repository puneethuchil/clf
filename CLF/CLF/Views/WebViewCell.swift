//
//  WebViewCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class WebViewCell: UITableViewCell{
    @IBOutlet weak var webView: UIWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func loadHTML(htmlString:String){
        let updatedString = "<style>* {font-weight: 500;font-family: 'PingFang SC';font-size: 12px;color: rgb(150, 150, 150)}</style>"+htmlString
        webView.loadHTMLString(updatedString, baseURL: nil)
    }
    
    func setUpStyles(){
    }
}
