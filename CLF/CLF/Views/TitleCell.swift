//
//  TitleCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func setUpStyles(){
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
    }
}
