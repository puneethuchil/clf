//
//  SummaryCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/15/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell{
    @IBOutlet weak var placedLabel: UILabel!
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var shippingAddress: UITextView!
    @IBOutlet weak var placedTitleLabel: UILabel!
    @IBOutlet weak var orderIDTitleLabel: UILabel!
    @IBOutlet weak var shippingAddressTitle: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpStyles(){
        if self.placedLabel != nil {
            self.placedLabel.subTitle1Label()
        }
        if self.orderIDLabel != nil {
            self.orderIDLabel.subTitle1Label()
        }
        if self.shippingAddress != nil {
            self.shippingAddress.setUpStyles()
        }
        if self.placedTitleLabel != nil {
            self.placedTitleLabel.titleLabel()
        }
        if self.orderIDTitleLabel != nil {
            self.orderIDTitleLabel.titleLabel()
        }
        if self.shippingAddressTitle != nil {
            self.shippingAddressTitle.titleLabel()
        }
    }
}
