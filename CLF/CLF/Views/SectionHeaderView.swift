//
//  SectionHeaderView.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/1/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

protocol SectionHeaderViewDelegate {
    func showMore(index:NSInteger)
}

class SectionHeaderView: UIView {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var more: UIButton!
    var delegate:SectionHeaderViewDelegate!
    
    @IBAction func viewMore(_ sender: UIButton) {
        if self.delegate != nil{
            self.delegate.showMore(index: sender.tag)
        }
    }
    
    
    class func instanceFromNib() -> SectionHeaderView {
        return UINib(nibName: "SectionHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SectionHeaderView
    }
}
