//
//  ConversationCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/4/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class ConversationCell: SuperCell{
    @IBOutlet weak var discussionTitle: UILabel!
    @IBOutlet weak var discussionDescription: UITextView!
    @IBOutlet weak var commentsButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        if self.discussionTitle != nil {
            self.discussionTitle.titleLabel()
        }
        if self.discussionDescription != nil {
        }
    }
    
    override func setUpStyles(){
        super.setUpStyles()

        if self.discussionTitle != nil {
            self.discussionTitle.titleLabel()
        }
        if self.discussionDescription != nil {
            self.discussionDescription.setUpStyles()
        }
    }
    
}
