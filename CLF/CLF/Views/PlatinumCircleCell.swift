//
//  PlatinumCircleCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class PlatinumCircleCell: UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var logoImageview: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    func setUpStyles(){
        if self.nameLabel != nil {
            self.nameLabel.titleLabel()
        }
        if self.linkLabel != nil {
            self.linkLabel.subTitle1Label()
        }
        if self.bgView != nil {
            self.bgView.shadow()
        }
    }
}
