//
//  SubtitleCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class SubtitleCell: UITableViewCell{
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setUpStyles(){
        if self.subTitleLabel != nil {
            self.subTitleLabel.subTitleLabel()
        }
    }
}

