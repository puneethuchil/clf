//
//  ArticleCardView.swift
//  CLF
//
//  Created by Puneeth Uchil on 12/14/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class ArticleCardView: UIView {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var showDetailButton: UIButton!

    class func instanceFromNib() -> ArticleCardView {
        return UINib(nibName: "ArticleCardView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ArticleCardView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpStyles(){
        super.awakeFromNib()
        if self.titleLabel != nil {
            self.titleLabel.titleLabel()
        }
        if self.subTitleLabel != nil {
            self.subTitleLabel.subTitleLabel()
        }
    }

    
    
    func updateInfo(){
  }
}
