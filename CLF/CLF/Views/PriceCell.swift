//
//  PriceCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation

class PriceCell: UICollectionViewCell{
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override var isSelected: Bool {
        didSet {
            self.tickImage.image = self.isSelected == true ? UIImage.init(named: "Tick") : UIImage.init(named: "unTick")
        }
    }
}
