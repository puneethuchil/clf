//
//  ToastView.swift
//  CLF
//
//  Created by Puneeth Uchil on 11/24/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class ToastView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!

    class func instanceFromNib() -> ToastView {
        return UINib(nibName: "ToastView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ToastView
    }
    
    static let sharedInstance: ToastView = {
        let instance = ToastView.instanceFromNib()
        // setup code
        return instance
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.viewBorder()
        bgView.shadow()
    }
    
    func showAlert(message:String){
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        self.titleLabel.text = message
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 82)
            keyWindow.addSubview(self)
            keyWindow.bringSubview(toFront: self)
        
            self.alpha=0.0
            self.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, -82, 0)
            UIView.animate(withDuration: 0.25, animations: {
                self.layer.transform = CATransform3DIdentity
                self.alpha=1.0
            })
        
        _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
    }
    
   @objc func hide(){
    UIView.animate(withDuration: 0.25, animations: {
        self.alpha=0.0
    }) { (finished) in
        self.removeFromSuperview()
    }
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.hide()
    }
    

}
