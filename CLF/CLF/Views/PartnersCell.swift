//
//  PartnersCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class PartnersCell: SuperCollectionCell{
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.viewBorder()
        bgView.shadow()
    }
}
