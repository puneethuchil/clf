//
//  NoDataView.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/16/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class NoDataView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    class func instanceFromNib() -> NoDataView {
        return UINib(nibName: "NoDataView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NoDataView
    }
    
    static let sharedInstance: NoDataView = {
        let instance = NoDataView.instanceFromNib()
        // setup code
        return instance
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func display(show:Bool){
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        self.center = keyWindow.center
        keyWindow.addSubview(self)
        keyWindow.bringSubview(toFront: self)
        
        if show == true {
            self.alpha=0.0
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 0.5, 0.5, 0.5)
            UIView.animate(withDuration: 0.25, animations: {
                self.layer.transform = CATransform3DIdentity
                self.alpha=1.0
            })
        }
        else {
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha=0.0
            }) { (finished) in
                self.removeFromSuperview()
            }
        }
    }

}
