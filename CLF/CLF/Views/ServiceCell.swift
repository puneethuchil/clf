//
//  ServiceCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class ServiceCell: SuperCell{
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.viewBorder()
        self.bgView.shadow()
    }
}
