//
//  SearchJobCell.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/13/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation

class SearchJobCell: UITableViewCell{
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var searchButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        searchText.setUpStyles()
        
    }
}
