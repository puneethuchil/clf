//
//  PDFListViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class PDFListViewController:UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var membersCollectionView: UICollectionView!
    var refreshControl: UIRefreshControl!
    
    var pdfArray:NSMutableArray = []
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var boardMembers:Boardmembers = Boardmembers.init(fromJson: [:])
    
    
    var boardMembersWebService = {
        return BoardMembersWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.membersCollectionView.addSubview(refreshControl)
        self.membersCollectionView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func itemTapped(_ sender: UIButton) {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pdfArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let mObject:PdfPath = self.pdfArray[indexPath.row] as! PdfPath
        
        let cell:ArticleType2Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleType2Cell", for: indexPath as IndexPath) as! ArticleType2Cell
//        cell.cellImage.loadImage(urlString: self.imageurl+mObject.image)
        cell.subTitleLabel.text = mObject.month+" - "+mObject.year
        cell.setUpStyles()
        cell.cellImage.dropShadow()
        cell.subTitleLabel.textColor = UIColor.gray
        return cell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //WebViewController
        let superVC:WebViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        superVC.loadViewIfNeeded()
        superVC.loadPDF(htmlString: "https://clubleadersforum.com/wp-content/uploads/2017/11/The-FORUM-Spring-2017.pdf")
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2-10, height: 250)
    }
    
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        refreshControl.endRefreshing()
    }
    
}
