//
//  PlatinumViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/15/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

extension PlatinumViewController:SectionHeaderViewDelegate{
    func showMore(index:NSInteger)
    {
    }
}

class PlatinumViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

    var eventsArray: NSArray = ["PlatinumEventCell1"]
    var sectionArray: NSArray = ["PLATINUMCLUBNET EVENTS","PLATINUMCLUBNET CLUBS"]
    var clubsArray: NSArray = ["PlatinumClubsCell1","PlatinumClubsCell2"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section {
        case 0:
            let cell:ServiceDetailCell = tableView.dequeueReusableCell(withIdentifier: "ServiceDetailCell", for: indexPath) as! ServiceDetailCell
            cell.cellImage.image = UIImage(named: eventsArray[indexPath.row] as! String)
            return cell
            
        case 1:
            let cell:JobCell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobCell
            cell.cellImage.image = UIImage(named: clubsArray[indexPath.row] as! String)
            
            return cell
        default:
            let cell:EventsCell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as! EventsCell
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        switch section {
        case 0:
            return eventsArray.count
        case 1:
            return clubsArray.count
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section {
        case 0:
            return 270
        case 1:
            return 71
        default:
            return 100
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = SectionHeaderView.instanceFromNib()
        headerView.delegate=self
        headerView.more.tag = section
        switch section {
        case 0:
            headerView.title.text="PLATINUM CLUB EVENTS"
        case 1:
            headerView.title.text="PLATINUMCLUB CLUBS"
        default:
            headerView.title.text="NEW"
        }
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        
        switch indexPath.section {
        case 0:
            superVC.bodyImage.image=UIImage(named: "PlatunumclubneteventsDetail")
        case 1:
            superVC.bodyImage.image=UIImage(named: "PlatinumclubnetClubDetails")
        default:
            superVC.bodyImage.image=UIImage(named: "UnderConstruction")
        }
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
}
