//
//  FormSubmissionViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/1/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class FormSubmissionViewController: UIViewController{
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var club: UITextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    var profileItem = Profile(fromJson: [:])
    var profileWebService = {
        return ProfileWebService()
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.name.text = profileItem.fName + " " + profileItem.lName
//        self.email.text = profileItem.email
//        self.club.text = profileItem.clubs
//        self.location.text = ""
//        self.phone.text = ""
//        self.message.text = ""
        self.setUpStyles()
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addTapView), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        profileWebService.fetchProfileInfo { (profileObject) in
            if let mObject = profileObject {
                self.profileItem = mObject
                DispatchQueue.main.async {
                }
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        self.view.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, CGFloat(textField.tag)*(-textField.frame.size.height), 0)

        switch textField {
        case name:
            email.becomeFirstResponder()
            break
        case email:
            club.becomeFirstResponder()
            break
        case club:
            location.becomeFirstResponder()
            break
        case location:
            phone.becomeFirstResponder()
            break
        case phone:
            message.becomeFirstResponder()
            break
        case message:
            textField.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        if notification.name == Notification.Name.UIKeyboardWillHide {
            self.view.layer.transform = CATransform3DIdentity
        }
    }
    
    @objc func addTapView(notification: Notification) {
        if notification.name == Notification.Name.UIKeyboardWillShow {
            self.view.addGestureRecognizer(UIGestureRecognizer.init(target: self, action: #selector(self.tapped)))
        }
    }
    
    func tapped(){
        
    }
    
    func setUpStyles(){
        self.name.setUpStyles()
        self.email.setUpStyles()
        self.club.setUpStyles()
        self.location.setUpStyles()
        self.phone.setUpStyles()
        self.message.setUpStyles()
        self.submitButton.setStyleBlue()
    }
    
    @IBAction func submit(_ sender: UIButton) {
        
    }
}
