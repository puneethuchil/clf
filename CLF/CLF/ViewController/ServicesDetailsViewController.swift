//
//  ServicesDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/15/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class ServicesDetailsViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var buyButton: UIButton!

    var servicesArray: NSMutableArray = []
    var descriptionHeightArray: NSMutableArray = []
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var serviceDetailDataItem = SubscriptionDetail.init(fromJson: [:])
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var serviceID = "0"
    var packegeType = 0
    var platinumServicesWebService = {
        return PlatinumServicesWebService()
    }()
    

    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        self.servicesArray.removeAllObjects()
        switch self.segment.selectedSegmentIndex {
        case 0:
            self.servicesArray.addObjects(from: self.serviceDetailDataItem.pCOW)
        case 1:
            self.servicesArray.addObjects(from: self.serviceDetailDataItem.pCOA)
        default:
            print("")
        }
        self.table.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        platinumServicesWebService.fetchPlatinumServicesDetail(serviceID: serviceID) { (platinumServices) in
            if let mObject = platinumServices {
                DispatchQueue.main.async{
                    self.serviceDetailDataItem = mObject
                    self.servicesArray.addObjects(from: self.serviceDetailDataItem.pCOW)
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let service:Service = self.servicesArray[indexPath.section] as! Service

        if indexPath.row == 0 {
            let cell:ServicesDetailsCell = tableView.dequeueReusableCell(withIdentifier: "ServicesDetailsCell", for: indexPath) as! ServicesDetailsCell
                cell.titleLabel.text = service.title
            cell.descWebView.delegate = self
            cell.descWebView.tag = indexPath.row
            cell.loadHTML(htmlString:service.descriptionField)
            cell.setUpStyles()
            
            return cell
        }
        else {
//            let price:PriceData = PriceData.init(fromJson: ["year":"1","price":"100"])
//            let price1:PriceData = PriceData.init(fromJson: ["year":"2","price":"200"])
//            let price2:PriceData = PriceData.init(fromJson: ["year":"3","price":"300"])
//            service.pricearray = NSArray.init(objects: price,price1,price2) as! [PriceData]

            let cell:ServiceDetail2Cell = tableView.dequeueReusableCell(withIdentifier: "ServiceDetail2Cell", for: indexPath) as! ServiceDetail2Cell
            cell.collection.delegate = self
            cell.collection.dataSource = self
            cell.collection.tag = indexPath.section
            cell.buyButton.tag = indexPath.section
            cell.buyButton.addTarget(self, action: #selector(self.chooseSubscription), for: UIControlEvents.touchUpInside)
            if service.pdfPath.count > 0 {
                cell.buyButton.setTitle("VIEW", for: UIControlState.normal)
            }
            else{
                cell.buyButton.setTitle("BUY", for: UIControlState.normal)
            }
            cell.buyButton.setStyleBlue()
            cell.collection.reloadData()
            return cell
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.servicesArray.count
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 {
            return CGFloat(65.0 + Float((self.servicesArray[indexPath.section] as! Service).webviewheight)!)}
        else{
            return CGFloat(50.0 + Float((self.servicesArray[indexPath.section] as! Service).collectionviewheight)!)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let service:Service = self.servicesArray[section] as! Service
        self.calculateCollectionViewHeight(index: section)
        return service.pricearray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let service:Service = self.servicesArray[indexPath.section] as! Service
        let price:PriceData = service.pricearray[indexPath.row]
        
        let cell:PriceCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PriceCell", for: indexPath as IndexPath) as! PriceCell
        cell.titleLabel.text = price.year+" YEAR : USD "+price.price
        if indexPath.row == self.packegeType {
            cell.isSelected = true
        }
        return cell
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        self.packegeType = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2-10, height: 50)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {

        LoadingView.sharedInstance.display(show: false)
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        
        if (self.servicesArray[webView.tag] as! Service).webviewheight == "70.0"{
            (self.servicesArray[webView.tag] as! Service).webviewheight = "\(webView.frame.size.height)"
            self.table.reloadData()
        }
    }

    
    @IBAction func chooseSubscription(_ sender: UIButton) {
        
        if isLoggedIn == false{
            let alert = UIAlertController(title: "Alert", message: "Please login to use this feature", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                { action in
                    switch action.style{
                    case .default:
                        let superVC:LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        superVC.hidesBottomBarWhenPushed = true
                        superVC.loadViewIfNeeded()
                        self.navigationController?.pushViewController(superVC, animated: true)
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ action in
            } ))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else {
            
            let service:Service = self.servicesArray[sender.tag] as! Service
            if service.pdfPath.count > 0 {
                let superVC:PDFListViewController = self.storyboard?.instantiateViewController(withIdentifier: "PDFListViewController") as! PDFListViewController
                superVC.pdfArray.addObjects(from: service.pdfPath)
                superVC.hidesBottomBarWhenPushed = true
                superVC.loadViewIfNeeded()
                self.navigationController?.pushViewController(superVC, animated: true)
            }
            else {
                let price:PriceData = service.pricearray[packegeType]
                let savedCard = UserDefaults.standard.bool(forKey: "savedCard") == true ? "0" : "1"
                
                let superVC:WebViewController = self.mainstoryboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                superVC.loadViewIfNeeded()
                
                if savedCard == "0" {
                    let alert = UIAlertController(title: "Alert", message: "Select the card", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Last Card", style: .default, handler: { action in
                        superVC.loadStripe(amount: price.price, email: currentUser.userEmail, orderId: service.id, payment: "CLF", newCard:"0", productType: "SERVICES")
                        self.navigationController?.pushViewController(superVC, animated: true)
                    }))
                    alert.addAction(UIAlertAction(title: "New Card", style: .default, handler: { action in
                        superVC.loadStripe(amount: price.price, email: currentUser.userEmail, orderId: service.id, payment: "CLF", newCard:"1", productType: "SERVICES")
                        self.navigationController?.pushViewController(superVC, animated: true)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    superVC.loadStripe(amount: price.price, email: currentUser.userEmail, orderId: service.id, payment: "CLF", newCard:"1", productType: "SERVICES")
                    self.navigationController?.pushViewController(superVC, animated: true)
                }
            }
        }
    }
    
    func calculateCollectionViewHeight(index:Int){
        let service:Service = self.servicesArray[index] as! Service
        let height = 50 * ((service.pricearray.count%2)+(service.pricearray.count/2))
        (self.servicesArray[index] as! Service).collectionviewheight = "\(height)"
    }
}




