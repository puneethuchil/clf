//
//  ProductDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/15/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

class ProductDetailsViewController:SuperDetailsViewController,UIWebViewDelegate
{
    var mItem:CommerceProduct = CommerceProduct(fromJson:[:])
    var pageType = "DETAIL"
    override func viewDidLoad() {
        super.viewDidLoad()
        if pageType == "DETAIL" {
            self.rowsArray = ["TitleCell","ImageCell","SubtitleCell","WebViewCell","ButtonCell"]
        }
        else if pageType == "SUMMERY" {
            self.rowsArray = ["TitleCell","ImageCell","SubtitleCell","WebViewCell","ButtonCell","SummaryCell"]
        }
        
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
            
        case "TitleCell":
            let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titleLabel.text = self.mItem.title
            cell.setUpStyles()
            return cell
            
        case "SubtitleCell":
            let cell:SubtitleCell = tableView.dequeueReusableCell(withIdentifier: "SubtitleCell", for: indexPath) as! SubtitleCell
            cell.subTitleLabel.text = "PRICE : "+self.mItem.price
            cell.setUpStyles()
            return cell
            
        case "ImageCell":
            let cell:ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as!ImageCell
            cell.imageview.loadImage(urlString: self.imageurl+self.mItem.image)
            return cell
            
        case "ButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.actionButton.addTarget(self, action: #selector(self.buy), for: UIControlEvents.touchUpInside)

            cell.setUpStyles()
            return cell
            
        case "WebViewCell":
            let cell:WebViewCell = tableView.dequeueReusableCell(withIdentifier: "WebViewCell", for: indexPath) as!WebViewCell
            cell.webView.delegate = self
            cell.loadHTML(htmlString: self.mItem.descriptionField)
            return cell
            
        case "SummaryCell":
            let cell:SummaryCell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as!SummaryCell
            return cell
            
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
    }
    
    func buy(){
        let alert = UIAlertController(title: "Alert", message: "Select the card", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Last Card", style: .default, handler: { action in
            let superVC:WebViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            superVC.loadViewIfNeeded()
            superVC.loadStripe(amount: self.mItem.price!, email: currentUser.userEmail, orderId: self.mItem.id!, payment: "CLF", newCard:"0", productType: "COMMERCE")
            self.navigationController?.pushViewController(superVC, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "New Card", style: .default, handler: { action in
            let superVC:WebViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            superVC.loadViewIfNeeded()
            superVC.loadStripe(amount: self.mItem.price!, email: currentUser.userEmail, orderId: self.mItem.id!, payment: "CLF", newCard:"1", productType: "COMMERCE")
            self.navigationController?.pushViewController(superVC, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: false)
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        
        if descriptionHeight == 0 {
            descriptionHeight = webView.frame.size.height + 20
            self.table.reloadData()
        }
    }
}

