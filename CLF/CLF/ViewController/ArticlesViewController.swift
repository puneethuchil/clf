//
//  ArticlesViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class ArticlesViewController: SuperViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var articleCollectionView: UICollectionView!
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var articles: NSMutableArray = []
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"

    var articleListWebService = {
        return ArticleListWebService()
    }()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let logo = UIImage(named: "TitleLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.articleCollectionView.addSubview(refreshControl)
        self.articleCollectionView.alwaysBounceVertical = true;

        articleListWebService.fetchArticleList { (articleListObject) in
            if let object = articleListObject{
                DispatchQueue.main.async {
                    self.updateData(object: object)
                }
                print(object)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let mObject:Article = self.articles[indexPath.row] as! Article

        if indexPath.row % 3==0 {
            let cell:ArticleType2Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleType2Cell", for: indexPath as IndexPath) as! ArticleType2Cell
            cell.cellImage.loadImage(urlString: self.imageurl+mObject.image)
            cell.titleLabel.text = mObject.name
            cell.subTitleLabel.text = mObject.createdAt
            cell.articleTypeLabel.text = mObject.typename.uppercased()
            cell.setUpStyles()
            return cell
        }
        else{
            let cell:ArticleType1Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleType1Cell", for: indexPath as IndexPath) as! ArticleType1Cell
            cell.cellImage.loadImage(urlString: self.imageurl+mObject.image)
            cell.titleLabel.text = mObject.name
            cell.subTitleLabel.text = mObject.createdAt
            cell.articleTypeLabel.text = mObject.typename.uppercased()
            cell.setUpStyles()
            return cell
        }

        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        let superVC:ArticlesDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ArticlesDetailsViewController") as! ArticlesDetailsViewController
        
        superVC.hidesBottomBarWhenPushed = true
        superVC.articleItem = self.articles[indexPath.row] as! Article
        superVC.imageurl = self.imageurl
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row % 3==0 {
            return CGSize(width: collectionView.frame.width, height: 232)
        }
        else{
            return CGSize(width: collectionView.frame.width/2-10, height: 239)
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshControl.endRefreshing()

        articleListWebService.fetchArticleList { (articleListObject) in
            if let object = articleListObject{
                DispatchQueue.main.async {
                    self.updateData(object: object)
                }
                print(object)
            }
        }
    }
    
    func updateData(object:ArticleList){
        self.articles.removeAllObjects()
        
        self.imageurl = object.imageUrl
        self.articles.addObjects(from: object.articles)
        refreshControl.endRefreshing()
        self.articleCollectionView.reloadData()
    }
}
