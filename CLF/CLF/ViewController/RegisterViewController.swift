//
//  RegisterViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/31/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class RegisterViewController:UIViewController , SearchDelegate//, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var firstnameText:UITextField!
    @IBOutlet weak var secondnameText:UITextField!
    @IBOutlet weak var designation  :UITextField!
    @IBOutlet weak var clubname:UITextField!
    @IBOutlet weak var clubcode:UITextField!
    @IBOutlet weak var username:UITextField!
    @IBOutlet weak var password:UITextField!

    @IBOutlet weak var registerButton:UIButton!
    @IBOutlet weak var termButton:UIButton!
    @IBOutlet weak var clubButton:UIButton!
    @IBOutlet weak var termsLabel:UILabel!
    @IBOutlet weak var clubSearchButton:UIButton!

    var cometChatWebService = {
        return CometChatWebService()
    }()
    
    var registerWebService = {
        return RegisterWebService()
    }()
    
    @IBAction func clubSearch(_ sender: UIButton) {
        let superVC:SearchViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        superVC.loadViewIfNeeded()
        superVC.delegate = self
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    //ForgotPasswordViewController
    @IBAction func term(_ sender: UIButton) {
        let superVC:WebViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        superVC.loadViewIfNeeded()
        superVC.loadTerms(url: "Test")
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    @IBAction func register(_ sender: UIButton) {
        
        if self.isEntriesValid() {
            registerWebService.register(username: username.text!, password: password.text!, fname: firstnameText.text!, lname: secondnameText.text!, clubname: clubname.text!, clubCode: clubcode.text!,designation:designation.text!) { (registerObject) in
                if let object = registerObject{
                    DispatchQueue.main.async{
                        print(object)
                        self.cometChatRegister()
                    }
                }
            }
        }
    }
    
    @IBAction func clubEmployee(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.clubcode.isEnabled = sender.isSelected == true ? true : false
        self.clubcode.alpha = clubcode.isEnabled == true ? 1.0 : 0.35
        self.designation.isEnabled = self.clubButton.isSelected == true ? true : false
        self.designation.alpha = clubcode.isEnabled == true ? 1.0 : 0.35
    }
    
    func cometChatRegister(){
        cometChatWebService.register(username: username.text!, password: password.text!,completion: { (success) -> Void in
            if success { // this will be equal to whatever value is set in this method call
                print("true")
                let alertTitle = "Title"
                let alertMessage = "Message"
                let alertOkButtonText = "Ok"
                
                let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
                //We add buttons to the alert controller by creating UIAlertActions:
                let actionOk = UIAlertAction(title: alertOkButtonText,
                                             style: .default,
                                             handler: nil) //You can use a block here to handle a press on this button
                
                alertController.addAction(actionOk)
            } else {
                print("false")
            }
        })
        
//        cometChatWebService.register(username: username.text!, password: password.text!) { userObject in
//            if let object = userObject{
//                DispatchQueue.main.async{
//                    print(object)
//                }
//            }
//            self.navigationController?.popViewController(animated: true)
//        }

    }
    
    func setStyle(){
        firstnameText.setUpStyles()
        secondnameText.setUpStyles()
        designation.setUpStyles()
        clubname.setUpStyles()
        clubcode.setUpStyles()
        username.setUpStyles()
        password.setUpStyles()
        
        registerButton.setStyleBlue()
        termButton.setUpStyles()
        clubButton.setUpStyles()
        termsLabel.subTitleLabel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyle()
        self.clubcode.isEnabled = self.clubButton.isSelected == true ? true : false
        self.clubcode.alpha = clubcode.isEnabled == true ? 1.0 : 0.35
        self.designation.isEnabled = self.clubButton.isSelected == true ? true : false
        self.designation.alpha = clubcode.isEnabled == true ? 1.0 : 0.35
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillShow, object: nil)

        
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        self.view.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, CGFloat(textField.tag)*(-textField.frame.size.height), 0)

        switch textField {
        case firstnameText:
            secondnameText.becomeFirstResponder()
            break
        case secondnameText:
            designation.becomeFirstResponder()
            break
        case designation:
            clubname.becomeFirstResponder()
            break
        case clubname:
            username.becomeFirstResponder()
            break
        case username:
            password.becomeFirstResponder()
            break
        case password:
            textField.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            self.view.layer.transform = CATransform3DIdentity
        }
    }
    
    func isEntriesValid() -> Bool{
        if self.username.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Email address is empty!")
            return false
        }
        if !self.username.isEmailValid(){
            ToastView.sharedInstance.showAlert(message: "Invalid email ID!")
            return false
        }
        if self.password.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Password is empty!")
            return false
        }
        if self.firstnameText.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "First name is empty!")
            return false
        }
        if self.secondnameText.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Second name is empty!")
            return false
        }
        if self.clubname.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Clubname is empty!")
            return false
        }
        if self.designation.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Designation is empty!")
            return false
        }
        
        return true
    }
    
    func itemSelected(item:String)
    {
        self.clubname.text = item
    }
}

