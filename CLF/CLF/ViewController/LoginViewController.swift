//
//  LoginViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/31/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class LoginViewController:UIViewController//, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var nameText:UITextField!
    @IBOutlet weak var passwordText:UITextField!
    @IBOutlet weak var loginButton:UIButton!
    @IBOutlet weak var registerButton:UIButton!
    @IBOutlet weak var rememberButton:UIButton!
    @IBOutlet weak var forgotButton:UIButton!
    @IBOutlet weak var registerLabel:UILabel!
    
    var loginWebService = {
        return LoginWebService()
    }()

    @IBAction func login(_ sender: UIButton) {
        if self.isEntriesValid(){
            loginWebService.performLogin(username: nameText.text!, password: passwordText.text!) { (userObject) in
                if let object = userObject{
                    DispatchQueue.main.async{
                        currentUser = object
                        headers = ["Content-Type":"application/json",
                                    "compatibility-version":"1",
                                    "authorization":"Bearer "+currentUser.accessToken,
                                    "server-ts":currentUser.serverts]
                        APPURL.profileImageURL = currentUser.imageUrl+currentUser.avatar
                        isLoggedIn = true
                        self.navigationController?.popViewController(animated: true)
                        self.loginToCometChat()
                    }
                }
            }
        }
    }

    @IBAction func register(_ sender: UIButton) {
        let superVC:RegisterViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        superVC.navigationItem.title = "REGISTER"
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            self.view.layer.transform = CATransform3DIdentity
        } else {
            self.view.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, -100, 0)
        }
    }
    @IBAction func remember(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    @IBAction func forgot(_ sender: UIButton) {
        let superVC:ForgotPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    func setStyle(){
        registerLabel.subTitleLabel()
        nameText.setUpStyles()
        passwordText.setUpStyles()
        loginButton.setStyleBlue()
        registerButton.setStyleUnderline()
        rememberButton.setStyleUnderline()
        forgotButton.setUpStyles()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameText.text = "sireeshnaidu@gmail.com"
        passwordText.text = "welcome"

        self.setStyle()
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        switch textField {
        case nameText:
            passwordText.becomeFirstResponder()
            break
        case passwordText:
            textField.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
    }
    
    func isEntriesValid() -> Bool{
        if self.nameText.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Email address is empty!")
            return false
        }
        if !self.nameText.isEmailValid(){
            ToastView.sharedInstance.showAlert(message: "Invalid email ID!")
            return false
        }
        if self.passwordText.isEmpty(){
            ToastView.sharedInstance.showAlert(message: "Password is empty!")
            return false
        }
        
        return true
    }
    
    func loginToCometChat(){
        let instanceOfCustomObject: CometChatViewController = CometChatViewController.sharedInstance() as! CometChatViewController
        instanceOfCustomObject.login(nameText.text, password: passwordText.text)
    }
}
