//
//  SpotlightDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class SpotlightDetailsViewController:SuperDetailsViewController,UIWebViewDelegate,SegmentDelegate
{
    var spotlightItem:SpotlightItem = SpotlightItem(fromJson:[:])
    var spotlightArray:NSMutableArray = []
    var archivespotlightArray:NSMutableArray = []

    var mPlatinumType:platinumType = platinumType.pcoa
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rowsArray = ["SegmentCell","TitleCell","ImageCell","WebViewCell","LogoCell","ArchivesButtonCell","SubmitButtonCell"]
        
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
            
        case "SegmentCell":
            let cell:SegmentCell = tableView.dequeueReusableCell(withIdentifier: "SegmentCell", for: indexPath) as!SegmentCell
            cell.delegate = self
            return cell
            
        case "TitleCell":
            let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titleLabel.text = self.spotlightItem.name
            cell.setUpStyles()
            return cell
            
        case "ImageCell":
            let cell:ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as!ImageCell
            cell.imageview.loadImage(urlString: self.imageurl+self.spotlightItem.image)
            return cell
            
        case "ButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.titleLabel.text = "Comments"
            cell.setUpStyles()
            return cell

        case "SubmitButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.titleLabel.text = "SUBMIT YOUR PROPOSAL"
            cell.setUpStyles()
            return cell

        case "ArchivesButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.titleLabel.text = "CLUB SPOTLIGHT ARCHIVES"
            cell.setUpStyles()
            return cell

        case "ClubSubmitButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.titleLabel.text = "CLUB SPOTLIGHT SUBMISSIONS"
            cell.setUpStyles()
            return cell

        case "WebViewCell":
            let cell:WebViewCell = tableView.dequeueReusableCell(withIdentifier: "WebViewCell", for: indexPath) as!WebViewCell
            cell.webView.delegate = self
            cell.loadHTML(htmlString: self.spotlightItem.descriptionField)
            return cell
            
        case "LogoCell":
            let cell:LogoCell = tableView.dequeueReusableCell(withIdentifier: "LogoCell", for: indexPath) as!LogoCell
            return cell
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
        case "ButtonCell":
            print("Tapped")
            break
        case "SubmitButtonCell":
            print("Tapped")
            let superVC:FormSubmissionViewController = self.storyboard!.instantiateViewController(withIdentifier: "FormSubmissionViewController") as! FormSubmissionViewController
            superVC.navigationItem.title = "PROPOSAL"
            superVC.loadViewIfNeeded()
            self.navigationController?.pushViewController(superVC, animated: true)
            break
        case "ArchivesButtonCell":
            print("Tapped")
            if archivespotlightArray.count > 0 {
                let superVC:SpotlightClubListViewController = self.storyboard!.instantiateViewController(withIdentifier: "SpotlightClubListViewController") as! SpotlightClubListViewController
                superVC.navigationItem.title = "ARCHIVES"
                superVC.spotlightArray = archivespotlightArray
                superVC.loadViewIfNeeded()
                self.navigationController?.pushViewController(superVC, animated: true)
            }
            break
        case "ClubSubmitButtonCell":
            print("Tapped")
            break
        default:
            print("Tapped")
        }
    }

    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: false)
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        
        if descriptionHeight == 0 {
            descriptionHeight = webView.frame.size.height + 20
            self.table.reloadData()
        }
    }
    
    func segmentSelected(index:Int){
         self.archivespotlightArray.removeAllObjects()
        var pcowArray = [SpotlightItem]()
        var pcoaArray = [SpotlightItem]()
        var predicate = NSPredicate(format: "spotlightType = 'PCOW'")
        pcowArray = spotlightArray.filtered(using: predicate) as! [SpotlightItem]
        predicate = NSPredicate(format: "spotlightType == 'PCOA'")
        pcoaArray = spotlightArray.filtered(using: predicate) as! [SpotlightItem]

        switch index {
        case 0:
            if pcowArray.count > 0 {
                spotlightItem = pcowArray[0]
                self.archivespotlightArray.addingObjects(from: pcowArray as [Any])
            }
            else {
                ToastView.sharedInstance.showAlert(message: "Data Not Available")
            }
            break
        case 1:
            if pcoaArray.count > 0 {
                spotlightItem = pcoaArray[0]
                self.archivespotlightArray.addingObjects(from: pcoaArray as [Any])
            }else {
                ToastView.sharedInstance.showAlert(message: "Data Not Available")
            }
            break
        default:
            print("None")
        }
        
        self.table.reloadData()
    }
}

