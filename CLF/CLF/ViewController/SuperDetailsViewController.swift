//
//  SuperDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class SuperDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var table: UITableView!
    var refreshControl: UIRefreshControl!
    var descriptionHeight:CGFloat = 0

    var rowsArray:NSMutableArray = ["SegmentCell","TitleCell","SubtitleCell","ImageCell","LogoCell","ButtonCell","WebViewCell"]
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = ""
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.table.addSubview(refreshControl) // not required when using UITableViewController
        self.table.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")

    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
            return  self.rowsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
        case "SegmentCell":
            let cell:SegmentCell = tableView.dequeueReusableCell(withIdentifier: "SegmentCell", for: indexPath) as!SegmentCell
            return cell
            
        case "TitleCell":
            let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            return cell
            
        case "SubtitleCell":
            let cell:SubtitleCell = tableView.dequeueReusableCell(withIdentifier: "SubtitleCell", for: indexPath) as! SubtitleCell
            return cell
            
        case "ImageCell":
            let cell:ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as!ImageCell
            return cell
            
        case "LogoCell":
            let cell:LogoCell = tableView.dequeueReusableCell(withIdentifier: "LogoCell", for: indexPath) as!LogoCell
            return cell
            
        case "ButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            return cell
            
        case "WebViewCell":
            let cell:WebViewCell = tableView.dequeueReusableCell(withIdentifier: "WebViewCell", for: indexPath) as!WebViewCell
            return cell
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
        case "SegmentCell":
            return 49
        case "TitleCell":
            return 84
        case "SubtitleCell":
            return 30
        case "ImageCell":
            return 206
        case "logoCell":
            return 121
        case "ButtonCell","SubmitButtonCell","ArchivesButtonCell","ClubSubmitButtonCell":
            return 60
        case "WebViewCell":
            return descriptionHeight
        case "SummaryCell":
            return 260
        default:
            return 100
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        refreshControl.endRefreshing()
    }
}
