//
//  HomeViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

extension HomeViewController:SectionHeaderViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func showMore(index:NSInteger)
    {
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        
        switch index {
        case 0: // More Articles
            let superVC:ArticlesViewController = mainstoryboard.instantiateViewController(withIdentifier: "ArticleWindow") as! ArticlesViewController
            superVC.loadViewIfNeeded()
            self.navigationController?.pushViewController(superVC, animated: true)
            return
            
        case 1://MoreEventsViewController
            let superVC:MoreEventsViewController = mainstoryboard.instantiateViewController(withIdentifier: "MoreEventsViewController") as! MoreEventsViewController
            superVC.hidesBottomBarWhenPushed = true
            superVC.loadViewIfNeeded()
            superVC.title = "EXCLUSIVE EVENTS"
            self.navigationController?.pushViewController(superVC, animated: true)
            return
        case 2:// More Spotlight
            return
            
        case 3:// More Jobs
            let superVC:JobsViewController = mainstoryboard.instantiateViewController(withIdentifier: "JobsWindow") as! JobsViewController
            superVC.loadViewIfNeeded()
            self.navigationController?.pushViewController(superVC, animated: true)
            return
        case 4: // More services
            return
            
        default:
            superVC.bodyImage.image=UIImage(named: "UnderConstruction")
        }
        self.navigationController?.pushViewController(superVC, animated: true)
    }
}

class HomeViewController: SuperViewController, UITableViewDataSource, UITableViewDelegate, articleDelegate {
    @IBOutlet weak var table: UITableView!
    var sectionArray: NSMutableArray = []
    var articlesArray: NSMutableArray = []
    var eventsArray: NSMutableArray = []
    var homespotlightArray: NSMutableArray = []
    var clubspotlightArray: NSMutableArray = []
    var managerspotlightArray: NSMutableArray = []
    var vendorspotlightArray: NSMutableArray = []

    var jobsArray: NSMutableArray = []
    var subscriptionArray: NSMutableArray = []
    var imageurl = "http://clf.oneclubnet.com/uploads/images/"
    var homeWebService = {
        return HomeWebService()
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let logo = UIImage(named: "TitleLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.table.addSubview(refreshControl) // not required when using UITableViewController

        if isLoggedIn == true {
            homeWebService.fetchHomeData { (homeObject) in
                if let object = homeObject{
                    DispatchQueue.main.async {
                        self.updateTable(object: object)
                    }
                    print(object)
                }
            }
        }
        else{
            homeWebService.fetchHomeDataNoLogin { (homeObject) in
                if let object = homeObject{
                    DispatchQueue.main.async {
                        self.updateTable(object: object)
                    }
                    print(object)
                }
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = SectionHeaderView.instanceFromNib()
        headerView.delegate=self
        headerView.more.tag = section
        headerView.title.text = self.sectionArray.object(at: section) as? String
        
        switch self.sectionArray.object(at: section) as! String {
        case "LATEST ARTICLES","EXCLUSIVE EVENTS","JOB ALERTS","SERVICES AND SUBSCRIPTIONS":
            headerView.more.setTitle("More...", for: UIControlState.normal)
        case "SPOTLIGHT":
            headerView.more.setTitle("", for: UIControlState.normal)
        default:
            headerView.more.setTitle("", for: UIControlState.normal)
        }
        headerView.title.titleLabel()
        headerView.more.subTitleLabel()

        return headerView
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        switch self.sectionArray.object(at: section) as! String {
        case "LATEST ARTICLES":
            return 1
        case "EXCLUSIVE EVENTS":
            return self.eventsArray.count
        case "SPOTLIGHT":
            return 1
        case "JOB ALERTS":
            return self.jobsArray.count
        case "SERVICES & SUBSCRIPTIONS":
            return self.subscriptionArray.count
            
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return sectionArray[section] as? String
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.sectionArray.object(at: indexPath.section) as! String {
        case "LATEST ARTICLES":
                    let cell:ArticleCell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleCell
                    cell.imageurl = self.imageurl
                    cell.delegate = self
                    cell.configure(articles: self.articlesArray)
                    return cell
            
        case "EXCLUSIVE EVENTS":
                    let cell:EventsCell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as! EventsCell
                    let mObject:Event = self.eventsArray[indexPath.row] as! Event
                    if mObject.image.count > 0 {
                        cell.cellImage.loadImage(urlString: self.imageurl+mObject.image[0])
                    }
                    cell.titleLabel.text = mObject.title
                    cell.subTitle1Label.text = mObject.endDate
                    cell.setUpStyles()
                    return cell
            
        case "SPOTLIGHT":
                    let cell:SpotlightCell = tableView.dequeueReusableCell(withIdentifier: "SpotlightCell", for: indexPath) as! SpotlightCell
                        cell.spotlightCollectionView.delegate = self
                        cell.spotlightCollectionView.dataSource = self
                        cell.spotlightCollectionView.reloadData()
                        cell.setUpStyles()

                    return cell
            
        case "JOB ALERTS":
                    let cell:JobCell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobCell
                    let mObject:Job = self.jobsArray[0] as! Job
                    cell.cellImage.loadImage(urlString: self.imageurl+mObject.image)
                    cell.subTitleLabel.text = mObject.title
                    cell.subTitle1Label.text = mObject.endDate
                    cell.setUpStyles()

                    return cell
            
        case "SERVICES & SUBSCRIPTIONS":
                    let cell:ServiceCell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
                    let mObject:Subscription = self.subscriptionArray[indexPath.row] as! Subscription
                    cell.titleLabel.text = mObject.title
                    cell.setUpStyles()

                    return cell
            
        default:
                    let cell:EventsCell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as! EventsCell
                    return cell

        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        
        
        switch self.sectionArray.object(at: indexPath.section) as! String {
        case "LATEST ARTICLES":
            superVC.navigationItem.title = "LATEST ARTICLES"
            superVC.navigationItem.backBarButtonItem?.title = ""

        case "EXCLUSIVE EVENTS":
            
            let superVC:EventsDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventsDetailsViewController") as! EventsDetailsViewController
            superVC.hidesBottomBarWhenPushed = true
            superVC.eventsItem = self.eventsArray[ indexPath.row] as! Event
            superVC.imageurl = self.imageurl
            superVC.loadViewIfNeeded()
            superVC.navigationItem.title = "EXCLUSIVE EVENTS"
            superVC.navigationItem.backBarButtonItem?.title = ""
            self.navigationController?.pushViewController(superVC, animated: true)
            return
            
        case "SPOTLIGHT":
            
//            let superVC:SpotlightDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotlightDetailsViewController") as! SpotlightDetailsViewController
//            superVC.hidesBottomBarWhenPushed = true
//            superVC.spotlightItem = self.spotlightArray[ indexPath.row] as! SpotlightItem
//            superVC.imageurl = self.imageurl
//            superVC.loadViewIfNeeded()
//            superVC.navigationItem.title = "SPOTLIGHT"
//            superVC.navigationItem.backBarButtonItem?.title = ""
//            self.navigationController?.pushViewController(superVC, animated: true)
            return
            
        case "JOB ALERTS":
            superVC.navigationItem.title = "JOB ALERTS"
        case "SERVICES & SUBSCRIPTIONS":
            superVC.navigationItem.title = "SERVICES & SUBSCRIPTIONS"
            let serviceItem:Subscription = subscriptionArray[indexPath.row] as! Subscription
            
            let superVC:ServicesDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ServicesDetailsViewController") as! ServicesDetailsViewController
            superVC.hidesBottomBarWhenPushed = true
            superVC.navigationItem.title = serviceItem.title.uppercased()
            superVC.serviceID = serviceItem.id
            superVC.loadViewIfNeeded()
            self.navigationController?.pushViewController(superVC, animated: true)
            return


        default:
            superVC.navigationItem.title = " "

        }

        self.navigationController?.pushViewController(superVC, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch self.sectionArray.object(at: indexPath.section) as! String {
        case "LATEST ARTICLES":
            return 253
        case "EXCLUSIVE EVENTS":
            return 100
        case "SPOTLIGHT":
            return 200
        case "JOB ALERTS":
            return 75
        case "SERVICES & SUBSCRIPTIONS":
            return 40
            
        default:
            return 100
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homespotlightArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let mObject:SpotlightItem = self.homespotlightArray[indexPath.row] as! SpotlightItem
        
        let cell:ArticleType2Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleType2Cell", for: indexPath as IndexPath) as! ArticleType2Cell
        if mObject.type == "CLUB" {
            cell.titleLabel.text = "CLUB SPOTLIGHT"
        }
        else if mObject.type == "MANAGERS" {
            cell.titleLabel.text = "MANAGER SPOTLIGHT"
            }
        else if mObject.type == "VENDOR" {
            cell.titleLabel.text = "VENDOR SPOTLIGHT"
        }

        cell.setUpStyles()
        return cell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        let superVC:SpotlightDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotlightDetailsViewController") as! SpotlightDetailsViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.mPlatinumType = indexPath.row == 0 ? platinumType.pcow: platinumType.pcoa
        superVC.imageurl = self.imageurl
        superVC.spotlightItem = self.homespotlightArray[indexPath.row] as! SpotlightItem
        if superVC.spotlightItem.type == "CLUB" {
            superVC.spotlightArray = self.clubspotlightArray
            superVC.navigationItem.title = "CLUB SPOTLIGHT"
        }
        else if superVC.spotlightItem.type == "MANAGERS" {
            superVC.spotlightArray = self.managerspotlightArray
            superVC.navigationItem.title = "MANAGERS SPOTLIGHT"
        }
        else if superVC.spotlightItem.type == "VENDOR" {
            superVC.spotlightArray = self.vendorspotlightArray
            superVC.navigationItem.title = "VENDOR SPOTLIGHT"
        }

        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3-10, height: 150)
    }
    

    
    
    func articleSelected(index:Int){
        
        let superVC:ArticlesDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ArticlesDetailsViewController") as! ArticlesDetailsViewController
        
        superVC.hidesBottomBarWhenPushed = true
        superVC.articleItem = self.articlesArray[index] as! Article
        superVC.imageurl = self.imageurl
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
        
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        refreshControl.endRefreshing()
        homeWebService.fetchHomeData { (homeObject) in
            if let object = homeObject{
                DispatchQueue.main.async {
                    self.updateTable(object: object)
                }
                print(object)
            }
        }
    }
    
    func updateTable(object:Home){
        self.sectionArray.removeAllObjects()
        self.articlesArray.removeAllObjects()
        self.eventsArray.removeAllObjects()
        self.homespotlightArray.removeAllObjects()
        self.jobsArray.removeAllObjects()
        self.subscriptionArray.removeAllObjects()
        self.clubspotlightArray.removeAllObjects()
        self.managerspotlightArray.removeAllObjects()
        self.vendorspotlightArray.removeAllObjects()
        
        if object.articles.count > 0{
            self.sectionArray.add("LATEST ARTICLES")
            self.articlesArray.addObjects(from: object.articles)
        }
        if object.events.count > 0{
            self.sectionArray.add("EXCLUSIVE EVENTS")
            self.eventsArray.addObjects(from: object.events)
        }
        if object.spotlight.clubArray.count > 0 || object.spotlight.managerArray.count > 0 || object.spotlight.vendorArray.count > 0{
            self.sectionArray.add("SPOTLIGHT")
        }
        if object.spotlight.clubArray.count > 0 {
            self.homespotlightArray.add(object.spotlight.clubArray[0])
            self.clubspotlightArray.addObjects(from: object.spotlight.clubArray)
        }
        if object.spotlight.managerArray.count > 0{
            self.homespotlightArray.add(object.spotlight.managerArray[0])
            self.managerspotlightArray.addObjects(from: object.spotlight.managerArray)
        }
        if object.spotlight.vendorArray.count > 0{
            self.homespotlightArray.add(object.spotlight.vendorArray[0])
            self.vendorspotlightArray.addObjects(from: object.spotlight.vendorArray)
        }
        if object.jobs.count > 0{
            self.sectionArray.add("JOB ALERTS")
            self.jobsArray.addObjects(from: object.jobs)
        }
        if object.subscription.count > 0{
            self.sectionArray.add("SERVICES & SUBSCRIPTIONS")
            self.subscriptionArray.addObjects(from: object.subscription)
        }
        self.imageurl = object.imageUrl
        refreshControl.endRefreshing()
        self.table.reloadData()
    }

}
