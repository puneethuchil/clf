//
//  EventsDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class EventsDetailsViewController:SuperDetailsViewController,UIWebViewDelegate
{
    var eventsItem:Event = Event(fromJson:[:])

    override func viewDidLoad() {
        super.viewDidLoad()
        self.rowsArray = ["ImageCell","TitleCell","SubtitleCell","WebViewCell"]

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
            
        case "TitleCell":
            let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titleLabel.text = self.eventsItem.title
            cell.setUpStyles()
            return cell
            
        case "SubtitleCell":
            let cell:SubtitleCell = tableView.dequeueReusableCell(withIdentifier: "SubtitleCell", for: indexPath) as! SubtitleCell
            if indexPath.row == 2 {cell.subTitleLabel.text = eventsItem.city}
            else if indexPath.row == 3{cell.subTitleLabel.text = eventsItem.startDate+" - "+eventsItem.endDate}
            
            return cell
            
        case "ImageCell":
            let cell:ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as!ImageCell
            if eventsItem.image.count > 0 {
                cell.imageview.loadImage(urlString: self.imageurl+eventsItem.image[0])
            }
            return cell
            
        case "ButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.titleLabel.text = "Comments"
            cell.setUpStyles()
            return cell
            
        case "WebViewCell":
            let cell:WebViewCell = tableView.dequeueReusableCell(withIdentifier: "WebViewCell", for: indexPath) as!WebViewCell
            cell.webView.delegate = self
            cell.loadHTML(htmlString: self.eventsItem.descriptionField)
            return cell
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: false)
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        
        if descriptionHeight == 0 {
            descriptionHeight = webView.frame.size.height + 20
            self.table.reloadData()
        }
    }
}

