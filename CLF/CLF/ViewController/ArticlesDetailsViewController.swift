//
//  ArticlesDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/17/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ArticlesDetailsViewController:SuperDetailsViewController,UIWebViewDelegate
{
    var articleItem:Article = Article(fromJson:[:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rowsArray = ["TitleCell","ImageCell","SubtitleCell","SubtitleCell","WebViewCell"]

    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
            
        case "TitleCell":
            let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titleLabel.text = self.articleItem.name
            cell.setUpStyles()
            return cell
            
        case "SubtitleCell":
            let cell:SubtitleCell = tableView.dequeueReusableCell(withIdentifier: "SubtitleCell", for: indexPath) as! SubtitleCell
            if indexPath.row == 2 {cell.subTitleLabel.text = self.articleItem.author}
            else if indexPath.row == 3{cell.subTitleLabel.text = self.articleItem.designation+", "+self.articleItem.clubName}
            cell.setUpStyles()
            return cell
            
        case "ImageCell":
            let cell:ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as!ImageCell
            cell.imageview.loadImage(urlString: self.imageurl+self.articleItem.image)
            return cell
            
        case "ButtonCell":
            let cell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as!ButtonCell
            cell.titleLabel.text = "Comments"
            cell.setUpStyles()
            return cell
            
        case "WebViewCell":
            let cell:WebViewCell = tableView.dequeueReusableCell(withIdentifier: "WebViewCell", for: indexPath) as!WebViewCell
            cell.webView.delegate = self
            cell.loadHTML(htmlString: self.articleItem.descriptionField)
            return cell
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: false)
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        
        if descriptionHeight == 0 {
            descriptionHeight = webView.frame.size.height + 20
            self.table.reloadData()
        }
    }
}
