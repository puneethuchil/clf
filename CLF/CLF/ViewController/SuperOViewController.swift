//
//  SuperOViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/14/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class SuperOViewController: UIViewController
{
    
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var bodyImage: UIImageView!
    @IBOutlet weak var tapView: UIView!

    var childVC:SuperChildViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SuperChildWindow") as! SuperChildViewController

    @IBAction func gotToNextPage(_ sender: UITapGestureRecognizer) {
        if(self.childVC.bodyImage != nil) {
            self.navigationController?.pushViewController(self.childVC, animated: true)
        }
    }
    
    var segmentArray: NSArray = []
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        self.bodyImage.image = UIImage(named: segmentArray[self.segment.selectedSegmentIndex] as! String)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    } 
}
