//
//  MoreEventsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/24/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class MoreEventsViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    enum EventType {
        case past, present
    }
    
    @IBOutlet weak var table: UITableView!
    
    var currentEventType:EventType = EventType.present
    var sectionArray: NSMutableArray = []
    var pastArray: NSMutableArray = []
    var presentArray: NSMutableArray = []
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var circleObject = Circle.init(fromJson: [:])
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var exclusiveEventsData:ExclusiveEventsData = ExclusiveEventsData.init(fromJson: [:])
    
    var exclusiveEventsWebService = {
        return ExclusiveEventsWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exclusiveEventsWebService.fetchAllExclusiveEvents { (exclusiveEventsData) in
            if let mObject = exclusiveEventsData {
                DispatchQueue.main.async{
                    self.exclusiveEventsData = mObject
                    self.pastArray.removeAllObjects()
                    self.presentArray.removeAllObjects()

                    self.pastArray.addObjects(from: mObject.eventsList.past)
                    self.presentArray.addObjects(from: mObject.eventsList.future)
                    self.imageurl = mObject.imageUrl
                    
                    if self.pastArray.count > 0 {
                        self.sectionArray.add("PAST EVENTS")
                    }
                    if self.presentArray.count > 0 {
                        self.sectionArray.add("UPCOMING EVENTS")
                    }

                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:EventsCell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as! EventsCell
        var event:Event = Event.init(fromJson: [:])
        // Present Events
        if self.sectionArray.object(at: indexPath.section) as!String == "UPCOMING EVENTS" {
            event = self.presentArray.object(at: indexPath.row) as! Event
        }
        //Past
        if self.sectionArray.object(at: indexPath.section) as!String == "PAST EVENTS" {
            event = self.pastArray.object(at: indexPath.row) as! Event
        }
        
        cell.titleLabel.text = event.title
        cell.subTitleLabel.text = event.city + ", "+event.countryname
        cell.subTitle1Label.text = event.startDate + " - " + event.endDate
        
        if event.image.count > 0 {
            cell.cellImage.loadImage(urlString: self.imageurl+event.image[0])
        }
        cell.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (self.sectionArray.object(at: section) as! String)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionArray.count
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if self.sectionArray.object(at: section) as!String == "UPCOMING EVENTS" {
            return self.presentArray.count
        }
        if self.sectionArray.object(at: section) as!String == "PAST EVENTS" {
            return self.pastArray.count
        }
        else { return 0 }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:EventsDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventsDetailsViewController") as! EventsDetailsViewController
        superVC.hidesBottomBarWhenPushed = true
        if self.sectionArray.object(at: indexPath.section) as!String == "UPCOMING EVENTS" {
            superVC.eventsItem = self.presentArray[ indexPath.row] as! Event
        }
        else{
            superVC.eventsItem = self.pastArray[ indexPath.row] as! Event
        }
        superVC.imageurl = self.imageurl
        superVC.loadViewIfNeeded()
        superVC.navigationItem.title = "EXCLUSIVE EVENTS"
        superVC.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.pushViewController(superVC, animated: true)
    }
}

