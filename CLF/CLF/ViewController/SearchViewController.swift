//
//  SearchViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/7/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

protocol SearchDelegate {
    func itemSelected(item:String)
}

class SearchViewController:UIViewController, UITableViewDataSource, UITableViewDelegate , UISearchBarDelegate
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    var data:NSMutableArray = []
    var filtered:[String] = []
    var delegate:SearchDelegate?

    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    var clubListWebService = {
        return ClubListWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        searchBar.delegate = self

        
        clubListWebService.fetchTopClubList { (clubdata) in
            if let mObject = clubdata {
                self.data.removeAllObjects()
                for i in 0..<mObject.clublist.count {
                    let object:Club = mObject.clublist[i]
                    self.data.add(object.clubName)
                }
                DispatchQueue.main.async{
                    self.table.reloadData()
                }
            }

        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = data.filter({ (text) -> Bool in
            let tmp: NSString = text as! NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        }) as! [String]
        if(filtered.count == 0){
            searchActive = true;
        }else {
            searchActive = true;
        }
        self.table.reloadData()
    }

    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
        
        if(searchActive){
            cell.titleLabel.text = filtered[indexPath.row]
        } else {
            cell.titleLabel.text = data[indexPath.row] as? String
        }
        cell.titleLabel.titleLabel()
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if(searchActive) {
            return filtered.count
        }

        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var result = self.data[indexPath.row]
        if(searchActive) {
            result = self.filtered[indexPath.row]
        }
        self.delegate?.itemSelected(item: result as! String)
        self.navigationController?.popViewController(animated: true)
    }
}


