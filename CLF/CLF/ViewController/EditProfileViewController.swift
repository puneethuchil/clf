//
//  EditProfileViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/25/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class EditProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var designation: UITextField!
    @IBOutlet weak var club: UITextField!
    @IBOutlet weak var interests: UITextField!
    @IBOutlet weak var contact: UITextField!
    @IBOutlet weak var aboutMe: UITextView!
    @IBOutlet weak var jobSwitch: UISwitch!
    @IBOutlet weak var skill: UITextField!
    @IBOutlet weak var picButton: UIButton!
    @IBOutlet weak var profilePic: UIImageView!
    
    var descriptionHeight:CGFloat = 0
    var profileItem = Profile(fromJson: [:])
    var rowsArray:NSMutableArray = ["Club","Management","PCOA"]
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var savedImageurl:NSURL!
    var profileWebService = {
        return ProfileWebService()
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showEdit()
        self.table.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        self.table.setEditing(true, animated: true)
        
        self.name.text = profileItem.fName + " " + profileItem.lName
        self.designation.text = profileItem.pastDesignation
        self.club.text = profileItem.clubs
        self.aboutMe.text = profileItem.aboutMe
        self.interests.text = profileItem.interest
        self.contact.text = profileItem.email
        self.profilePic.roundedCorner()

        self.setUpStyles()

        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillShow, object: nil)

        
        profileWebService.fetchProfileInfo { (profileObject) in
            if let mObject = profileObject {
                self.profileItem = mObject
                self.imageurl = mObject.imageUrl
                DispatchQueue.main.async {
                    self.table.reloadData()
                }
            }
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.itemWith(colorfulImage: UIImage(named: "Submit.png"), target: self, action:  #selector(submit))
        
    }
    @IBAction func submit(_ sender: UIButton) {
        let imagedata:Data = UIImagePNGRepresentation(self.profilePic.image!)!
        profileWebService.editProfile(imageData:imagedata, parameters: ["f_name":self.name.text!,
                                                                        "l_name":self.name.text!,
                                                                        "current_designation":self.designation.text!,
                                                                        "skills":self.skill.text!,
                                                                        "interest":self.interests.text!,
                                                                        "about_me":self.aboutMe.text!])
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showEdit(){
    }
    
    @IBAction func addSkill(_ sender: UIButton) {
        self.rowsArray.add(self.skill.text as Any)
        self.table.reloadData()
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return  self.rowsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
        cell.titleLabel.text = self.rowsArray.object(at: indexPath.row) as? String
        cell.setUpStyles()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.rowsArray.removeObject(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .middle)
            tableView.endUpdates()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        switch textField {
        case name:
            designation.becomeFirstResponder()
            break
        case designation:
            club.becomeFirstResponder()
            break
        case club:
            contact.becomeFirstResponder()
            break
        case contact:
            interests.becomeFirstResponder()
            break
        case interests:
            skill.becomeFirstResponder()
            break
        case skill:
            textField.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            self.view.layer.transform = CATransform3DIdentity
        } else {
            self.view.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, -100, 0)
        }
    }
    
    func setUpStyles(){
        self.name.setUpStyles()
        self.designation.setUpStyles()
        self.club.setUpStyles()
        self.aboutMe.setUpStyles()
        self.interests.setUpStyles()
        self.contact.setUpStyles()
        self.skill.setUpStyles()
    }
    
    @IBAction func changePic(_ sender: UIButton) {
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let takeActionButton = UIAlertAction(title: "Take Photo", style: .default) { _ in
            print("Take Photo")
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }
        actionSheetControllerIOS8.addAction(takeActionButton)
        
        let chooseActionButton = UIAlertAction(title: "Choose Photo", style: .default)
        { _ in
            print("Choose Photo")
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }
        actionSheetControllerIOS8.addAction(chooseActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Delete Photo", style: .default)
        { _ in
            print("Delete Photo")
            self.profilePic.image = UIImage.init(named: "placeholder")
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.savedImageurl = info[UIImagePickerControllerReferenceURL] as! NSURL
        self.profilePic.image =  image
        dismiss(animated:true, completion: nil)
    }

    
}
