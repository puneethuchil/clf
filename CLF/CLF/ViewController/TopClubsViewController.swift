//
//  TopClubsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/19/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class TopClubsViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var titleLabel: UILabel!

    var clubCategoryArray: NSMutableArray = []

    var topclubs:TopClubs = TopClubs.init(fromJson: [:])
    var categoryIndex = 0
    
    enum pageType {
        case pcowclubs, pcoacategories, pcoaclubs
    }
    
    var currentPage = pageType.pcowclubs

    var topClubsWebService = {
        return TopClubsWebService()
    }()


    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topClubsWebService.fetchTopClubsInfo(forType: "PCOW") { (topClubs) in
            if let mObject = topClubs {
                DispatchQueue.main.async{
                    self.topclubs = mObject
                    self.clubCategoryArray.removeAllObjects()
                    self.clubCategoryArray.addObjects(from: self.topclubs.clubCategories)
                    self.titleLabel.text = "Platinum Clubs Of World"
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        
        switch self.segment.selectedSegmentIndex {
        case 0:
            currentPage = pageType.pcowclubs
            self.topClubsWebService.fetchTopClubsInfo(forType: "PCOW") { (topClubs) in
                if let mObject = topClubs {
                    DispatchQueue.main.async{
                        self.topclubs = mObject
                        self.clubCategoryArray.removeAllObjects()
                        self.clubCategoryArray.addObjects(from: self.topclubs.clubCategories)
                        self.titleLabel.text = "Platinum Clubs Of World"
                        self.table.reloadData()
                    }
                }
            }
            break
        case 1:
            currentPage = pageType.pcoacategories
            self.topClubsWebService.fetchTopClubsInfo(forType: "PCOA") { (topClubs) in
                if let mObject = topClubs {
                    DispatchQueue.main.async{
                        self.topclubs = mObject
                        self.clubCategoryArray.removeAllObjects()
                        self.clubCategoryArray.addObjects(from: self.topclubs.clubCategories)
                        self.titleLabel.text = "Platinum Clubs Of America"
                        self.table.reloadData()
                    }
                }
            }
            break
        default:
            break
        }
        
        if self.topclubs.clubCategories != nil &&  self.topclubs.clubCategories.count != 0 {
            let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
            self.titleLabel.text = category.name
        }
        
//        let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
//        self.titleLabel.text = category.name
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch currentPage {
        case pageType.pcowclubs, pageType.pcoaclubs:
            let cell:TopClubCell = tableView.dequeueReusableCell(withIdentifier: "TopClubCell", for: indexPath) as! TopClubCell
            let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
            let club:ClubInfo = category.subArray[categoryIndex]
            cell.nameLabel.text = club.clubName
            cell.locationLabel.text = club.country
            cell.pointsLabel.text = club.points
            cell.rankLabel.text = club.rank
            cell.setUpStyles()
            return cell
        case pageType.pcoacategories:
            let cell:TopClubCategoryCell = tableView.dequeueReusableCell(withIdentifier: "TopClubCategoryCell", for: indexPath) as! TopClubCategoryCell
            let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
            cell.nameLabel.text = category.name
            cell.setUpStyles()
            return cell
        }
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if self.topclubs.clubCategories == nil ||  self.topclubs.clubCategories.count == 0 {
            return 0
        }

        switch currentPage {
        case pageType.pcowclubs,pageType.pcoaclubs:
            let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
            return category.subArray.count
            
        case pageType.pcoacategories:
            return self.clubCategoryArray.count

        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch currentPage {
        case pageType.pcowclubs,pageType.pcoaclubs:
            return 80
        case pageType.pcoacategories:
            return 50
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch currentPage {
        case pageType.pcowclubs,pageType.pcoaclubs:
            self.categoryIndex = 0
//                let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
//                let club:ClubInfo = category.subArray[categoryIndex]
//                //WebViewController
//                let superVC:WebViewController = mainstoryboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//                superVC.loadViewIfNeeded()
//                superVC.loadHTML(htmlString: club.clubName)
//                self.navigationController?.pushViewController(superVC, animated: true)
            return
        case pageType.pcoacategories:
            self.categoryIndex = indexPath.row
              let superVC:ClubListViewController = mainstoryboard.instantiateViewController(withIdentifier: "ClubListViewController") as! ClubListViewController
            let category = self.clubCategoryArray.object(at: categoryIndex) as! ClubCategories
            superVC.navigationItem.title = category.name.uppercased()
            superVC.clubsArray.addObjects(from: category.subArray)
            superVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(superVC, animated: true)
            return
        }
    }
}

