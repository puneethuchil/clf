//
//  PlatinumCircleViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class PlatinumCircleViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    
    var circleArray: NSMutableArray = []
    
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var circleObject = Circle.init(fromJson: [:])
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"

    var platinumCircleWebService = {
        return PlatinumCircleWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        platinumCircleWebService.fetchCircleInfo { (circle) in
            if let mObject = circle {
                DispatchQueue.main.async{
                    self.circleObject = mObject
                    self.circleArray.removeAllObjects()
                    self.circleArray.addObjects(from: mObject.platinumCircle)
                    self.imageurl = self.circleObject.imageUrl
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PlatinumCircleCell = tableView.dequeueReusableCell(withIdentifier: "PlatinumCircleCell", for: indexPath) as! PlatinumCircleCell
        let club:PlatinumCircle = self.circleArray[indexPath.row] as! PlatinumCircle
        cell.nameLabel.text = club.title
        cell.linkLabel.text = club.website
        cell.logoImageview.loadImage(urlString: self.imageurl+club.image)
        cell.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return self.circleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mObject:PlatinumCircle = self.circleArray[indexPath.row] as! PlatinumCircle
        //WebViewController
        let superVC:WebViewController = mainstoryboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        superVC.loadViewIfNeeded()
        superVC.loadHTML(htmlString: mObject.website)
        self.navigationController?.pushViewController(superVC, animated: true)
    }

}



