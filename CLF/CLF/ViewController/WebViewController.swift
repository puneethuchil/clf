//
//  WebViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/7/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit
class WebViewController: UIViewController,UIWebViewDelegate{
    @IBOutlet weak var contentWebView:UIWebView!
    
    func loadTerms(url:String){
        contentWebView.delegate = self
        let mUrl = URL (string:url);
        let request = URLRequest(url: mUrl!)
        contentWebView.loadRequest(request);
    }

    
    func loadHTML(htmlString:String){
        contentWebView.delegate = self
        let url = URL (string: "https://"+htmlString);
        let request = URLRequest(url: url!)
        contentWebView.loadRequest(request);
    }
    
    func loadPDF(htmlString:String){
        contentWebView.delegate = self
        let url = URL (string:htmlString);
        let request = URLRequest(url: url!)
        contentWebView.loadRequest(request);
    }
    
    func loadStripe(amount:String, email:String, orderId:String, payment:String, newCard:String, productType:String){
        contentWebView.delegate = self
        let url = URL (string: "https://clf.platinumclubnet.com/payment/stripe.php");
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        let postString = "email="+email+"&amount="+amount+"&order_id="+orderId+"&payment="+payment+"&new_card="+newCard+"&product_type="+productType+"&user_id="+currentUser.userId
        request.httpBody = postString.data(using: .utf8)
        contentWebView.loadRequest(request);
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LoadingView.sharedInstance.display(show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let urlString:String = (request.url?.lastPathComponent)!
        print(urlString)
        if urlString == "empty.php" {
            let superVC:OrderListingViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderListingViewController") as! OrderListingViewController
            superVC.loadViewIfNeeded()
            self.navigationController?.pushViewController(superVC, animated: true)
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: false)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        ToastView.sharedInstance.showAlert(message:"Error in loading the content!")
        LoadingView.sharedInstance.display(show: false)
    }
    
}
