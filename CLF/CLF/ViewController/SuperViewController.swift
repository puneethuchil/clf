//
//  SuperViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/12/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

extension SuperViewController:MenuDelegate
{
    func menuSelected(viewController:UIViewController)
    {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func loggedOut() {
        self.tabBarController?.selectedIndex = 0
    }
}

class SuperViewController: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var bodyImage: UIImageView!
    @IBOutlet weak var tapView: UIView!
    var refreshControl: UIRefreshControl!

    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    let menuVC:MenuViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuView") as! MenuViewController
    var childVC:SuperChildViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SuperChildWindow") as! SuperChildViewController
    
    @IBAction func gotToNextPage(_ sender: UITapGestureRecognizer) {
        if(self.childVC.bodyImage != nil) {
            self.navigationController?.pushViewController(self.childVC, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")

        self.menuVC.delegate=self as MenuDelegate
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]

    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @IBAction func MenuTapped(_ sender: UIBarButtonItem) {
        
        self.menuVC.view.frame.origin.x = -300

        UIApplication.shared.keyWindow!.addSubview(menuVC.view)
        UIApplication.shared.keyWindow!.bringSubview(toFront: menuVC.view)
        self.menuVC.tapView.alpha = 0.0;

        UIView.transition(with: menuVC.view,
                          duration: 0.5,
                          options: UIViewAnimationOptions.curveEaseOut,
                          animations: {
                            self.menuVC.view.frame.origin.x = 0
        }) { (completed) in
            UIView.animate(withDuration: 0.25, animations: {self.menuVC.tapView.alpha=0.5})
        }
    }
    
    func pushMe(tagIndex:NSInteger) {
        
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        
        superVC.loadViewIfNeeded()
        
        switch tagIndex {
        case 0:
            superVC.bodyImage.image = UIImage(named:"OtherProfileWindow")
        case 1:
            superVC.bodyImage.image = UIImage(named:"OtherProfileInviteWindow")
        case 2:
            superVC.bodyImage.image = UIImage(named:"ChatWindow")
        default:
            superVC.bodyImage.image = UIImage(named:"ChatWindow")
        }
        
        self.navigationController?.pushViewController(superVC, animated: true)
    }
}
