//
//  SuperChildViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/15/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class SuperChildViewController:UIViewController
{
    @IBOutlet weak var bodyImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func itemTapped(_ sender: UIButton) {
    }
    
}
