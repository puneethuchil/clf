//
//  ClubListViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class ClubListViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    
    var clubsArray: NSMutableArray = []
    
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TopClubCell = tableView.dequeueReusableCell(withIdentifier: "TopClubCell", for: indexPath) as! TopClubCell
        let club:ClubInfo = self.clubsArray[indexPath.row] as! ClubInfo
        cell.nameLabel.text = club.clubName
        cell.locationLabel.text = club.country
        cell.pointsLabel.text = club.points
        cell.rankLabel.text = club.rank
        cell.setUpStyles()

        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return self.clubsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        let club:ClubInfo = self.clubsArray[indexPath.row] as! ClubInfo
//        //WebViewController
//        let superVC:WebViewController = mainstoryboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//        superVC.loadViewIfNeeded()
//        superVC.loadHTML(htmlString: club.clubName)
//        self.navigationController?.pushViewController(superVC, animated: true)
    }
}


