//
//  JobSearchViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/9/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class JobSearchViewController:UIViewController, UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    var searchActive : Bool = false
    var data:NSMutableArray = []
    var filtered:[String] = []
    
    var clubListWebService = {
        return ClubListWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        searchBar.delegate = self
        
        
        clubListWebService.fetchTopClubList { (clubdata) in
            if let mObject = clubdata {
                self.data.removeAllObjects()
                for i in 0..<mObject.clublist.count {
                    let object:Club = mObject.clublist[i]
                    self.data.add(object.clubName)
                }
                DispatchQueue.main.async{
                    self.table.reloadData()
                }
            }
            
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:JobCell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobCell
        cell.cellImage.loadImage(urlString:"placeholder")
        cell.applyButton.tag = indexPath.row
        cell.applyButton.setStyleBlue()
        cell.titleLabel.titleLabel()
        cell.subTitleLabel.subTitleLabel()
        cell.subTitle1Label.subTitle1Label()
        cell.bgView.viewBorder()
        cell.bgView.shadow()
        cell.applyButton.addTarget(self, action: #selector(self.showDetails) , for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if(searchActive) {
            return filtered.count
        }
        
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var result = self.data[indexPath.row]
        if(searchActive) {
            result = self.filtered[indexPath.row]
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showDetails(_ sender: UIButton) {
        let superVC:SuperViewController = self.storyboard?.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        superVC.bodyImage.image = UIImage(named:"JobDetailPage")
        self.navigationController?.pushViewController(superVC, animated: true)
    }
}


