//
//  DiscussionDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/23/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class DiscussionDetailsViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var discussionTitle: UILabel!
    @IBOutlet weak var discussionDescription: UITextView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet weak var club: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var commentText: UITextView!
    @IBOutlet weak var commentButton: UIButton!

    var servicesArray: NSMutableArray = []
    
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var discussionObject = Discussion.init(fromJson: [:])
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var discussionDataItem = DiscussionData.init(fromJson: [:])
    var discussionViewModel = {
        return DiscussionViewModel()
    }()
    
    @IBAction func comment(_ sender: UIButton) {
        self.commentText.resignFirstResponder()
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to post your comment?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
            { action in
                switch action.style{
                case .default:
                    self.discussionViewModel.postComments(description: self.commentText.text, id:self.discussionObject.id) { (commentsObject) in
                        if let mObject = commentsObject {
                            DispatchQueue.main.async{
                                print(mObject)
                                
                                self.discussionViewModel.fetchComments(id: self.discussionObject.id, completion: { (commentlist) in
                                    if let mObject = commentlist {
                                        print(mObject)
                                        self.discussionObject.comment.removeAll()
                                        self.discussionObject.comment = mObject.commentList
                                        self.table.reloadData()
                                    }
                                })
                            }
                        }
                    }
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}
        ))
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentText.setUpStyles()
        self.discussionTitle.text = discussionObject.title
        self.discussionDescription.text = discussionObject.descriptionField
        self.name.text = discussionObject.fName
        self.designation.text = discussionObject.current_designation
        self.club.text = discussionObject.clubs
        self.image.loadImage(urlString: self.imageurl+discussionObject.avatar)
        self.discussionTitle.titleLabel()
        self.discussionDescription.setUpStyles()
        self.name.titleLabel()
        self.designation.subTitleLabel()
        self.club.subTitle1Label()
        self.commentButton.setStyleBlue()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CommentCell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        let comment:Comment = self.discussionObject.comment[indexPath.row]
        cell.commentLabel.text =  comment.descriptionField
        cell.commentLabel.border()
        cell.commentLabel.radius()
        cell.timeLabel.text = comment.title
        cell.avatarImage.roundedCorner()
        cell.avatarImage.loadImage(urlString: self.imageurl+comment.avatar)
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return self.discussionObject.comment.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}
