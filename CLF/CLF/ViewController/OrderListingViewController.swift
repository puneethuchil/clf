//
//  OrderListingViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/15/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class OrderListingViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"

    var transactionsArray: NSMutableArray = []
    
    var orderWebService = {
        return OrderWebService()
    }()
    
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        orderWebService.fetchOrders { (orderData) in
            if let mObject = orderData {
                DispatchQueue.main.async{
                    self.imageurl = (orderData?.imageUrl)!
                    self.transactionsArray.removeAllObjects()
                    self.transactionsArray.addObjects(from: mObject.orderList)
                    self.saveCard()
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
        
        // Pop to Home 
        if self.navigationController?.viewControllers.index(of: self) == nil {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveCard(){
        if self.transactionsArray.count > 0 {
            UserDefaults.standard.set(true, forKey: "savedCard")
        }
        else {
            UserDefaults.standard.set(false, forKey: "savedCard")
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TransactionCell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
        let mObject:Order = self.transactionsArray[indexPath.row] as! Order
        cell.titleLabel.text = mObject.subscriptionTitle
        cell.subTitleLabel.text = "ORDER ID: "+mObject.id
        cell.subTitle1Label.text = "PRICE : USD "+mObject.amount
        cell.cellImage.loadImage(urlString: self.imageurl+mObject.id)
        cell.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return self.transactionsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:ProductDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.pageType = "SUMMARY"
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
    }
}



