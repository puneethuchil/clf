//
//  PrefferedPartnersViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class PrefferedPartnersViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var partnerCollectionView: UICollectionView!
    var refreshControl: UIRefreshControl!
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var platinumPartnerObject = PlatinumPartners.init(fromJson: [:])
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    
    var platinumPartnerWebService = {
        return PlatinumPartnerWebService()
    }()
    
    enum partnerType {
        case Platinum, Gold, Silver
    }
    var ptype:partnerType = partnerType.Platinum
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.partnerCollectionView.addSubview(refreshControl)
        self.partnerCollectionView.alwaysBounceVertical = true;
        
        platinumPartnerWebService.fetchPlatinumPartners { (partnerObject) in
            if let object = partnerObject{
                DispatchQueue.main.async {
                    self.platinumPartnerObject = object
                    self.imageurl = self.platinumPartnerObject.imageUrl
                    self.partnerCollectionView.reloadData()
                }
                print(object)
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeaderCollectionReusableView", for: indexPath) as! SectionHeaderCollectionReusableView

        switch indexPath.section {
        case 0:
            header.headerLabel.text = "PLATINUM PARTNERS"
            break
        case 1:
            header.headerLabel.text = "GOLD PARTENRS"
            break
        case 2:
            header.headerLabel.text = "SILVER PARTNERS"
            break
        default:
            header.headerLabel.text = "SILVER PARTNERS"
        }
        
        header.setUpStyles()
        
        return header
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: 100, height: 50)
//    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.platinumPartnerObject.partnerList == nil {
            return 0
        }
        switch section {
        case 0:
            return self.platinumPartnerObject.partnerList.platinum.count
        case 1:
            return self.platinumPartnerObject.partnerList.gold.count
        case 2:
            return self.platinumPartnerObject.partnerList.silver.count
        default:
            return 0
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var mObject:Partner = Partner.init(fromJson: [:])

        switch indexPath.section {
        case 0:
            mObject = self.platinumPartnerObject.partnerList.platinum[indexPath.row]
        case 1:
            mObject = self.platinumPartnerObject.partnerList.gold[indexPath.row]
        case 2:
            mObject = self.platinumPartnerObject.partnerList.silver[indexPath.row]
        default:
            break
        }
        
        let cell:PartnersCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartnersCell", for: indexPath as IndexPath) as! PartnersCell
        cell.cellImage.loadImage(urlString: self.imageurl+mObject.image)
        cell.setUpStyles()
        return cell
    
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events

        var mObject:Partner = Partner.init(fromJson: [:])

        switch indexPath.section {
        case 0:
            mObject = self.platinumPartnerObject.partnerList.platinum[indexPath.row]
        case 1:
            mObject = self.platinumPartnerObject.partnerList.gold[indexPath.row]
        case 2:
            mObject = self.platinumPartnerObject.partnerList.silver[indexPath.row]
        default:
            break
        }
        
        let superVC:WebViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        superVC.loadViewIfNeeded()
        superVC.loadHTML(htmlString: mObject.id)
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width/3-10, height: collectionView.frame.width/3-10)
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshControl.endRefreshing()

        platinumPartnerWebService.fetchPlatinumPartners { (platinumPartnerObject) in
            if let object = platinumPartnerObject{
                DispatchQueue.main.async {
                    self.platinumPartnerObject = object
                    self.imageurl = self.platinumPartnerObject.imageUrl
                    self.partnerCollectionView.reloadData()
                    self.refreshControl.endRefreshing()
                }
                print(object)
            }
        }
    }
}
