//
//  PrefferedVendersViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/25/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class PrefferedVendersViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var currentVendordata:VendorData =  VendorData.init(fromJson: [:])
    var vendorWebService = {
        return VendorWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vendorWebService.fetchVenderInfo { (vendordata) in
            if let mObject = vendordata {
                DispatchQueue.main.async{
                    self.currentVendordata = mObject
                    self.imageurl = mObject.imageUrl
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PlatinumCircleCell = tableView.dequeueReusableCell(withIdentifier: "PlatinumCircleCell", for: indexPath) as! PlatinumCircleCell
        let mObject:Vendor = self.currentVendordata.vendors[indexPath.row]
        cell.nameLabel.text = mObject.name
        cell.linkLabel.text = mObject.website
        cell.logoImageview.loadImage(urlString: self.imageurl+mObject.image)
        cell.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if self.currentVendordata.vendors == nil {
            return 0
        }
        return self.currentVendordata.vendors.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //VenderDetailsViewController
        let superVC:VenderDetailsViewController = mainstoryboard.instantiateViewController(withIdentifier: "VenderDetailsViewController") as! VenderDetailsViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.vendorObject = self.currentVendordata.vendors[indexPath.row]
        superVC.navigationItem.title = superVC.vendorObject.name.uppercased()
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)

    }
}




