//
//  ForgotPasswordViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/15/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ForgotPasswordViewController:UIViewController {
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBAction func submit(_ sender: UIButton) {
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }

}
