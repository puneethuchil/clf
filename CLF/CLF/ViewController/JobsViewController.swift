//
//  JobsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class JobsViewController: SuperViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource  {
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var skillCollection: UICollectionView!
    @IBOutlet weak var ClubLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var haederView: UIView!

     var sectionArray: NSArray = ["SEARCH A JOB","JOBS FOR YOU"]
    var jobsArray: NSArray = ["Job 1","Job 2","Job 3","Job 4"]
    var skillArray: NSArray = ["CLUBS","MANAGEMENT","GOLF","PLATINUMCLUBS"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let logo = UIImage(named: "TitleLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.skillCollection.delegate = self
        self.skillCollection.dataSource = self
        self.designationLabel.titleLabel()
        self.ClubLabel.subTitleLabel()
        self.avatarImage.roundedCorner()
        self.table.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isLoggedIn == false{
            self.avatarImage.image = UIImage.init(named:"CLFPlaceholder")
            self.haederView.frame.size.height = 0
            self.haederView.isHidden = true
        }
        else{
            self.haederView.frame.size.height = 172
            self.haederView.isHidden = false
            self.avatarImage.loadImage(urlString: APPURL.profileImageURL)
        }
        self.table.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = SectionHeaderView.instanceFromNib()

        switch section {
        case 0:
            headerView.title.text="SEARCH A JOB"
            break
        case 1:
            headerView.title.text="JOBS FOR YOU"
            break
        default: break
        }
        headerView.more.setTitle("", for: UIControlState.normal)
        headerView.title.titleLabel()
        return headerView
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        switch section {
        case 0:
            return 1
        case 1:
            return 5
        default: break
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section {
        case 0:
            let cell:SearchJobCell = tableView.dequeueReusableCell(withIdentifier: "SearchJobCell", for: indexPath) as! SearchJobCell
                cell.searchText.delegate = self
                cell.searchButton.addTarget(self, action: #selector(self.search) , for: UIControlEvents.touchUpInside)

            return cell
        case 1:
            let cell:JobCell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobCell
            cell.cellImage.loadImage(urlString:"placeholder")
            cell.applyButton.tag = indexPath.row
            cell.applyButton.setStyleBlue()
            cell.titleLabel.titleLabel()
            cell.subTitleLabel.subTitleLabel()
            cell.subTitle1Label.subTitle1Label()
            cell.bgView.viewBorder()
            cell.bgView.shadow()
            cell.applyButton.addTarget(self, action: #selector(self.showDetails) , for: UIControlEvents.touchUpInside)
            return cell
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        superVC.bodyImage.image = UIImage(named:"JobDetailPage")
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    @IBAction func search(_ sender: UIButton) {
        
    }
    
    @IBAction func showDetails(_ sender: UIButton) {
        
        if isLoggedIn == false {
            let alert = UIAlertController(title: "Alert", message: "Please login to use this feature", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                { action in
                    switch action.style{
                    case .default:
                        let superVC:LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        superVC.hidesBottomBarWhenPushed = true
                        superVC.loadViewIfNeeded()
                        self.navigationController?.pushViewController(superVC, animated: true)
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ action in
            } ))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
            superVC.hidesBottomBarWhenPushed = true
            superVC.loadViewIfNeeded()
            superVC.bodyImage.image = UIImage(named:"JobDetailPage")
            self.navigationController?.pushViewController(superVC, animated: true)
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.skillArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        let cell:SkillCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillCell", for: indexPath as IndexPath) as! SkillCell
        cell.skillName.text = self.skillArray.object(at: indexPath.row) as? String
        return cell
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
}
