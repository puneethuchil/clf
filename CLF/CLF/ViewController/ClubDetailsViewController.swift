//
//  ClubDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/1/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class ClubDetailsViewController:SuperDetailsViewController,UIWebViewDelegate
{
    var spotlightItem:SpotlightItem = SpotlightItem(fromJson:[:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rowsArray = ["TitleCell","ImageCell","WebViewCell","LogoCell"]
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
            
        case "TitleCell":
            let cell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titleLabel.text = self.spotlightItem.name
            cell.setUpStyles()
            return cell
            
        case "ImageCell":
            let cell:ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as!ImageCell
            cell.imageview.loadImage(urlString: self.imageurl+self.spotlightItem.image)
            return cell
            
        case "WebViewCell":
            let cell:WebViewCell = tableView.dequeueReusableCell(withIdentifier: "WebViewCell", for: indexPath) as!WebViewCell
            cell.webView.delegate = self
            cell.loadHTML(htmlString: self.spotlightItem.descriptionField)
            return cell
            
        case "LogoCell":
            let cell:LogoCell = tableView.dequeueReusableCell(withIdentifier: "LogoCell", for: indexPath) as!LogoCell
            return cell
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingView.sharedInstance.display(show: false)
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        
        if descriptionHeight == 0 {
            descriptionHeight = webView.frame.size.height + 20
            self.table.reloadData()
        }
    }
}


