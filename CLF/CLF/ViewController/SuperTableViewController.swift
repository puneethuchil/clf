//
//  SuperTableViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/15/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class SuperTableViewController:UIViewController, UITableViewDataSource, UITableViewDelegate 
{
    @IBOutlet weak var table: UITableView!

    var segmentArray: NSArray = []
    var segmentVCArray: NSArray = []
    var childVCArray: NSArray = []

    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:ServiceDetailCell = tableView.dequeueReusableCell(withIdentifier: "ServiceDetailCell", for: indexPath) as! ServiceDetailCell
        cell.cellImage.image = UIImage(named: segmentArray[indexPath.row] as! String)
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return segmentArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 180
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        
        superVC.loadViewIfNeeded()
        
        switch indexPath.row {
        case 0:
            superVC.bodyImage.image=UIImage(named: segmentVCArray[indexPath.row] as! String)
            superVC.childVC.loadViewIfNeeded()
            superVC.childVC.bodyImage.image = UIImage(named: childVCArray[indexPath.row] as! String)
        case 1:
            superVC.bodyImage.image = UIImage(named: segmentVCArray[indexPath.row] as! String)
        case 2:
            superVC.bodyImage.image = UIImage(named: segmentVCArray[indexPath.row] as! String)
        default:
            superVC.bodyImage.image = UIImage(named: segmentVCArray[indexPath.row] as! String)
        }
        
        self.navigationController?.pushViewController(superVC, animated: true)
    }
}
