//
//  MenuViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 9/13/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

protocol MenuDelegate {
    func menuSelected(viewController:UIViewController)
    func loggedOut()
}


class MenuViewController:UIViewController, UITableViewDataSource, UITableViewDelegate {

    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var section1Array: NSMutableArray = ["LOGIN","MY PROFILE"]
    var section2Array: NSArray = ["ABOUT US","ADVISORY BOARD","PRODUCT CART"]
    var section3Array: NSArray = ["PLATINUM CLUBS","PREFERRED PARTNERS","PREFERRED VENDORS","PLATINUM CIRCLE","SERVICE & SUBSCRIPTIONS"]
    var section4Array: NSMutableArray = ["PLATINUMCLUBNET","MY TRANSACTIONS"]
    var sectionArray: NSArray = []
    var newsectionArray: NSArray = ["LOGIN","MY PROFILE","NEWS & VIEWS","CAREERS & JOBS","NETWORK","BEST PRACTICE","PRODUCT MART","THE FORUM","SERVICES","PARTNERS","PLATINUM CLUBS"]

    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    var delegate:MenuDelegate?
    
    @IBAction func MenuTapped(_ sender: Any) {
        self.tapView.alpha=0.0
        UIView.transition(with: self.view,
                          duration: 0.5,
                          options: UIViewAnimationOptions.curveEaseOut,
                          animations: {
                            self.view.frame.origin.x = -300
        }) { (completed) in
            self.view.removeFromSuperview()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isLoggedIn == true {
            section1Array = ["LOG OUT","MY PROFILE"]
            section4Array = ["PLATINUMCLUBNET","MY TRANSACTIONS"]
            self.avatarImage.loadImage(urlString: APPURL.profileImageURL)
        }
        else{
            section1Array = ["LOGIN"]
            section4Array = ["PLATINUMCLUBNET"]
            self.avatarImage.image = UIImage.init(named:"CLFPlaceholder")
        }
        sectionArray = [section1Array,section2Array,section3Array,section4Array]
        self.table.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = SectionHeaderView.instanceFromNib()
        headerView.isHidden=true
        return headerView
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        switch section {
        case 0:
            return section1Array.count
        case 1:
            return section2Array.count
        case 2:
            return section3Array.count
        case 3:
            return section4Array.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0 && indexPath.row == 0 {
            let logincell:TitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            logincell.titleLabel.text = section1Array[indexPath.row] as? String
            return logincell
        }
        
        let cell:MenuItemCell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell", for: indexPath) as! MenuItemCell
        cell.cellImage.image = UIImage(named:"MenuItemBG.png")
        
        switch indexPath.section {
        case 0:
            cell.cellLabel.text = section1Array[indexPath.row] as? String
            return cell
        case 1:
            cell.cellLabel.text = section2Array[indexPath.row] as? String
            return cell
        case 2:
            cell.cellLabel.text = section3Array[indexPath.row] as? String
            return cell
        case 3:
            cell.cellLabel.text = section4Array[indexPath.row] as? String
            return cell
            
        default:
            cell.cellLabel.text = section1Array[indexPath.row] as? String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.MenuTapped("Yes")
        
        let superVC:SuperViewController = mainstoryboard.instantiateViewController(withIdentifier: "SuperWindow") as! SuperViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        superVC.navigationItem.backBarButtonItem?.title = ""
        superVC.hidesBottomBarWhenPushed = true

        switch indexPath.section {
        case 0:
            switch indexPath.row {
                case 0://LoginViewController
                    if isLoggedIn == false {
                        let superVC:LoginViewController = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        superVC.navigationItem.title = (section1Array[indexPath.row] as? String)?.uppercased()
                        superVC.hidesBottomBarWhenPushed = true
                        delegate?.menuSelected(viewController:superVC)
                    }
                    else {
                        let instanceOfCustomObject: CometChatViewController = CometChatViewController.sharedInstance() as! CometChatViewController
                            instanceOfCustomObject.logout()
                        isLoggedIn = false
                        UserDefaults.standard.set(nil, forKey: "currentUser")
                        UserDefaults.standard.set(false, forKey: "savedCard")
                        delegate?.loggedOut()
                    }
                    break
                case 1:
                    let superVC:ProfileViewController = mainstoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    superVC.navigationItem.title = (section1Array[indexPath.row] as? String)?.uppercased()
                    superVC.hidesBottomBarWhenPushed = true
                    delegate?.menuSelected(viewController:superVC)
                    break
                default:
                    break
            }
            break
        case 1:
            switch indexPath.row {
            case 0:
                let superVC:AboutusViewController = mainstoryboard.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
                superVC.navigationItem.title = (section2Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break
            case 1:
                let superVC:BoardMembersViewController = mainstoryboard.instantiateViewController(withIdentifier: "BoardMembersViewController") as! BoardMembersViewController
                superVC.navigationItem.title = (section2Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break
            case 2://ProductListingViewController
                let superVC:ProductListingViewController = mainstoryboard.instantiateViewController(withIdentifier: "ProductListingViewController") as! ProductListingViewController
                superVC.navigationItem.title = (section2Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break
            default:
                break
            }
            
            
        case 2:
            switch indexPath.row {
            case 0: //TopClubsViewController
                let superVC:TopClubsViewController = mainstoryboard.instantiateViewController(withIdentifier: "TopClubsViewController") as! TopClubsViewController
                superVC.navigationItem.title = (section3Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break
            case 1://PrefferedPartnersViewController
                let superVC:PrefferedPartnersViewController = mainstoryboard.instantiateViewController(withIdentifier: "PrefferedPartnersViewController") as! PrefferedPartnersViewController
                superVC.navigationItem.title = (section3Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                
                break
            case 2://PrefferedVendersViewController
                let superVC:PrefferedVendersViewController = mainstoryboard.instantiateViewController(withIdentifier: "PrefferedVendersViewController") as! PrefferedVendersViewController
                superVC.navigationItem.title = (section3Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break

            case 3:
                let superVC:PlatinumCircleViewController = mainstoryboard.instantiateViewController(withIdentifier: "PlatinumCircleViewController") as! PlatinumCircleViewController
                superVC.navigationItem.title = (section3Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break
               // ServicesListingViewController
            case 4:
                let superVC:ServicesListingViewController = mainstoryboard.instantiateViewController(withIdentifier: "ServicesListingViewController") as! ServicesListingViewController
                superVC.navigationItem.title = (section3Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                delegate?.menuSelected(viewController:superVC)
                break
            default:
                superVC.navigationItem.title = (section3Array[indexPath.row] as? String)?.uppercased()
                superVC.hidesBottomBarWhenPushed = true
                superVC.bodyImage.image = UIImage(named:"UnderConstruction")
                delegate?.menuSelected(viewController:superVC)
                break
            }
            break
        case 3:
            switch indexPath.row {
                case 0:
                superVC.navigationItem.title = (section4Array[indexPath.row] as? String)?.uppercased()
                superVC.bodyImage.image = UIImage(named:"UnderConstruction")
                delegate?.menuSelected(viewController:superVC)
                break
            case 1:
                let superVC:OrderListingViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderListingViewController") as! OrderListingViewController
                superVC.hidesBottomBarWhenPushed = true
                superVC.loadViewIfNeeded()
                delegate?.menuSelected(viewController:superVC)
                default:
                    superVC.bodyImage.image = UIImage(named:"UnderConstruction")
                    delegate?.menuSelected(viewController:superVC)
                    break
            }
            break
        default:
            superVC.bodyImage.image = UIImage(named:"UnderConstruction")
            delegate?.menuSelected(viewController:superVC)
            break
        }
    }
}
