//
//  ServicesListingViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/23/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class ServicesListingViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    
    var servicesArray: NSMutableArray = []
    
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    var platinumServicesItem:SubscriptionListData = SubscriptionListData.init(fromJson: [:])
    var platinumServicesWebService = {
        return PlatinumServicesWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        platinumServicesWebService.fetchPlatinumServices { (platinumServices) in
            if let mObject = platinumServices {
                DispatchQueue.main.async{
                    self.platinumServicesItem = mObject
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ServicesCell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell", for: indexPath) as! ServicesCell
        let serviceItem = platinumServicesItem.subscriptionList[indexPath.row]
        cell.titleLabel.text = serviceItem.title
        cell.subTitleLabel.text = serviceItem.descriptionField
        cell.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if self.platinumServicesItem.subscriptionList == nil {
            return 0
        }
        return self.platinumServicesItem.subscriptionList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 200
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let serviceItem = platinumServicesItem.subscriptionList[indexPath.row]

        let superVC:ServicesDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ServicesDetailsViewController") as! ServicesDetailsViewController
        superVC.hidesBottomBarWhenPushed = true
        superVC.navigationItem.title = serviceItem.title.uppercased()
        superVC.serviceID = serviceItem.id
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)

    }
}




