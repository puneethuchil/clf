//
//  ProfileViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var table: UITableView!
    var refreshControl: UIRefreshControl!
    var descriptionHeight:CGFloat = 0
    var profileItem = Profile(fromJson: [:])
    var rowsArray:NSMutableArray = []
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var skillArray: NSMutableArray = ["Club","Management","PCOA"]
    
    var profileWebService = {
        return ProfileWebService()
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showEdit()
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.table.addSubview(refreshControl) // not required when using UITableViewController
        self.table.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        
        profileWebService.fetchProfileInfo { (profileObject) in
            if let mObject = profileObject {
                self.profileItem = mObject
                self.imageurl = mObject.imageUrl
                APPURL.profileImageURL = self.imageurl+self.profileItem.avatar
                DispatchQueue.main.async {
                    self.rowsArray = ["ProfilePicCell","CollectionCell","ProfileDescriptionCellAbout","ProfileDescrip`tionCellInterest","ProfileDescriptionCellContact","CollectionCellImage","JobToggleCell"]

                    self.table.reloadData()
                }
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showEdit(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.itemWith(colorfulImage: UIImage(named: "edit.png"), target: self, action:  #selector(addTapped))
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(addTapped))
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        //EditProfileViewController
        let superVC:EditProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        superVC.profileItem = self.profileItem
        superVC.hidesBottomBarWhenPushed = true
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: false)

    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return  self.rowsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
        case "ProfilePicCell":
            let cell:ProfilePicCell = tableView.dequeueReusableCell(withIdentifier: "ProfilePicCell", for: indexPath) as!ProfilePicCell
                cell.profileImage.loadImage(urlString: self.imageurl+self.profileItem.avatar)
                cell.nameLabel.text = self.profileItem.fName+" "+self.profileItem.lName
                cell.designationLabel.text = self.profileItem.currentDesignation
                cell.clubLabel.text = self.profileItem.clubs
                cell.setUpStyles()
            return cell

            
        case "CollectionCell":
            let cell:CollectionCell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
            cell.collection.tag = 0
            cell.collection.delegate = self
            cell.collection.dataSource = self
            return cell
            
        case "ProfileDescriptionCellAbout":
            let cell:ProfileDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "ProfileDescriptionCell", for: indexPath) as! ProfileDescriptionCell
            cell.titleLabel.text = "ABOUT ME"
            cell.descriptionLabel.text = self.profileItem.aboutMe

            cell.setUpStyles()
            return cell
            
        case "ProfileDescriptionCellInterest":
            let cell:ProfileDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "ProfileDescriptionCell", for: indexPath) as! ProfileDescriptionCell
            cell.titleLabel.text = "MY INTERESTS"
            cell.descriptionLabel.text = self.profileItem.interest
            
            cell.setUpStyles()
            return cell
            
        case "ProfileDescriptionCellContact":
            let cell:ProfileDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "ProfileDescriptionCell", for: indexPath) as! ProfileDescriptionCell
            cell.titleLabel.text = "CONTACT INFOMATION"
            cell.descriptionLabel.text = self.profileItem.email

            cell.setUpStyles()
            return cell
            
        case "CollectionCellImage":
            let cell:CollectionCell = tableView.dequeueReusableCell(withIdentifier: "CollectionCellImage", for: indexPath) as! CollectionCell
            cell.collection.delegate = self
            cell.collection.dataSource = self
            cell.collection.tag = 1
            return cell
            
        case "JobToggleCell":
            let cell:JobToggleCell = tableView.dequeueReusableCell(withIdentifier: "JobToggleCell", for: indexPath) as! JobToggleCell
            cell.titleLabel.text = "LOOKING FOR JOBS"
            cell.jobSwitch.isEnabled = false
            cell.setUpStyles()
            return cell
            
        default:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch self.rowsArray.object(at: indexPath.row) as! String {
        case "ProfilePicCell":
            return 250
        case "CollectionCell":
            return 50
        case "ProfileDescriptionCellAbout","ProfileDescriptionCellInterest","ProfileDescriptionCellContact":
            return 120
        case "JobToggleCell":
            return 50
        case "CollectionCellImage":
            return 100
        default:
            return 0
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.skillArray.count
        }
        else {
            return 1
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        
        if collectionView.tag == 0 {
            let cell:SkillCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillCell", for: indexPath as IndexPath) as! SkillCell
            cell.skillName.text = self.skillArray.object(at: indexPath.row) as? String
            return cell
        }
        else {
            let cell:ImageCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath as IndexPath) as! ImageCollectionCell
            if self.profileItem.platinumStatus == "PCOA"{
                cell.image?.image = UIImage.init(named: "PCOALogo")
            }else if self.profileItem.platinumStatus == "PCOW"{
                cell.image?.image = UIImage.init(named: "PCOWLogo")
            }
            return cell
        }
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        refreshControl.endRefreshing()

        profileWebService.fetchProfileInfo { (profileObject) in
            if let mObject = profileObject {
                self.profileItem = mObject
                self.imageurl = mObject.imageUrl
                DispatchQueue.main.async {
                    self.rowsArray = ["ProfilePicCell","CollectionCell","ProfileDescriptionCellAbout","ProfileDescrip`tionCellInterest","ProfileDescriptionCellContact","CollectionCellImage","JobToggleCell"]
                    
                    self.table.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }

    }
    
}
