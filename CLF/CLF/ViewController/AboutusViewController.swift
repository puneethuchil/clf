//
//  AboutusViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/1/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class AboutusViewController:UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var image: UIImageView!

    var aboutUsData = AboutUsData.init(fromJson: [:])
    var mainstoryboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    var aboutUsWebService = {
        return AboutUsWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutUsWebService.fetchAboutUs { (aboutUsObject) in
            
            if let mObject = aboutUsObject {
                self.aboutUsData = mObject
                DispatchQueue.main.async{
                    self.image.loadImage(urlString: self.aboutUsData.imageUrl+self.aboutUsData.aboutus.image)
                    self.table.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TextViewCell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell", for: indexPath) as! TextViewCell
        cell.descriptionText.text = self.aboutUsData.aboutus.descriptionField
        cell.descriptionText.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
            return self.aboutUsData.aboutus == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.view.frame.size.height - self.image.frame.size.height
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}



