//
//  NetworkViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class NetworkViewController: SuperViewController{
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let logo = UIImage(named: "TitleLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        print("viewWillLayoutSubviews")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isLoggedIn == false{
            let alert = UIAlertController(title: "Alert", message: "Please login to use this feature", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                { action in
                    switch action.style{
                    case .default:
                        let superVC:LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        superVC.hidesBottomBarWhenPushed = true
                        superVC.loadViewIfNeeded()
                        self.navigationController?.pushViewController(superVC, animated: true)
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ action in
                self.tabBarController?.selectedIndex = 0
            } ))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else{
            LoadingView.sharedInstance.displayonview(view: self.view, show: true)
                let instanceOfCustomObject: CometChatViewController = CometChatViewController.sharedInstance() as! CometChatViewController
                instanceOfCustomObject.delegate = self
                instanceOfCustomObject.launchChat()
        }  
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func cometChatCallback(){
        self.tabBarController?.selectedIndex = 0;
    }
    
}
