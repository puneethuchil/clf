//
//  DiscussionsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/31/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit

class DiscussionsViewController: SuperViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var discussionTitle: UITextField!
    @IBOutlet weak var discussionDescription: UITextView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var discussionLabel: UILabel!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!

    var servicesArray: NSMutableArray = []
    
    var circleObject = Circle.init(fromJson: [:])
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var discussionDataItem = DiscussionData.init(fromJson: [:])
    var discussionViewModel = {
        return DiscussionViewModel()
    }()
    
    @IBAction func postDiscussion(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to post this?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
            { action in
                switch action.style{
                case .default:
                    self.discussionViewModel.postDiscussion(title: self.discussionTitle.text!, description: self.discussionDescription.text!) { (discussionObject) in
                        if let mObject = discussionObject {
                            print(mObject)
                            self.discussionViewModel.fetchPostedDiscussions { (discussionObject) in
                                if let mObject = discussionObject {
                                    DispatchQueue.main.async{
                                        self.discussionDataItem = mObject
                                        self.table.reloadData()
                                    }
                                }
                            }

                        }
                    }

                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}
        ))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func clearDiscussion(_ sender: UIButton) {
        self.discussionTitle.text = ""
        self.discussionDescription.text = ""
    }
    
    @IBAction func addComment(_ sender: UIButton) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isLoggedIn == false{
            self.avatarImage.image = UIImage.init(named:"CLFPlaceholder")
            let alert = UIAlertController(title: "Alert", message: "Please login to use this feature", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                { action in
                    switch action.style{
                    case .default:
                        let superVC:LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        superVC.hidesBottomBarWhenPushed = true
                        superVC.loadViewIfNeeded()
                        self.navigationController?.pushViewController(superVC, animated: true)
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ action in
                self.tabBarController?.selectedIndex = 0
            } ))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else {
            self.avatarImage.loadImage(urlString: APPURL.profileImageURL)
            
            discussionViewModel.fetchPostedDiscussions { (discussionObject) in
                if let mObject = discussionObject {
                    DispatchQueue.main.async{
                        self.discussionDataItem = mObject
                        self.table.reloadData()
                    }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named: "TitleLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.setStyle()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.table.addSubview(refreshControl) // not required when using UITableViewController
        self.discussionTitle.setUpStyles()
        self.discussionDescription.setUpStyles()
        self.discussionTitle.addToolBar()
        self.discussionDescription.addToolBar()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func setStyle(){
        discussionTitle.setUpStyles()
        discussionDescription.setUpStyles()
        avatarImage.roundedCorner()
        discussionLabel.titleLabel()
        postButton.setStyleBlue()
        clearButton.setStyleBlue()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ConversationCell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell", for: indexPath) as! ConversationCell
        let discussion:Discussion = self.discussionDataItem.discussion[indexPath.row]
        cell.titleLabel.text =  discussion.fName
        cell.subTitleLabel.text = discussion.current_designation
        cell.subTitle1Label.text = discussion.clubs
        cell.discussionTitle.text = discussion.title
        cell.discussionDescription.text = discussion.descriptionField
        cell.cellImage.loadImage(urlString: self.imageurl+discussion.avatar)
        cell.commentsButton.tag = indexPath.row
        cell.commentsButton.setStyleBlue()
        cell.discussionDescription.setUpStyles()
        cell.discussionTitle.titleLabel()

        cell.commentsButton.addTarget(self, action: #selector(DiscussionsViewController.addComment(_:)), for: UIControlEvents.touchUpInside)

        cell.setUpStyles()
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if self.discussionDataItem.discussion == nil {return 0}
        return self.discussionDataItem.discussion.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 250
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:DiscussionDetailsViewController = mainstoryboard.instantiateViewController(withIdentifier: "DiscussionDetailsViewController") as! DiscussionDetailsViewController
        superVC.hidesBottomBarWhenPushed = true
//        superVC.navigationItem.title = "EXCLUSIVE EVENTS"
        superVC.discussionObject = self.discussionDataItem.discussion[indexPath.row]
        superVC.loadViewIfNeeded()
        self.navigationController?.pushViewController(superVC, animated: true)
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        switch textField {
        case discussionTitle:
            discussionDescription.becomeFirstResponder()
            break
        case discussionDescription:
            textField.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        refreshControl.endRefreshing()

        discussionViewModel.fetchPostedDiscussions { (discussionObject) in
            if let mObject = discussionObject {
                DispatchQueue.main.async{
                    self.discussionDataItem = mObject
                    self.table.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }

    }
}
