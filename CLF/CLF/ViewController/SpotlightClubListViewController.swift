//
//  SpotlightClubListViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/1/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import UIKit

class SpotlightClubListViewController:ClubListViewController
{
    var spotlightArray:NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spotlightArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TopClubCategoryCell = tableView.dequeueReusableCell(withIdentifier: "TopClubCategoryCell", for: indexPath) as! TopClubCategoryCell
        let spotlightItem:SpotlightItem = spotlightArray.object(at: indexPath.row) as! SpotlightItem
        cell.nameLabel.text = spotlightItem.name
        cell.setUpStyles()
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let superVC:ClubDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: "ClubDetailsViewController") as! ClubDetailsViewController
        superVC.loadViewIfNeeded()
        superVC.spotlightItem = spotlightArray.object(at: indexPath.row) as! SpotlightItem

        self.navigationController?.pushViewController(superVC, animated: true)
    }

}
