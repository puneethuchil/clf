//
//  VenderDetailsViewController.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/25/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import UIKit

class VenderDetailsViewController:UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var membersCollectionView: UICollectionView!
    var refreshControl: UIRefreshControl!
    
    var boardMembersArray:NSMutableArray = []
    var imageurl:String = "http://clf.oneclubnet.com/uploads/images/"
    var vendorObject:Vendor = Vendor.init(fromJson: [:])
    var productDataItem:ProductData = ProductData.init(fromJson:[:])
    
    
    var vendorWebService = {
        return VendorWebService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.membersCollectionView.addSubview(refreshControl)
        
        vendorWebService.fetchProductsForVendor(id: vendorObject.id) { (productData) in
            DispatchQueue.main.async {
                if let mObject = productData{
                    self.productDataItem = mObject
                    self.imageurl = mObject.imageUrl
                    self.membersCollectionView.reloadData()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        LoadingView.sharedInstance.displayonview(view: self.view, show: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func interested(_ sender: UIButton) {
        sender.isEnabled = false
        vendorWebService.postInterest(id: vendorObject.id) { (productObject) in
            DispatchQueue.main.async {
                if let mObject = productObject{
                    print(mObject)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.productDataItem.product == nil{
            return 0
        }
        return self.productDataItem.product.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let mObject:Product = self.productDataItem.product[indexPath.row]
        
        let cell:ProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath as IndexPath) as! ProductCell
        cell.cellImage.loadImage(urlString: self.imageurl+mObject.image)
        cell.titleLabel.text = mObject.title
        cell.loadHTML(htmlString: mObject.descriptionField)
        cell.setUpStyles()
        cell.interest.tag = indexPath.row
        cell.interest.isEnabled = !mObject.intrested
        cell.interest.addTarget(self, action: #selector(VenderDetailsViewController.interested(_:)), for: UIControlEvents.touchUpInside)
        return cell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 260)
    }
    
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        refreshControl.endRefreshing()
    }
    
    
}
