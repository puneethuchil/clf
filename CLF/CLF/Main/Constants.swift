//
//  Constants.swift
//  CLF
//
//  Created by Puneeth Uchil on 12/8/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit
import Alamofire

enum platinumType {
    case pcow, pcoa
}

var currentUser:UserInfo = UserInfo.init(fromJson: ["server-ts": "","access_token": ""])
var isLoggedIn:Bool = false
var headers : HTTPHeaders = ["Content-Type":"application/json",
                             "compatibility-version":"1",
                             "authorization":currentUser.accessToken,
                             "server-ts":currentUser.serverts]

var avatarImagePath:String = "https://clf.platinumclubnet.com/uploads/images/"
var cometChatAPIKey = "f76e7e423cda8d4884f73d15fecb22f8"

struct APPURL {
    
    static var profileImageURL:String = "https://clf.platinumclubnet.com/uploads/images/"
    
    private struct Domains {
        static let Dev = "https://clfdev.platinumclubnet.com/api/"
        static let UAT = "https://clf.platinumclubnet.com/api/"
        static let Local = "192.145.1.1"
        static let QA = "https://clf.platinumclubnet.com/api/"
        static let Commet = "https://clfchat.platinumclubnet.com/cometchat/api/index.php/api/index.php"
    }
    
    static var BaseURL: String {
        return Domains.Dev
    }
    static var CommetChatURL: String {
        return Domains.Commet
    }
}

struct FontNames {
    
    static let fontName = "PingFang SC"
    struct PingFong {
        static let fontBold = "PingFang-SC-SemiBold"
        static let fontMedium = "PingFang-SC-Medium"
        static let fontRegular = "PingFang-SC-Regular"
        static let fontLight = "PingFang-SC-Light"
    }
    
    static let appTitleFont =  UIFont(name: PingFong.fontBold, size: 11)
    static let appSubTitleFont =  UIFont(name: PingFong.fontMedium, size: 11)
    static let appSubTitle1Font =  UIFont(name: PingFong.fontLight, size: 11)
    static let appDescriptionFont =  UIFont(name: PingFong.fontRegular, size: 11)

}

//KeyConstants.swift
struct Key {
    
    static let DeviceType = "iOS"
    
    struct UserDefaults {
        static let k_App_Running_FirstTime = "userRunningAppFirstTime"
    }
    
    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
    }
    
    struct Google{
        static let placesKey = "AIzaSyAAE-IH4xo5WQRodH5X1tJLt3Swu3ZIJms"//for photos
        static let serverKey = "AIzaSyAAE-IH4xo5WQRodH5X1tJLt3Swu3ZIJms"
    }
}

struct AppColor {
    
    private struct Alphas {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }
    
    static let appPrimaryColor =  UIColor.white.withAlphaComponent(Alphas.SemiOpaque)
    static let appSecondaryColor =  UIColor.blue.withAlphaComponent(Alphas.Opaque)
    
    struct TextColors {
        static let Error = AppColor.appSecondaryColor
        static let Success = UIColor(red: 0.1303, green: 0.9915, blue: 0.0233, alpha: Alphas.Opaque)
        static let titleColor = UIColor.black
        static let subTitleColor = UIColor.darkText
        static let subTitle1Color = UIColor.gray
        static let descriptionColor = UIColor(red: 3.0, green: 3.0, blue: 3.0, alpha: Alphas.Opaque)
    }
    
    struct BorderColor {
        static let textView = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: Alphas.SemiTransparent).cgColor
    }
    
    struct ButtonColor {
       static let actionButton = UIColor.init(red: 217.00/255.0, green: 234.00/255.0, blue: 234.00/255.0, alpha: 1.0)
    }
    
    struct TabBarColors{
        static let Selected = UIColor.white
        static let NotSelected = UIColor.black
    }
    
    struct OverlayColor {
        static let SemiTransparentBlack = UIColor.black.withAlphaComponent(Alphas.Transparent)
        static let SemiOpaque = UIColor.black.withAlphaComponent(Alphas.SemiOpaque)
        static let demoOverlay = UIColor.black.withAlphaComponent(0.6)
    }
}

extension UIStoryboard{
    func loginStoryboard()->UIStoryboard{
        return UIStoryboard.init(name: "Login", bundle: Bundle.main)
    }
    func mainStoryboard()->UIStoryboard{
        return UIStoryboard.init(name: "Main", bundle: Bundle.main)
    }
    func dineStoryboard()->UIStoryboard{
        return UIStoryboard.init(name: "Details", bundle: Bundle.main)
    }
    func rewardsStoryboard()->UIStoryboard{
        return UIStoryboard.init(name: "Rewards", bundle: Bundle.main)
    }
}

