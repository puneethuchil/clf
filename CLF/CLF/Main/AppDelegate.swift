//
//  AppDelegate.swift
//  CLF
//
//  Created by Puneeth Uchil on 8/28/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.customizeNavigationBar()
        
        if userAlreadyExist(kUsernameKey: "currentUser") {
            if let decoded = UserDefaults.standard.data(forKey: "currentUser"){
                currentUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
                isLoggedIn = currentUser.accessToken.count > 0 ? true : false
            }
            else{
                isLoggedIn = false
            }
        }
        else {
            isLoggedIn = false
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        UserDefaults.standard.set(isLoggedIn, forKey: "isLoggedIn")
        if isLoggedIn == true {
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: currentUser)
            UserDefaults.standard.set(encodedData, forKey: "currentUser")
        }
        else{
            UserDefaults.standard.set(nil, forKey: "currentUser")
        }
    }

    func customizeNavigationBar(){
        // Navigation Bar
//        let myShadow = NSShadow()
//        myShadow.shadowBlurRadius = 3
//        myShadow.shadowOffset = CGSize(width: 3, height: 3)
//        myShadow.shadowColor = UIColor.gray
//        let font1 = UIFont(name: "PingFang SC", size: 12.0)!
//        let attrs = [NSFontAttributeName:font1,
//                     NSForegroundColorAttributeName: UIColor.white,
//                     NSShadowAttributeName:myShadow]
//        UINavigationBar.appearance().titleTextAttributes = attrs

        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "Navbar.png"), for: .default)
        UINavigationBar.appearance().tintColor = UIColor.white

        //Hide UIBarButton tite for back bar button
        let font = UIFont(name: "PingFang SC", size: 0)!
        let normalTextAttributes = [NSFontAttributeName: font]
        UIBarButtonItem.appearance().setTitleTextAttributes(normalTextAttributes, for: UIControlState.normal)
        UIBarButtonItem.appearance().setTitleTextAttributes(normalTextAttributes, for: UIControlState.selected)
        
        //Label
        UILabel.appearance().font = UIFont(name: "PingFang SC", size: 12.0)
    }
    
    func registerPush(application: UIApplication){
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
    }
    
    func userAlreadyExist(kUsernameKey: String) -> Bool {
        return UserDefaults.standard.data(forKey: kUsernameKey) != nil
    }
}

