//
//  Utility.swift
//  CLF
//
//  Created by Puneeth Uchil on 10/26/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import UIKit
import AlamofireImage

class Utility {
    //App Constants
}

extension UIBarButtonItem {
    class func itemWith(colorfulImage: UIImage?, target: AnyObject, action: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(colorfulImage, for: .normal)
        button.frame = CGRect(x: 0.0, y: 0.0, width: 44.0, height: 44.0)
        button.addTarget(target, action: action, for: .touchUpInside)
        
        let barButtonItem = UIBarButtonItem(customView: button)
        return barButtonItem
    }
}

extension String {
    
    func timeStringWithOCNFormat()-> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: date!)
    }
    
    func dateStringWithFormat(fromformat:String, toFormat:String)-> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromformat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: date!)
    }
    
    func dateStringWithFormat(format:String)-> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: date!)
    }
    
    func dateStringWithOCNFormat()-> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "MMM dd,yyyy"
        
        return dateFormatter.string(from: date!)
    }
}

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    func viewBorder(){
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func shadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
    }
    
    func radius(){
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
}

extension UILabel{
    func border(){
        self.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.8).cgColor
        self.layer.borderWidth = CGFloat(1.0)
        self.layer.masksToBounds = true
    }
    
    func titleLabel(){
        self.font = FontNames.appTitleFont
        self.textColor = AppColor.TextColors.titleColor
        self.text = self.text?.uppercased();
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
    
    func subTitleLabel(){
        self.font = FontNames.appSubTitleFont
        self.textColor = AppColor.TextColors.subTitleColor
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
    
    func subTitle1Label(){
        self.font = FontNames.appSubTitle1Font
        self.textColor = AppColor.TextColors.subTitle1Color
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
    
    func descreptionabel(){
        self.font = FontNames.appDescriptionFont
        self.textColor = AppColor.TextColors.descriptionColor
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
}

extension UIButton{
    
    func setStyleBlue(){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text)!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.titleLabel?.attributedText = attributedString
        self.backgroundColor = UIColor.init(red: 217.00/255.0, green: 234.00/255.0, blue: 234.00/255.0, alpha: 1.0)
        self.border()
    }
    func setStyleUnderline(){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text)!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        
        self.titleLabel?.attributedText = attributedString
    }
    
    func setUpStyles(){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text)!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.titleLabel?.attributedText = attributedString
    }
    
    func subTitleLabel(){
        self.titleLabel?.font = FontNames.appSubTitleFont!
        self.titleLabel?.textColor = AppColor.TextColors.subTitleColor
    }
    func border(){
        self.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.8).cgColor
        
        self.layer.borderWidth = CGFloat(1.0)
        self.layer.masksToBounds = true
    }
    func bottomBorder(add:Bool){
        let border = CALayer()
        let width = CGFloat(2.0)
        if add == true {
            border.borderColor = UIColor.orange.cgColor
        }
        else {
            border.borderColor = UIColor.white.cgColor
        }
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

}

extension UITextView{
    
    func border(){
        self.layer.borderColor = AppColor.BorderColor.textView
        self.layer.borderWidth = CGFloat(1.0)
        self.layer.masksToBounds = true
    }
    
    func setUpStyles(){
        self.font = FontNames.appDescriptionFont
        self.textColor = AppColor.TextColors.descriptionColor
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        self.attributedText = attributedString
        if self.isEditable == true{
            self.border()
        }
    }
    
    func addToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.gray
        //        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.donePressed))
        let doneButton =  UIBarButtonItem.itemWith(colorfulImage: UIImage(named: "dismiss.png"), target: self, action:  #selector(donePressed))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action:nil)
        toolBar.setItems([space,doneButton,space], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
    @objc func donePressed(){
        self.endEditing(true)
    }
}

extension UITextField{
    
    func isEmailValid() -> Bool {
        let emailID = self.text
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    func isEmpty() -> Bool {
        if self.text == "" {
            return true
        }
        else{
            return false
        }
    }
    
    func setUpStyles(){
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.50), range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
        
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 15, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
        
        self.border()
        self.addToolBar()
    }
    
    func border(){
        self.layer.borderColor = AppColor.BorderColor.textView
        self.layer.borderWidth = CGFloat(1.0)
        self.layer.masksToBounds = true
    }
    
    func roundedCorner(){
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }

    func bottomBorder(){
        let border = CALayer()
        let width = CGFloat(1.0)//rgb 205 203 203
        border.borderColor = UIColor(red: 205.0/255.0, green: 203.0/255.0, blue: 203.0/255.0, alpha: 0.5).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
    
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func addToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.gray
//        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.donePressed))
        let doneButton =  UIBarButtonItem.itemWith(colorfulImage: UIImage(named: "dismiss.png"), target: self, action:  #selector(donePressed))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action:nil)
        toolBar.setItems([space,doneButton,space], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
    @objc func donePressed(){
        self.endEditing(true)
    }
}


extension UIColor{
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }

}

extension UIImage {
    func imageTintColor(_ color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
//    func imageWithColor(color: UIColor, size: CGSize, alpha: CGFloat) -> UIImage {
//        UIGraphicsBeginImageContext(size)
//        let currentContext = UIGraphicsGetCurrentContext()
//        let fillRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        currentContext!.setFillColor(color.cgColor)
//        currentContext!.setAlpha(alpha)
//        currentContext!.fill(fillRect)
//        let retval: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return retval
//    }
}


extension UIImageView{
    
    
    func dropShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
    }
    
    func roundedCorner(){
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.masksToBounds = true
    }
    
    func addGradient()  {
        self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        let mGradient = CAGradientLayer()
        mGradient.frame = self.bounds
        mGradient.colors = [UIColor.clear.cgColor,UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor]
        mGradient.locations = [0.0,1.0]
        self.layer.addSublayer(mGradient)
    }
    
    func addTint()  {
        self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        let mGradient = CAGradientLayer()
        mGradient.frame = self.bounds
        mGradient.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3).cgColor
        mGradient.locations = [0.0,1.0]
        self.layer.addSublayer(mGradient)
    }
    
    func loadImage(urlString:String, placeholder:String){
        let url = URL(string: urlString)!
        
        let placeholderImage = UIImage(named: placeholder)!
        
//        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
//            size: self.frame.size,
//            radius: 5.0
//        )
        
        self.af_setImage(
            withURL: url,
            placeholderImage: placeholderImage,
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
    
    func loadImage(urlString:String){
        let url = URL(string: urlString)!
        
        let placeholderImage = UIImage(named: "placeholder")!
        
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
            size: self.frame.size,
            radius: 2.0
        )

        
        self.af_setImage(
            withURL: url,
            placeholderImage: placeholderImage,
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
    
    func loadImageInCircle(urlString:String){
        let url = URL(string: urlString)!
        
        let placeholderImage = UIImage(named: "placeholder")!
        
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
            size: self.frame.size,
            radius: 5.0
        )

        self.af_setImage(
            withURL: url,
            placeholderImage: placeholderImage,
            filter: filter,
            imageTransition: .crossDissolve(0.2)
        )
    }


}


