//
//  TopClubs.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
//
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import Alamofire

class TopClubs : NSObject{
    
    var clubCategories : [ClubCategories]!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubCategories = [ClubCategories]()
        
        let clubArray = json["platinumClubs"].arrayValue
        for clubJson in clubArray{
            let value = ClubCategories(fromJson: clubJson)
            clubCategories.append(value)
        }
    }
}


class ClubCategories : NSObject{
    
    var name : String!
    var subArray : [ClubInfo]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        name = json["name"].stringValue
        subArray = [ClubInfo]()
        let subArrayArray = json["subArray"].arrayValue
        for subArrayJson in subArrayArray{
            let value = ClubInfo(fromJson: subArrayJson)
            subArray.append(value)
        }
    }
}

class ClubInfo : NSObject{
    
    var clubName : String!
    var country : String!
    var points : String!
    var rank : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubName = json["club_name"].stringValue
        country = json["country"].stringValue
        points = json["points"].stringValue
        rank = json["rank"].stringValue
    }
}

class TopClubsWebService:NSObject {
    override init() {
    }
    
    func fetchTopClubsInfo(forType:String, completion: @escaping (TopClubs?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"platinumClubApi/"+forType)!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let topClubsObject = TopClubs.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(topClubsObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}




