//
//	Event.swift
//
//	Create by Puneeth Uchil on 15/1/2018
//	Copyright © 2018 Mobicom Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Event{
    
    var city : String!
    var clubName : String!
    var countrycode : String!
    var countryname : String!
    var descriptionField : String!
    var endDate : String!
    var id : String!
    var image : [String]!
    var startDate : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        city = json["city"].stringValue
        clubName = json["club_name"].stringValue
        countrycode = json["countrycode"].stringValue
        countryname = json["countryname"].stringValue
        descriptionField = json["description"].stringValue
        endDate = json["end_date"].stringValue
        id = json["id"].stringValue
        image = [String]()
        let imageArray = json["image"].arrayValue
        for imageJson in imageArray{
            image.append(imageJson.stringValue)
        }
        startDate = json["start_date"].stringValue
        title = json["title"].stringValue
    }
    
}
