//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON
import Alamofire

class Boardmember : NSObject{
    
    var clubname : String!
    var createdAt : String!
    var designation : String!
    var id : String!
    var image : String!
    var name : String!
    var status : String!
    var updatedAt : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubname = json["clubname"].stringValue
        createdAt = json["created_at"].stringValue
        designation = json["designation"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        status = json["status"].stringValue
        updatedAt = json["updated_at"].stringValue
    }
}



class Boardmembers : NSObject{

	var boardmember : [Boardmember]!
	var imageUrl : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		boardmember = [Boardmember]()
		let boardmemberArray = json["boardmember"].arrayValue
		for boardmemberJson in boardmemberArray{
			let value = Boardmember(fromJson: boardmemberJson)
			boardmember.append(value)
		}
		imageUrl = json["image_url"].stringValue
	}
}

class BoardMembersWebService:NSObject {
    override init() {
    }
    
    func fetchBoardMembersList(completion: @escaping (Boardmembers?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"boardMemberListApi")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let boardmembersObject = Boardmembers.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(boardmembersObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}

