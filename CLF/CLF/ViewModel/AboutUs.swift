//
//  AboutUs.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/1/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
//
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import Alamofire
//
//    Data.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class AboutUsData{
    
    var aboutus : Aboutus!
    var imageUrl : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        let aboutusJson = json["aboutus"]
        if !aboutusJson.isEmpty{
            aboutus = Aboutus(fromJson: aboutusJson)
        }
        imageUrl = json["image_url"].stringValue
    }
    
}

class Aboutus{
    
    var createdAt : String!
    var descriptionField : String!
    var id : String!
    var image : String!
    var status : String!
    var title : String!
    var updatedAt : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        updatedAt = json["updated_at"].stringValue
    }
}

class AboutUsWebService:NSObject {
    override init() {
    }
    
    func fetchAboutUs(completion: @escaping (AboutUsData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"aboutUs")!,
            method: .get,
            parameters: nil,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = AboutUsData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}

