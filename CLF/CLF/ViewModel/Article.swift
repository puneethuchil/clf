//
//	Article.swift
//
//	Create by Puneeth Uchil on 15/1/2018
//	Copyright © 2018 Mobicom Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Article : NSObject{
    
    var author : String!
    var clubId : String!
    var clubName : String!
    var createStatus : String!
    var createdAt : String!
    var descriptionField : String!
    var designation : String!
    var id : String!
    var image : String!
    var month : String!
    var name : String!
    var pdfPath : String!
    var price : String!
    var startDate : String!
    var subscriptionId : String!
    var tags : String!
    var typeId : String!
    var typename : String!
    var updatedAt : String!
    var userType : String!
    var year : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        author = json["author"].stringValue
        clubId = json["club_id"].stringValue
        clubName = json["club_name"].stringValue
        createStatus = json["create_status"].stringValue
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        designation = json["designation"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        month = json["month"].stringValue
        name = json["name"].stringValue
        pdfPath = json["pdf_path"].stringValue
        price = json["price"].stringValue
        startDate = json["start_date"].stringValue
        subscriptionId = json["subscription_id"].stringValue
        tags = json["tags"].stringValue
        typeId = json["type_id"].stringValue
        typename = json["typename"].stringValue
        updatedAt = json["updated_at"].stringValue
        userType = json["user_type"].stringValue
        year = json["year"].stringValue
    }
}

