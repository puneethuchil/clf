//
//	Home.swift
//
//	Create by Puneeth Uchil on 15/1/2018
//	Copyright © 2018 Mobicom Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON
import Alamofire

class Home : NSObject{

	var articles : [Article]!
	var events : [Event]!
	var imageUrl : String!
    var spotlight : Spotlight!
	var subscription : [Subscription]!
    var jobs : [Job]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		articles = [Article]()
		let articlesArray = json["articles"].arrayValue
		for articlesJson in articlesArray{
			let value = Article(fromJson: articlesJson)
			articles.append(value)
		}
		events = [Event]()
		let eventsArray = json["events"].arrayValue
		for eventsJson in eventsArray{
			let value = Event(fromJson: eventsJson)
			events.append(value)
		}
		imageUrl = json["image_url"].stringValue
        let spotlightJson = json["spotlight"]
        if !spotlightJson.isEmpty{
            spotlight = Spotlight(fromJson: spotlightJson)
        }
        subscription = [Subscription]()
		let subscriptionArray = json["subscription"].arrayValue
		for subscriptionJson in subscriptionArray{
			let value = Subscription(fromJson: subscriptionJson)
			subscription.append(value)
		}
        jobs = [Job]()
        let jobsArray = json["jobs"].arrayValue
        for jobsArrayJson in jobsArray{
            let value = Job(fromJson: jobsArrayJson)
            jobs.append(value)
        }
	}
}


class HomeWebService:NSObject {
    override init() {
    }
    
    func fetchHomeData(completion: @escaping (Home?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"authHomeApi")!,
            method: .get,
            parameters: nil,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let homeObject = Home.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(homeObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }

        }
    }
    
    func fetchHomeDataNoLogin(completion: @escaping (Home?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"homeApi")!,
            method: .get,
            parameters: nil,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let homeObject = Home.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(homeObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                
        }
    }
    
}


