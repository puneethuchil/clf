//
//  Register.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/6/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class RegisterInfo{
    
    var clubCode : String!
    var clubs : String!
    var email : String!
    var fName : String!
    var id : String!
    var lName : String!
    var password : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubCode = json["club_code"].stringValue
        clubs = json["clubs"].stringValue
        email = json["email"].stringValue
        fName = json["f_name"].stringValue
        id = json["id"].stringValue
        lName = json["l_name"].stringValue
        password = json["password"].stringValue
    }
    
}

class RegisterWebService:NSObject {
    override init() {
    }
    /*
     {
     "f_name":"Amit",
     "l_name":"Jha",
     "clubs":"Platinum Circle",
     "club_code":"ABC11",
     "email":"amit@mobicom.com",
     "password":"amitjha"
     }
 
 */
    func register(username:String, password:String, fname:String, lname:String,clubname:String, clubCode:String,designation:String, completion: @escaping (RegisterInfo?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"signUp")!,
            method: .post,
            parameters: ["f_name":fname,
                         "l_name":lname,
                         "clubs":clubname,
                         "club_code":clubCode,
                         "current_designation":designation,
                         "email":username,
                         "password":password],
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = RegisterInfo.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}


