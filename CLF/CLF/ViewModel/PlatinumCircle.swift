//
//  PlatinumCircle.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import Alamofire

class Circle : NSObject{
    
    var imageUrl : String!
    var platinumCircle : [PlatinumCircle]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        imageUrl = json["image_url"].stringValue
        platinumCircle = [PlatinumCircle]()
        let platinumCircleArray = json["platinumCircle"].arrayValue
        for platinumCircleJson in platinumCircleArray{
            let value = PlatinumCircle(fromJson: platinumCircleJson)
            platinumCircle.append(value)
        }
    } 
}

class PlatinumCircle : NSObject{
    
    var createdAt : String!
    var descriptionField : String!
    var id : String!
    var image : String!
    var status : String!
    var title : String!
    var updatedAt : String!
    var website : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        updatedAt = json["updated_at"].stringValue
        website = json["website"].stringValue
    }
}

class PlatinumCircleWebService:NSObject {
    override init() {
    }
    
    func fetchCircleInfo( completion: @escaping (Circle?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"PlatinumCircleListApi")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let circleObject = Circle.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(circleObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
}
