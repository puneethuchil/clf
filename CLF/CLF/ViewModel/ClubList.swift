//
//  ClubList.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/7/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class ClubData{
    
    var clublist : [Club]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clublist = [Club]()
        let clubArray = json["club"].arrayValue
        for clubJson in clubArray{
            let value = Club(fromJson: clubJson)
            clublist.append(value)
        }
    }
    
}

class Club{
    
    var clubCode : String!
    var clubName : String!
    var clubTypeId : String!
    var country : String!
    var createdDate : String!
    var id : String!
    var location : String!
    var points : String!
    var rank : String!
    var status : String!
    var updatedDate : String!
    var website : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubCode = json["club_code"].stringValue
        clubName = json["club_name"].stringValue
        clubTypeId = json["club_type_id"].stringValue
        country = json["country"].stringValue
        createdDate = json["created_date"].stringValue
        id = json["id"].stringValue
        location = json["location"].stringValue
        points = json["points"].stringValue
        rank = json["rank"].stringValue
        status = json["status"].stringValue
        updatedDate = json["updated_date"].stringValue
    }
}


class ClubListWebService:NSObject {
    override init() {
    }
    
    func fetchTopClubList(completion: @escaping (ClubData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"clubListApi")!,
            method: .get,
            parameters: nil,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let topClubsObject = ClubData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(topClubsObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}





