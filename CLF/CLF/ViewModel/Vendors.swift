//
//  Vendors.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/25/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

import Foundation
import SwiftyJSON

class Product{
    
    var createdAt : String!
    var descriptionField : String!
    var id : String!
    var image : String!
    var intrested : Bool!
    var status : String!
    var title : String!
    var updatedAt : String!
    var vendorId : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        intrested = json["intrested"].boolValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        updatedAt = json["updated_at"].stringValue
        vendorId = json["vendor_id"].stringValue
    }
    
}

class ProductData{
    
    var imageUrl : String!
    var product : [Product]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        imageUrl = json["image_url"].stringValue
        product = [Product]()
        let vendorArray = json["vendor"].arrayValue
        for vendorJson in vendorArray{
            let value = Product(fromJson: vendorJson)
            product.append(value)
        }
    }
    
}

class Vendor{
    
    var createdAt : String!
    var descriptionField : String!
    var email : String!
    var id : String!
    var image : String!
    var mobile : String!
    var name : String!
    var status : String!
    var updatedAt : String!
    var website : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        email = json["email"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        mobile = json["mobile"].stringValue
        name = json["name"].stringValue
        status = json["status"].stringValue
        updatedAt = json["updated_at"].stringValue
        website = json["website"].stringValue
    }
    
}

class VendorData{
    
    var imageUrl : String!
    var vendors : [Vendor]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        imageUrl = json["image_url"].stringValue
        vendors = [Vendor]()
        let vendorsArray = json["vendors"].arrayValue
        for vendorsJson in vendorsArray{
            let value = Vendor(fromJson: vendorsJson)
            vendors.append(value)
        }
    }
    
}

class VendorWebService:NSObject {
    override init() {
    }
    
    func fetchVenderInfo( completion: @escaping (VendorData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"vendorList")!,
            method: .get,
            parameters: nil,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = VendorData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
        }
    }
    
    func fetchProductsForVendor(id:String, completion: @escaping (ProductData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        
        let userID = isLoggedIn == true ? currentUser.userId: "0"
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"vendorProductList/"+id+"/"+userID!)!,
            method: .get,
            parameters: nil,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = ProductData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
    func postInterest(id:String, completion: @escaping (CommentsData?) -> Void) {
        let userID:String = currentUser.userId
        LoadingView.sharedInstance.display(show: true)
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"productInterest/")!,
            method: .post,
            parameters: ["product_id":id,"user_id":userID],
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = CommentsData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }

}

