//
//  ArticleList.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/16/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import Alamofire


class ArticleList : NSObject{
    
    var articles : [Article]!
    var imageUrl : String!
    var pdfUrl : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        articles = [Article]()
        let articlesArray = json["articles"].arrayValue
        for articlesJson in articlesArray{
            let value = Article(fromJson: articlesJson)
            articles.append(value)
        }
        imageUrl = json["image_url"].stringValue
        pdfUrl = json["pdf_url"].stringValue
    }
}

class ArticleListWebService:NSObject {
    override init() {
    }
    
    func fetchArticleList(completion: @escaping (ArticleList?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"articleListApi/0")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let articleListObject = ArticleList.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(articleListObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}


