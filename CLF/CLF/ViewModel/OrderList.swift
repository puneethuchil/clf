//
//  OrderList.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/16/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class OrderData{
    
    var imageUrl : String!
    var orderList : [Order]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        imageUrl = json["image_url"].stringValue
        orderList = [Order]()
        let transactionArray = json["transaction"].arrayValue
        for transactionJson in transactionArray{
            let value = Order(fromJson: transactionJson)
            orderList.append(value)
        }
    }
}

class Order{
    
    var amount : String!
    var cretedAt : String!
    var id : String!
    var orderStatus : String!
    var paymentStatus : String!
    var paymentToken : String!
    var paymentType : String!
    var productType : String!
    var subscriptionId : String!
    var subscriptionTitle : String!
    var updatedAt : String!
    var userId : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amount = json["amount"].stringValue
        cretedAt = json["creted_at"].stringValue
        id = json["id"].stringValue
        orderStatus = json["order_status"].stringValue
        paymentStatus = json["payment_status"].stringValue
        paymentToken = json["payment_token"].stringValue
        paymentType = json["payment_type"].stringValue
        productType = json["product_type"].stringValue
        subscriptionId = json["subscription_id"].stringValue
        subscriptionTitle = json["subscription_title"].stringValue
        updatedAt = json["updated_at"].stringValue
        userId = json["user_id"].stringValue
    }

}

class OrderWebService:NSObject {
    override init() {
    }
    
    func fetchOrders(completion: @escaping (OrderData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"userTransaction/"+currentUser.userId)!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let profileObject = OrderData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(profileObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}

