//
//  Profile.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/18/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Profile : NSObject{
    
    var aboutMe : String!
    var androidDeviceId : String!
    var avatar : String!
    var clubs : String!
    var contactInfo : String!
    var cretedAt : String!
    var currentDesignation : String!
    var email : String!
    var emailVerify : String!
    var fName : String!
    var id : String!
    var imageUrl : String!
    var interest : String!
    var iosDeviceId : String!
    var lName : String!
    var mName : String!
    var password : String!
    var pastDesignation : String!
    var platinumStatus : String!
    var relogin : String!
    var skills : String!
    var status : String!
    var updatedAt : String!
    var userType : String!
    var username : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        aboutMe = json["about_me"].stringValue
        androidDeviceId = json["android_device_id"].stringValue
        avatar = json["avatar"].stringValue
        clubs = json["clubs"].stringValue
        contactInfo = json["contact_info"].stringValue
        cretedAt = json["creted_at"].stringValue
        currentDesignation = json["current_designation"].stringValue
        email = json["email"].stringValue
        emailVerify = json["email_verify"].stringValue
        fName = json["f_name"].stringValue
        id = json["id"].stringValue
        imageUrl = json["image_url"].stringValue
        interest = json["interest"].stringValue
        iosDeviceId = json["ios_device_id"].stringValue
        lName = json["l_name"].stringValue
        mName = json["m_name"].stringValue
        password = json["password"].stringValue
        pastDesignation = json["past_designation"].stringValue
        platinumStatus = json["platinum_status"].stringValue
        relogin = json["relogin"].stringValue
        skills = json["skills"].stringValue
        status = json["status"].stringValue
        updatedAt = json["updated_at"].stringValue
        userType = json["user_type"].stringValue
        username = json["username"].stringValue
    }
}

class ProfileWebService:NSObject {
    override init() {
    }
    
    func fetchProfileInfo(completion: @escaping (Profile?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"userProfile/"+currentUser.userId)!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let profileObject = Profile.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(profileObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
    func editProfile(imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        LoadingView.sharedInstance.display(show: true)

        let url = APPURL.BaseURL+"profileUpdate/"+currentUser.userId /* your API url */
        
        let headers : HTTPHeaders = ["Content-Type":"multipart/form-data",
                                     "compatibility-version":"1",
                                     "authorization":currentUser.accessToken,
                                     "server-ts":currentUser.serverts]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    LoadingView.sharedInstance.display(show: false)
                    if let err = response.error{
                        ToastView.sharedInstance.showAlert(message: ("Error while uploading!"))
                        onError?(err)
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                ToastView.sharedInstance.showAlert(message: (error.localizedDescription))
                LoadingView.sharedInstance.display(show: false)
                onError?(error)
            }
        }
    }
    
}



