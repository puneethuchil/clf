//
//  Job.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/15/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation 
import SwiftyJSON


class Job : NSObject{
    
    var city : String!
    var countryId : String!
    var countrycode : String!
    var countryname : String!
    var createdAt : String!
    var descriptionField : String!
    var endDate : String!
    var id : String!
    var image : String!
    var startDate : String!
    var status : String!
    var title : String!
    var updatedAt : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        city = json["city"].stringValue
        countryId = json["country_id"].stringValue
        countrycode = json["countrycode"].stringValue
        countryname = json["countryname"].stringValue
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        endDate = json["end_date"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        startDate = json["start_date"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        updatedAt = json["updated_at"].stringValue
    }   
}

