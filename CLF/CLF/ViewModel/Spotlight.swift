//
//	Spotlight.swift
//
//	Create by Puneeth Uchil on 15/1/2018
//	Copyright © 2018 Mobicom Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Spotlight{
    
    var clubArray : [SpotlightItem]!
    var managerArray : [SpotlightItem]!
    var vendorArray : [SpotlightItem]!

    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubArray = [SpotlightItem]()
        let cLUBArray = json["CLUB"].arrayValue
        for cLUBJson in cLUBArray{
            let value = SpotlightItem(fromJson: cLUBJson)
            clubArray.append(value)
        }
        managerArray = [SpotlightItem]()
        let mANAGERArray = json["MANAGER"].arrayValue
        for mANAGERJson in mANAGERArray{
            let value = SpotlightItem(fromJson: mANAGERJson)
            managerArray.append(value)
        }
        vendorArray = [SpotlightItem]()
        let mVENDORArray = json["CLUB"].arrayValue
        for mVENDORJson in mVENDORArray{
            let value = SpotlightItem(fromJson: mVENDORJson)
            value.type = "VENDOR"
            vendorArray.append(value)
        }
    }
    
}

class SpotlightItem : NSObject{
    
    var clubId : String!
    var createdAt : String!
    var descriptionField : String!
    var designation : String!
    var id : String!
    var image : String!
    var name : String!
    var pid : String!
    var spotlightType : String!
    var status : String!
    var title : String!
    var type : String!
    var updatedAt : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        clubId = json["club_id"].stringValue
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        designation = json["designation"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        pid = json["pid"].stringValue
        spotlightType = json["spotlight_type"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        type = json["type"].stringValue
        updatedAt = json["updated_at"].stringValue
    }
}
