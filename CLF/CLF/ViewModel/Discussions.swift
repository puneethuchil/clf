//
//  Discussions.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/23/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Discussion : NSObject{
    
    var avatar : String!
    var comment : [Comment]!
    var descriptionField : String!
    var fName : String!
    var id : String!
    var title : String!
    var userId : String!
    var clubs : String!
    var current_designation : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        avatar = json["avatar"].stringValue
        comment = [Comment]()
        let commentArray = json["comment"].arrayValue
        for commentJson in commentArray{
            let value = Comment(fromJson: commentJson)
            comment.append(value)
        }
        descriptionField = json["description"].stringValue
        fName = json["f_name"].stringValue
        id = json["id"].stringValue
        title = json["title"].stringValue
        userId = json["user_id"].stringValue
        clubs = json["clubs"].stringValue
        current_designation = json["current_designation"].stringValue

    }
}

class DiscussionData : NSObject{
    
    var discussion : [Discussion]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        discussion = [Discussion]()
        let discussionArray = json["discussion"].arrayValue
        for discussionJson in discussionArray{
            let value = Discussion(fromJson: discussionJson)
            discussion.append(value)
        }
    }
}

class Conversations : NSObject{
    
    var descriptionField : String!
    var id : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        title = json["title"].stringValue
    }
}

class CommentsData : NSObject{
    
    var descriptionField : String!
    var id : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
    }
}

class Comment{
    
    var avatar : String!
    var clubs : String!
    var createdAt : String!
    var currentDesignation : String!
    var descriptionField : String!
    var fName : String!
    var id : String!
    var title : String!
    var updatedAt : String!
    var userId : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        avatar = json["avatar"].stringValue
        clubs = json["clubs"].stringValue
        createdAt = json["created_at"].stringValue
        currentDesignation = json["current_designation"].stringValue
        descriptionField = json["description"].stringValue
        fName = json["f_name"].stringValue
        id = json["id"].stringValue
        title = json["title"].stringValue
        updatedAt = json["updated_at"].stringValue
        userId = json["user_id"].stringValue
    }
}


class CommentList{
    
    var commentList : [Comment]!
    var imageUrl : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        commentList = [Comment]()
        let commentArray = json["comment"].arrayValue
        for commentJson in commentArray{
            let value = Comment(fromJson: commentJson)
            commentList.append(value)
        }
        imageUrl = json["image_url"].stringValue
    }
    
}




class DiscussionViewModel:NSObject {
    override init() {
    }
    
    func postDiscussion(title:String, description:String, completion: @escaping (Conversations?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        let userID = currentUser.userId

        Alamofire.request(URL(string:APPURL.BaseURL+"userDiscussion/"+userID!)!,
                          method: .post,
                          parameters: ["title":title,"description":description],
                          encoding: JSONEncoding.default,
                          headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let discussionObject = Conversations.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(discussionObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
    func fetchPostedDiscussions( completion: @escaping (DiscussionData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"userDiscussion")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = DiscussionData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
    func postComments(description:String,id:String, completion: @escaping (CommentsData?) -> Void) {
        let userID = currentUser.userId
        
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"userComment/"+userID!+"/"+id)!,
            method: .post,
            parameters: ["description":description],
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = CommentsData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
    func fetchComments(id:String, completion: @escaping (CommentList?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"userDiscussion/"+id)!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = CommentList.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}

