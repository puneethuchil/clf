//
//  Services.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/23/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class SubscriptionDetailData{
    
    var subscriptionDetail : [SubscriptionDetail]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        subscriptionDetail = [SubscriptionDetail]()
        let subscriptionDetailArray = json["subscriptionDetail"].arrayValue
        for subscriptionDetailJson in subscriptionDetailArray{
            let value = SubscriptionDetail(fromJson: subscriptionDetailJson)
            subscriptionDetail.append(value)
        }
    }
    
}

class SubscriptionDetail{
    
    var pCOA : [Service]!
    var pCOW : [Service]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        pCOA = [Service]()
        let pCOAArray = json["PCOA"].arrayValue
        for pCOAJson in pCOAArray{
            let value = Service(fromJson: pCOAJson)
            pCOA.append(value)
        }
        pCOW = [Service]()
        let pCOWArray = json["PCOW"].arrayValue
        for pCOWJson in pCOWArray{
            let value = Service(fromJson: pCOWJson)
            pCOW.append(value)
        }
    }
    
}

class Service{
    
    var categoryId : String!
    var cretedAt : String!
    var descriptionField : String!
    var id : String!
    var pdfPath : [PdfPath]!
    var pricearray : [PriceData]!
    var status : String!
    var title : String!
    var type : String!
    var webviewheight : String!
    var collectionviewheight : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        categoryId = json["category_id"].stringValue
        cretedAt = json["creted_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        pdfPath = [PdfPath]()
        let pdfPathArray = json["pdf_path"].arrayValue
        for pdfPathJson in pdfPathArray{
            let value = PdfPath(fromJson: pdfPathJson)
            pdfPath.append(value)
        }
        pricearray = [PriceData]()
        let priceAr = json["pricearray"].arrayValue
        for priceJson in priceAr{
            let value = PriceData(fromJson: priceJson)
            pricearray.append(value)
        }
        
        status = json["status"].stringValue
        title = json["title"].stringValue
        type = json["type"].stringValue
        webviewheight = "70.0"
        collectionviewheight = "50.0"
    }
    
}

class PdfPath{
    
    var id : String!
    var month : String!
    var pdfPath : String!
    var status : String!
    var subscriptionId : String!
    var year : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        month = json["month"].stringValue
        pdfPath = json["pdf_path"].stringValue
        status = json["status"].stringValue
        subscriptionId = json["subscription_id"].stringValue
        year = json["year"].stringValue
    }
}

    class PriceData{
        
        var year : String!
        var price : String!
        
        /**
         * Instantiate the instance using the passed json values to set the properties values
         */
        init(fromJson json: JSON!){
            if json.isEmpty{
                return
            }
            price = json["price"].stringValue
            year = json["duration_year"].stringValue
        }
}

class SubscriptionListData{
    
    var subscriptionList : [SubscriptionList]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        subscriptionList = [SubscriptionList]()
        let subscriptionListArray = json["subscriptionList"].arrayValue
        for subscriptionListJson in subscriptionListArray{
            let value = SubscriptionList(fromJson: subscriptionListJson)
            subscriptionList.append(value)
        }
    }
    
}

class SubscriptionList{
    
    var descriptionField : String!
    var id : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        title = json["title"].stringValue
    }
    
}


class PlatinumServicesWebService:NSObject {
    override init() {
    }
    
    func fetchPlatinumServices( completion: @escaping (SubscriptionListData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"subscriptionListData")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = SubscriptionListData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
    func fetchPlatinumServicesDetail(serviceID:String, completion: @escaping (SubscriptionDetail?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        Alamofire.request(
            URL(string:APPURL.BaseURL+"subscriptionDetailById/"+serviceID)!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = SubscriptionDetailData.init(fromJson: swiftyJsonVar["data"])
                    let object:SubscriptionDetail = mObject.subscriptionDetail[0]
                    LoadingView.sharedInstance.display(show: false)
                    completion(object)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
}

