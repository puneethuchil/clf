//
//  ExclusiveEvents.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/24/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
//
//    Past.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import Alamofire

class EventsList : NSObject{
    
    var future : [Event]!
    var past : [Event]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        future = [Event]()
        let futureArray = json["future"].arrayValue
        for futureJson in futureArray{
            let value = Event(fromJson: futureJson)
            future.append(value)
        }
        past = [Event]()
        let pastArray = json["past"].arrayValue
        for pastJson in pastArray{
            let value = Event(fromJson: pastJson)
            past.append(value)
        }
    }
}

class ExclusiveEventsData : NSObject{
    
    var eventsList : EventsList!
    var imageUrl : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        let eventsListJson = json["eventsList"]
        if !eventsListJson.isEmpty{
            eventsList = EventsList(fromJson: eventsListJson)
        }
        imageUrl = json["image_url"].stringValue
    }
}

class ExclusiveEventsWebService:NSObject {
    override init() {
    }
    func fetchAllExclusiveEvents( completion: @escaping (ExclusiveEventsData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"exclusiveEventListApi")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let ExclusiveEventsDataObject = ExclusiveEventsData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(ExclusiveEventsDataObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
}



