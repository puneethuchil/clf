//
//  OfferProducts.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/15/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class CommerceProduct{
    
    var brand : String!
    var createdAt : String!
    var descriptionField : String!
    var id : String!
    var image : String!
    var price : String!
    var productid : String!
    var status : String!
    var title : String!
    var updatedAt : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        brand = json["brand"].stringValue
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        price = json["price"].stringValue
        productid = json["productid"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        updatedAt = json["updated_at"].stringValue
    }
}

class CommerceData{
    
    var productList : [CommerceProduct]!
    var imageUrl : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        productList = [CommerceProduct]()
        let commerceArray = json["commerce"].arrayValue
        for commerceJson in commerceArray{
            let value = CommerceProduct(fromJson: commerceJson)
            productList.append(value)
        }
        imageUrl = json["image_url"].stringValue
    }
    
}

class CommerceWebService:NSObject {
    override init() {
    }
    
    func fetchProducts(completion: @escaping (CommerceData?) -> Void) {
        LoadingView.sharedInstance.display(show: true)
        
        Alamofire.request(
            URL(string:APPURL.BaseURL+"commerceListApi")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let profileObject = CommerceData.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(profileObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}


