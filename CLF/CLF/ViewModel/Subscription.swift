//
//	Subscription.swift
//
//	Create by Puneeth Uchil on 15/1/2018
//	Copyright © 2018 Mobicom Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Subscription{
    
    var descriptionField : String!
    var id : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        title = json["title"].stringValue
    }
    
}
