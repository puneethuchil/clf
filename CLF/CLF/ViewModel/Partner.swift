//
//  Partner.swift
//  CLF
//
//  Created by Puneeth Uchil on 1/22/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//
//
//
//    Data.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import Alamofire

class PlatinumPartners : NSObject, NSCoding{
    
    var imageUrl : String!
    var partnerList : PartnerList!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        imageUrl = json["image_url"].stringValue
        let partnerListJson = json["partnerList"]
        if !partnerListJson.isEmpty{
            partnerList = PartnerList(fromJson: partnerListJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if partnerList != nil{
            dictionary["partnerList"] = partnerList.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        partnerList = aDecoder.decodeObject(forKey: "partnerList") as? PartnerList
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if partnerList != nil{
            aCoder.encode(partnerList, forKey: "partnerList")
        }
        
    }
    
}

class PartnerList : NSObject, NSCoding{
    
    var gold : [Partner]!
    var platinum : [Partner]!
    var silver : [Partner]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        gold = [Partner]()
        let goldArray = json["gold"].arrayValue
        for goldJson in goldArray{
            let value = Partner(fromJson: goldJson)
            gold.append(value)
        }
        platinum = [Partner]()
        let platinumArray = json["platinum"].arrayValue
        for platinumJson in platinumArray{
            let value = Partner(fromJson: platinumJson)
            platinum.append(value)
        }
        silver = [Partner]()
        let silverArray = json["silver"].arrayValue
        for silverJson in silverArray{
            let value = Partner(fromJson: silverJson)
            silver.append(value)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if gold != nil{
            var dictionaryElements = [[String:Any]]()
            for goldElement in gold {
                dictionaryElements.append(goldElement.toDictionary())
            }
            dictionary["gold"] = dictionaryElements
        }
        if platinum != nil{
            var dictionaryElements = [[String:Any]]()
            for platinumElement in platinum {
                dictionaryElements.append(platinumElement.toDictionary())
            }
            dictionary["platinum"] = dictionaryElements
        }
        if silver != nil{
            dictionary["silver"] = silver
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        gold = aDecoder.decodeObject(forKey: "gold") as? [Partner]
        platinum = aDecoder.decodeObject(forKey: "platinum") as? [Partner]
        silver = aDecoder.decodeObject(forKey: "silver") as? [Partner]
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if gold != nil{
            aCoder.encode(gold, forKey: "gold")
        }
        if platinum != nil{
            aCoder.encode(platinum, forKey: "platinum")
        }
        if silver != nil{
            aCoder.encode(silver, forKey: "silver")
        }
        
    }
    
}

class Partner : NSObject, NSCoding{
    
    var cretedAt : String!
    var id : String!
    var image : String!
    var name : String!
    var status : String!
    var type : String!
    var updatedAt : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        cretedAt = json["creted_at"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        status = json["status"].stringValue
        type = json["type"].stringValue
        updatedAt = json["updated_at"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cretedAt != nil{
            dictionary["creted_at"] = cretedAt
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if name != nil{
            dictionary["name"] = name
        }
        if status != nil{
            dictionary["status"] = status
        }
        if type != nil{
            dictionary["type"] = type
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cretedAt = aDecoder.decodeObject(forKey: "creted_at") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if cretedAt != nil{
            aCoder.encode(cretedAt, forKey: "creted_at")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        
    }
}

class PlatinumPartnerWebService:NSObject {
    override init() {
    }
    
    func fetchPlatinumPartners( completion: @escaping (PlatinumPartners?) -> Void) {
        LoadingView.sharedInstance.display(show: true)

        Alamofire.request(
            URL(string:APPURL.BaseURL+"partnerList")!,
            method: .get,
            parameters: nil,
            headers:headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let platinumPartnersObject = PlatinumPartners.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(platinumPartnersObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
}
