//
//  Login.swift
//  CLF
//
//  Created by Puneeth Uchil on 11/20/17.
//  Copyright © 2017 MobiCom Technologies. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class UserInfo:NSObject, NSCoding{
    
    var accessToken : String!
    var avatar : String!
    var clubs : String!
    var currentDesignation : String!
    var imageUrl : String!
    var refreshToken : String!
    var serverts : String!
    var tokenType : String!
    var userEmail : String!
    var userFirstName : String!
    var userId : String!
    var userLastName : String!
    var userMiddleName : String!
    var userName : String!
    var userType : String!

    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        accessToken = json["access_token"].stringValue
        avatar = json["avatar"].stringValue
        clubs = json["clubs"].stringValue
        currentDesignation = json["current_designation"].stringValue
        imageUrl = json["image_url"].stringValue
        refreshToken = json["refresh_token"].stringValue
        serverts = json["server-ts"].stringValue
        tokenType = json["token_type"].stringValue
        userEmail = json["user_email"].stringValue
        userFirstName = json["user_first_name"].stringValue
        userId = json["user_id"].stringValue
        userLastName = json["user_last_name"].stringValue
        userMiddleName = json["user_middle_name"].stringValue
        userName = json["user_name"].stringValue
        userType = json["user_type"].stringValue

        
        UserDefaults.standard.set(accessToken, forKey: "accessToken")
    }
    
    @objc required init(coder aDecoder: NSCoder)
    {
        accessToken = aDecoder.decodeObject(forKey: "access_token") as? String
        avatar = aDecoder.decodeObject(forKey: "avatar") as? String
        clubs = aDecoder.decodeObject(forKey: "clubs") as? String
        currentDesignation = aDecoder.decodeObject(forKey: "current_designation") as? String
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        refreshToken = aDecoder.decodeObject(forKey: "refresh_token") as? String
        serverts = aDecoder.decodeObject(forKey: "server-ts") as? String
        tokenType = aDecoder.decodeObject(forKey: "token_type") as? String
        userEmail = aDecoder.decodeObject(forKey: "user_email") as? String
        userFirstName = aDecoder.decodeObject(forKey: "user_first_name") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
        userLastName = aDecoder.decodeObject(forKey: "user_last_name") as? String
        userMiddleName = aDecoder.decodeObject(forKey: "user_middle_name") as? String
        userName = aDecoder.decodeObject(forKey: "user_name") as? String
        userType = aDecoder.decodeObject(forKey: "user_type") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if accessToken != nil{
            aCoder.encode(accessToken, forKey: "access_token")
        }
        if avatar != nil{
            aCoder.encode(avatar, forKey: "avatar")
        }
        if clubs != nil{
            aCoder.encode(clubs, forKey: "clubs")
        }
        if currentDesignation != nil{
            aCoder.encode(currentDesignation, forKey: "current_designation")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if refreshToken != nil{
            aCoder.encode(refreshToken, forKey: "refresh_token")
        }
        if serverts != nil{
            aCoder.encode(serverts, forKey: "server-ts")
        }
        if tokenType != nil{
            aCoder.encode(tokenType, forKey: "token_type")
        }
        if userEmail != nil{
            aCoder.encode(userEmail, forKey: "user_email")
        }
        if userFirstName != nil{
            aCoder.encode(userFirstName, forKey: "user_first_name")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        if userLastName != nil{
            aCoder.encode(userLastName, forKey: "user_last_name")
        }
        if userMiddleName != nil{
            aCoder.encode(userMiddleName, forKey: "user_middle_name")
        }
        if userName != nil{
            aCoder.encode(userName, forKey: "user_name")
        }
        if userType != nil{
            aCoder.encode(userType, forKey: "user_type")
        }
        
    }
    
}



class LoginWebService:NSObject {
    override init() {
    }
    
    func performLogin(username:String, password:String, completion: @escaping (UserInfo?) -> Void) {
        
        LoadingView.sharedInstance.display(show: true)

        let headers : HTTPHeaders = [ "Content-Type":"application/json","compatibility-version":"1"]

        Alamofire.request(
            URL(string:APPURL.BaseURL+"login")!,
            method: .post,
            parameters: ["email":username,
                         "password":password,
                         "deviceid":"1",
                         "deviceos":"54543hb4344bj42343k4k3b3knk32432",
                         "locale":"en"],
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                    return
                }
                
                let swiftyJsonVar = JSON(response.result.value!)
                
                if swiftyJsonVar["status"].stringValue == "error"{
                    if response.response?.statusCode == 403 {
                        
                    }
                        
                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
                else if let resData = swiftyJsonVar["data"].dictionaryObject {
                    print(resData)
                    let mObject = UserInfo.init(fromJson: swiftyJsonVar["data"])
                    LoadingView.sharedInstance.display(show: false)
                    completion(mObject)
                }
                else{
                    LoadingView.sharedInstance.display(show: false)
                    completion(nil)
                }
        }
    }
    
}


