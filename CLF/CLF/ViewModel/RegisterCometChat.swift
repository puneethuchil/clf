//
//  RegisterCometChat.swift
//  CLF
//
//  Created by Puneeth Uchil on 2/3/18.
//  Copyright © 2018 MobiCom Technologies. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class ChatUser{
    
    var success : Succes!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        let successJson = json["success"]
        if !successJson.isEmpty{
            success = Succes(fromJson: successJson)
        }
    }
    
}
class Succes{
    
    var message : String!
    var status : String!
    var userid : Int!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        status = json["status"].stringValue
        userid = json["userid"].intValue
    }
    
}
class CometChatWebService:NSObject {
    override init() {
    }
    
    /*
     https://clfchat.platinumclubnet.com/cometchat/api/index.php?action=createuser&api-key=f76e7e423cda8d4884f73d15fecb22f8&username=Puneeth&password=1234567&link=""&avatar=""&displayname=""&callbackfn:mobileapp
    */
    
    
    let urlToRequest = "https://clfchat.platinumclubnet.com/cometchat/api/index.php"
    
    func register(username:String, password:String, completion: @escaping (Bool) -> ()) {
        print("First line of code executed")
        let url4 = URL(string: urlToRequest)!
        let session4 = URLSession.shared
        let request = NSMutableURLRequest(url: url4)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let paramString = "action=createuser&api-key=f76e7e423cda8d4884f73d15fecb22f8&username="+username+"&password="+password+"&link=''&avatar=''&displayname=''&callbackfn=mobileapp"
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        let task = session4.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                print("*****error")
                completion(false)
                return
            }
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("*****This is the data 4: \(String(describing: dataString))") //JSONSerialization
            completion(true)
            return
        }
        task.resume()
        

    }
    
    
//    func register(username:String, password:String, completion: @escaping (ChatUser?) -> Void) {
//        
//        let headers : HTTPHeaders = [ "Content-Type":"application/x-www-form-urlencoded"]
//        let action = "createuser"
//        let callbackfn = "mobileapp"
//        let apiKey = "f76e7e423cda8d4884f73d15fecb22f8"
//        
//        Alamofire.request(
//            URL(string:APPURL.CommetChatURL)!,
//            method: .post,
//            parameters: ["action":action,
//                         "api-key":apiKey,
//                         "username":username,
//                         "password":password,
//                         "displayname":username,
//                         "callbackfn":callbackfn],
//            encoding: JSONEncoding.default,
//            headers: headers)
//            .validate()
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    ToastView.sharedInstance.showAlert(message: "Error while fetching :\(String(describing: response.result.error))")
//                    LoadingView.sharedInstance.display(show: false)
//                    completion(nil)
//                    return
//                }
//                let swiftyJsonVar = JSON(response.result.value!)
//
//                if swiftyJsonVar["status"].stringValue == "error"{
//                    ToastView.sharedInstance.showAlert(message: swiftyJsonVar["message"].stringValue)
//                    completion(nil)
//                }
//                else if let resData = swiftyJsonVar["data"].dictionaryObject {
//                    print(resData)
//                    let mObject = ChatUser.init(fromJson: swiftyJsonVar["data"])
//                    LoadingView.sharedInstance.display(show: false)
//                    completion(mObject)
//                }
//                else{
//                    LoadingView.sharedInstance.display(show: false)
//                    completion(nil)
//                }
//        }
//    }
    
}


