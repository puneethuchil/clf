//
//  readyUIFIle.h
//  cometchat-ui
//
//  Created by Admin1 on 22/06/17.
//  Copyright © 2017 Inscripts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import AVFoundation;

@interface readyUIFIle : NSObject<AVAudioPlayerDelegate>

/* Launch CometChat UI */
-(void)launchCometChat:(BOOL)isFullScreen
              observer:(UIViewController *)currentView
              userInfo:(void (^)(NSDictionary *response))userInfo
             groupInfo:(void (^)(NSDictionary *response))groupInfo
      onMessageReceive:(void (^)(NSDictionary *response))onMessageReceive
               success:(void (^)(NSDictionary *response))success
               failure:(void (^)(NSError *error))failure
              onLogout:(void (^)(NSDictionary *response))onLogout;


/* Launch 'chat view' based on userid/groupid */
-(void)launchCometChat:(NSString *)groupUserId
               isGroup:(BOOL)isGroup
          isFullScreen:(BOOL)isFullScreen
              observer:(UIViewController *)currentView
         setBackButton:(BOOL)flag
              userInfo:(void (^)(NSDictionary *response))userInfo
             groupInfo:(void (^)(NSDictionary *response))groupInfo
      onMessageReceive:(void (^)(NSDictionary *response))onMessageReceive
               success:(void (^)(NSDictionary *response))success
               failure:(void (^)(NSError *error))failure
              onLogout:(void (^)(NSDictionary *response))onLogout;

@end
