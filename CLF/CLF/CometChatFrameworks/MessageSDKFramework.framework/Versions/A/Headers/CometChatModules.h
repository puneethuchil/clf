//
//  CometChatModules.h
//  CometChat
//
//  Created by Inscripts on 18/11/14.
//  Copyright (c) 2016 Inscripts
//  License: https://www.cometchat.com/legal/license
//

#import <Foundation/Foundation.h>

@interface CometChatModules : NSObject
+ (BOOL)isOneOnOneModuleEnabled;
+ (BOOL)isChatroomModuleEnabled;
+ (BOOL)isAnnouncementModuleEnabled;
+ (BOOL)isHomePageModuleEnabled;
+ (BOOL)isSinglePlayerGameEnabled;
+ (BOOL)isBroadcastMessageEnabled;
+ (BOOL)isBotsModuleEnabled;
+ (BOOL)isRecentModuleEnabled;
@end
