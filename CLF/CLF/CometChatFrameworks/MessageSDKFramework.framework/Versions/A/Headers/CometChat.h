/*
 
 CometChat
 Copyright (c) 2015 Inscripts
 
 CometChat ('the Software') is a copyrighted work of authorship. Inscripts
 retains ownership of the Software and any copies of it, regardless of the
 form in which the copies may exist. This license is not a sale of the
 original Software or any copies.
 
 By installing and using CometChat on your server, you agree to the following
 terms and conditions. Such agreement is either on your own behalf or on behalf
 of any corporate entity which employs you or which you represent
 ('Corporate Licensee'). In this Agreement, 'you' includes both the reader
 and any Corporate Licensee and 'Inscripts' means Inscripts (I) Private Limited:
 
 CometChat license grants you the right to run one instance (a single installation)
 of the Software on one web server and one web site for each license purchased.
 Each license may power one instance of the Software on one domain. For each
 installed instance of the Software, a separate license is required.
 The Software is licensed only to you. You may not rent, lease, sublicense, sell,
 assign, pledge, transfer or otherwise dispose of the Software in any form, on
 a temporary or permanent basis, without the prior written consent of Inscripts.
 
 The license is effective until terminated. You may terminate it
 at any time by uninstalling the Software and destroying any copies in any form.
 
 The Software source code may be altered (at your risk)
 
 All Software copyright notices within the scripts must remain unchanged (and visible).
 
 The Software may not be used for anything that would represent or is associated
 with an Intellectual Property violation, including, but not limited to,
 engaging in any activity that infringes or misappropriates the intellectual property
 rights of others, including copyrights, trademarks, service marks, trade secrets,
 software piracy, and patents held by individuals, corporations, or other entities.
 
 If any of the terms of this Agreement are violated, Inscripts reserves the right
 to revoke the Software license at any time.
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

#import <Foundation/Foundation.h>

/* Enum for Changing user status */
typedef NS_ENUM (NSInteger, STATUS_OPTIONS) {
    
    STATUS_AVAILABLE,
    STATUS_BUSY,
    STATUS_INVISIBLE,
    STATUS_OFFLINE,
    STATUS_AWAY
};

/* Enum for Primary Colors */
typedef NS_ENUM (NSInteger, PRIMARY_COLOR) {
    
    COLOR_PRIMARY,
    COLOR_PRIMARY_DARK,
    COLOR_PRIMARY_STRING,
    COLOR_PRIMARY_ACTION_BAR,
    COLOR_PRIMARY_LOGIN_BUTTON
    
};

/* Enum for Chatroom Types */
typedef NS_ENUM (NSInteger, GROUP_TYPES) {
    
    PUBLIC_GROUP,
    PASSWORD_PROTECTED_GROUP,
    INVITE_ONLY_GROUP
};

/* Enum for Featire Status */
typedef NS_ENUM (NSInteger, ISFEATURED_ENABLED) {
    
    
    RECENT_CHAT_ENABLED,
    SINGLE_CHAT_ENABLED,
    GROUPCHAT_ENABLED,
    LAST_SEEN_ENABLED,
    
    SHOW_TICKS,
    BROADCAST_MESSAGE_ENABLED,
    FILE_TRANSFER_ENABLED,
    AV_BROADCAST_ENABLED,
    
    CCWHITEBOARD_ENABLED,
    CCWRITEBOARD_ENABLED,
    CCHANDWRITE_ENABLED,
    
    STICKER_ENABLED,
    EMOJI_ENABLED,
    VIDEO_CALL_ENABLED,
    AUDIO_CALL_ENABLED,
    BLOCKED_USER_ENABLED,
    CLEAR_CONVERSATION_ENABLED,
    GRP_FILE_TRANSFER_ENABLED,
    GRP_AV_BROADCAST_ENABLED,
    GRP_WHITEBOARD_ENABLED,
    GRP_WRITEBOARD_ENABLED,
    GRP_HANDWRITE_ENABLED,
    GRP_STICKER_ENABLED,
    GRP_EMOJI_ENABLED,
    GRP_VIDEO_CALL_ENABLED,
    GRP_AUDIO_CALL_ENABLED,
    GRP_CLEAR_CONVERSATION_ENABLED,
    GRP_COLOR_YOUR_TEXT,
    ANNOUNCEMENT_ENABLED ,
    DISPLAY_ALL_USERS,
    COMETSERVICE_ENABLED,
    AUDIOCONFERENCE_ENABLED,
    AVBROADCAST_ENABLED,
    AVCONFERENCE_ENABLED,
    BOTS_ENABLED,
    BROADCASTMESSAGE_ENABLED,
    CRAVBROADCAST_ENABLED,
    CRHANDWRITE_ENABLED,
    CRHIDEUSERCOUNT,
    CRPERSONAL_CHAT,
    CRSMILIES_ENABLED,
    CCSMILIES_ENABLED,
    STICKERS_ENABLED,
    GRP_STICKERS_ENABLED,
    CRTEXTCOLOR_ENABLED,
    CRWHITEBOARD_ENABLED,
    WRITEBOARD_PLUGIN_ENABLED,
    DISABLECONTACTSTAB,
    DISABLERECENTTAB,
    INVITE_VIA_SMS,
    MAXHEARTBEAT,
    MINHEARTBEAT,
    ONEONE_ENABLED,
    RECEIPTS_ENABLED,
    RTT_KEY,
    SHARE_THIS_APP,
    GAMES_ENABLED,
    TYPING_ENABLED,
    GUEST_ENABLED
    
    
};


/* Enum for CometChat Language */
typedef NS_ENUM (NSInteger, CC_LANGUAGE) {
    
    
    LANG_RECENT_TAB,
    LANG_CONTACT_TAB,
    LANG_GROUP_TAB,
    LANG_CREATE_NEW_GROUP,
    LANG_BROADCAST_MESSAGE,
    LANG_VIEW_PROFILE,
    LANG_CLEAR_CONVERSATION,
    LANG_CLEAR_CONVERSATION_MESSAGE,
    LANG_REPORT_CONVERSATION,
    LANG_BLOCK_USER,
    LANG_WHITEBOARD,
    LANG_WRITEBOARD,
    LANG_HANDWRITE,
    LANG_CAPTURE_PHOTO,
    LANG_CAPTURE_VIDEO,
    LANG_REPORT_CONVERSATION_DIALOG_TITLE,
    LANG_REPORT_CONVERSATION_EMPTY,
    LANG_VIEW_MEMBER,
    LANG_INVITE_USERS,
    LANG_COLOR_YOUR_TEXT,
    LANG_LEAVE_ROOM,
    LANG_COLLABORATIVE_DOCUMENT,
    LANG_TYPE_YOUR_MESSAGE,
    LANG_CANCEL,LANG_SEND,
    LANG_RECORDING,LANG_BOTS,
    LANG_CHAT_SETTINGS,
    LANG_NOTIFICATION_SETTINGS,
    LANG_BLOCKED_USERS,LANG_GAMES,
    LANG_SHARE_APP,LANG_LOGOUT,
    LANG_STATUS_MESSAGE,
    LANG_ONLINE_STATUS,LANG_OK,
    LANG_SET_STATUS,LANG_NO_BOTS,
    LANG_NOTIFICATION_TITLE,LANG_SHOW_NOTIFICATIONS,
    LANG_SOUND,
    LANG_VIBRATE,
    LANG_READ_RECEIPT_SETTING,
    LANG_LAST_SEEN_SETTING,
    LANG_READ_TICK_TEXT,
    LANG_LAST_SEEN_MESSAGE,
    LANG_NO_BLOCKED_USERS,
    LANG_INVITE_CONTACT_HINT,
    LANG_INVITE,
    LANG_INVITE_YOUR_FRIENDS,
    LANG_TO,
    INVITE_MESSAGE,
    LANG_VIDEO_CALL,
    LANG_AUDIO_CALL,
    LANG_USER_NAME,
    LANG_MORE,
    LANG_NAME,
    LANG_TYPE,
    LANG_PUBLIC_GROUP,
    LANG_PASS_PROTECTED_GROUP,
    LANG_INVITATION_ONLY_GROUP,
    LANG_ENTER_PASSWORD,
    LANG_REPORT,
    LANG_REASON,
    LANG_DONT_HAVE_ACCOUNT,
    LANG_TRYTOREGISTER,
    LANG_TRYASGUEST,
    LANG_GUEST_NAME,
    LANG_USERNAME,
    LANG_PASSWORD,
    LANG_REMEMBER_ME,
    LANG_REGISTER_URL,
    LANG_LOGIN_BUTTON_TEXT,
    
    LANG_USER_NOT_FOUND,
    LANG_NO_USERS_ONLINE,
    LANG_NO_USERS_AVAILABLE_ONLINE
    
};

typedef NS_ENUM(NSInteger, LANGUAGE) {
    Default,
    Afrikaans,
    Albanian,
    Arabic,
    Belarusian,
    Bulgarian,
    Catalan,
    Chinese_Simpl,
    Chinese_Trad,
    Croatian,
    Czech,
    Danish,
    Dutch,
    English,
    Estonian,
    Filipino,
    Finnish,
    French,
    Galician,
    German,
    Greek,
    Haitian_Creole,
    Hebrew,
    Hindi,
    Hungarian,
    Icelandic,
    Indonesian,
    Irish,
    Italian,
    Japanese,
    Korean,
    Latvian,
    Lithuanian,
    Macedonian,
    Malay,
    Maltese,
    Norwegian,
    Persian,
    Polish,
    Portuguese,
    Romanian,
    Russian,
    Serbian,
    Slovak,
    Slovenian,
    Spanish,
    Swahili,
    Swedish,
    Thai,
    Turkish,
    Ukrainian,
    Vietnamese,
    Welsh,
    Yiddish
};



@interface CometChat : NSObject


/* Initialization method */
- (id)init; //__attribute__((unavailable("Must use initWithAPIKey: instead.")));
- (id)initWithAPIKey:(NSString *)apiKey;

/* Check CometChat URL is valid or Not */
- (void)checkCometChatUrl:(NSString *)siteURL
                  success:(void (^)(NSDictionary *response))success
                  failure:(void (^)(NSError *error))failure;


/* Initilaize CometChat */
- (void)initializeCometChat:(NSString *)siteUrl
                 licenseKey:(NSString *)licenseKey
                     apikey:(NSString *)apikey
            isCometOnDemand:(BOOL )isCometOnDemand
                    success:(void (^)(NSDictionary *response))success
                    failure:(void (^)(NSError *error))failure;

/* Login Callbacks */
- (void)login:(NSString *)userID
      success:(void (^)(NSDictionary *response))success
      failure:(void (^)(NSError *error))failure;

- (void)login:(NSString *)username
     password:(NSString *)password
      success:(void (^)(NSDictionary *response))success
      failure:(void (^)(NSError *error))failure;

- (void)guestLogin:(NSString *)guestName
           success:(void (^)(NSDictionary *response))success
           failure:(void (^)(NSError *error))failure;


/* Subscribe to OneOnOne chat - Group chat - recent list - bots list:
 with BOOL mode indicating whether to strip html tags or not */
- (void)SubscribeCallbacks:(BOOL)mode
          onMyInfoReceived:(void (^)(NSDictionary *response))myInfo
        onUserListReceived:(void (^)(NSDictionary *response))onlineUsers
         onMessageReceived:(void (^)(NSDictionary *response))oneOnOneMessage
   onAVChatMessageReceived:(void (^)(NSDictionary *response))oneOnOneAVChatMessage
   onActionMessageReceived:(void (^)(NSDictionary *response))oneOnOneactionMessage
       onGroupListReceived:(void(^)(NSDictionary *response))groupList
    onGroupMessageReceived:(void(^)(NSDictionary *response))groupMeessage
onGroupAVChatMessageReceived:(void(^)(NSDictionary *response))groupAVChatMessage
onGroupActionMessageReceived:(void(^)(NSDictionary *response))groupActionMessage
  onRecentChatListReceived:(void (^)(NSDictionary *response))recentList
    onAnnouncementReceived:(void (^)(NSDictionary *response))announcement
                   failure:(void (^)(NSError *))failure;


/*  Send message to User with local message ID*/
- (void)sendMessage:(NSString *)message
               toId:(NSString *)toId
            localID:(NSString *)localID
            isGroup:(BOOL)isGroup
            success:(void(^)(NSDictionary *response))response
            failure:(void(^)(NSError *error))failure;

/*  Send image to user with the given image path isGroup*/
- (void)sendImageWithPath:(NSString *)imagePath
                     toId:(NSString *)toId
                  localID:(NSString *)localID
                  isGroup:(BOOL)isGroup
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/*  Send image to user with given image data */
- (void)sendImageWithData:(NSData *)imageData
                     toId:(NSString *)toId
                  localID:(NSString *)localID
                  isGroup:(BOOL)isGroup
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/*   Send audio to user with the given audio path */
- (void)sendAudioWithPath:(NSString *)audioPath
                     toID:(NSString *)toID
                  localID:(NSString *)localID
                  isGroup:(BOOL)isGroup
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/*  Send audio to user with given image data */
- (void)sendAudioWithData:(NSData *)audioData
                     toID:(NSString *)toID
                  localID:(NSString *)localID
                  isGroup:(BOOL)isGroup
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/*   Send file to user with the given image path */
- (void)sendFile:(NSString *)filePath
                     toId:(NSString *)toId
                  localId:(NSString *)localId
                  isGroup:(BOOL)isGroup
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/*   Send image to user with the given image path isGroup*/
- (void)sendVideoWithPath:(NSString *)videoPath
                     toId:(NSString *)toId
                  localID:(NSString *)localID
                  isGroup:(BOOL)isGroup
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;


/*   Send image to user with the given video data isGroup*/
- (void)sendVideoWithURL:(NSURL *)videoFile
                    toId:(NSString *)toId
                 localID:(NSString *)localID
                 isGroup:(BOOL)isGroup
                 success:(void(^)(NSDictionary *response))response
                 failure:(void(^)(NSError *error))failure;

/* Send Stickers in Contacts/Groups */
- (void)sendStickers:(NSString *)stickerName
                toId:(NSString *)toId
             localID:(NSString *)localID
             isGroup:(BOOL)isGroup
             success:(void(^)(NSDictionary *response))response
             failure:(void(^)(NSError *error))failure;

/* Broadcast message to users of given userIDs*/
- (void)broadcastMessage:(NSString *)message
                 toUsers:(NSArray *)usersId
                 success:(void(^)(NSDictionary *response))response
                 failure:(void(^)(NSError *error))failure;

/* Send Whiteboard Request */
- (void)sendWhiteBoardRequest:(NSString *)toID
                      isGroup:(BOOL)isGroup
                      success:(void(^)(NSDictionary *response))response
                      failure:(void(^)(NSError *error))failure;
/* Send Writeboard Request */
- (void)sendWriteBoardRequest:(NSString *)toID
                      isGroup:(BOOL)isGroup
                      success:(void(^)(NSDictionary *response))response
                      failure:(void(^)(NSError *error))failure;

/* Send Writeboard Ack */
- (void)sendWriteBoardAck:(NSString *)buddyID;

/* Change status of logged-in user. Refer to STATUS_OPTIONS enum in CometChat.h file for status values. */
- (void)setStatus:(NSInteger)statusOption
             success:(void(^)(NSDictionary *response))response
             failure:(void(^)(NSError *error))failure;

/* Change status message of logged-in user. */
- (void)setStatusMessage:(NSString *)statusMessage
                    success:(void(^)(NSDictionary *response))response
                    failure:(void(^)(NSError *error))failure;

/* Get Plugin Info if user is logged-in */
- (void)getPluginInfo:(void (^)(NSDictionary *response))response
              failure:(void (^)(NSError *error))failure;

/* Get user information for userID */
- (void)getUserInfo:(NSString *)userId
            success:(void(^)(NSDictionary *response))response
            failure:(void(^)(NSError *error))failure;

/* Get online users of the site */
- (void)getOnlineUsers:(void(^)(NSDictionary *response))response
               failure:(void(^)(NSError *error))failure;

/* Get Single Player Game URL */
- (void)getSinglePlayerGamesUrl:(void (^)(NSDictionary *))response
                        failure:(void (^)(NSError *))failure;

/* Get all announcements of the site */
- (void)getAllAnnouncements:(void (^)(NSDictionary *response))response
                    failure:(void (^)(NSError *error))failure;

/* Block User */
- (void)blockUser:(NSString *)userID
          success:(void(^)(NSDictionary *response))success
          failure:(void(^)(NSError *error))failure;

/* Unblock User */
- (void)unblockUser:(NSString *)userID
            success:(void(^)(NSDictionary *response))success
            failure:(void(^)(NSError *error))failure;

/* Delete User's History */
- (void)deleteUserHistory:(NSString *)userID
                  success:(void(^)(NSDictionary *response))success
                  failure:(void(^)(NSError *error))failure;

/* Get List of blocked users */
- (void)getBlockedUserList:(void(^)(NSDictionary *response))success
                            failure:(void(^)(NSError *error))failure;

/* Get chat history of user specified by userID */
- (void)getChatHistory:(NSString *)userId
             messageId:(NSString *)messageId
               success:(void(^)(NSDictionary *response))success
               failure:(void(^)(NSError *error))failure;

/* Translate OneOnOne and Chatroom Messages */
- (void)setTranslateLanguage:(NSInteger)language
                     success:(void(^)(NSDictionary *response))response
                     failure:(void(^)(NSError *error))failure;

- (void)checkforOfflineMessage:(void(^)(NSDictionary *response))response;

/* Check whether the user is logged-in. Class Method */
+ (BOOL)isLoggedIn;

/* Check whether the user is logged-in */
- (BOOL)isUserLoggedIn;

/* Unsubscribe from OneOnChat */
- (void)unsubscribe;

/* Logout */
- (void)logout:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/* Returns the SDK version. Only for Internal */
+ (NSString *)getSDKVersion;

/*Returns the CometChat Path */
+ (NSString *)getCometChatURL;

/* Set Development Mode. Only for Internal */
+ (void)setDevelopmentMode:(BOOL)flag;

/* isTyping */
- (void)isTyping:(BOOL)istyping
         channel:(NSString *)channel
         failure:(void(^)(NSError *error))failure;

/* Deliver - Read Receipt Feature */
- (void)sendDeliverdReceipt:(NSString *)msgID
                    channel:(NSString *)channel
                    failure:(void(^)(NSError *error))failure;

- (void)sendReadReceipt:(NSString *)msgID
                channel:(NSString *)channel
                failure:(void(^)(NSError *error))failure;



/* Self Hosted Login with only Basedata */
- (void)loginWithBasedata:(NSString *)userID
                  success:(void (^)(NSDictionary *))success
                  failure:(void (^)(NSError *))failure;

/* Join chatroom: specify groupID and password */
- (void)joinGroup:(NSString *)name
          groupID:(NSString *)groupID
         password:(NSString *)password
          success:(void(^)(NSDictionary *response))success
          failure:(void(^)(NSError *error))failure;

/* color your text */
- (void)colorYourText:(NSString *)color groupid:(NSString *)groupid
          success:(void(^)(NSDictionary *response))success
          failure:(void(^)(NSError *error))failure;

/* Report Conversation */
- (void)reportConversation:(NSString *)userID reason:(NSString *)reason
              success:(void(^)(NSDictionary *response))success
              failure:(void(^)(NSError *error))failure;


/* Leave currently joined Group */
/* parameters to be send in the dictionary as KEY are as follows: groupid */
- (void)leaveGroup:(NSDictionary *)leaveGroupParameter
           success:(void (^)(NSDictionary *))success
           failure:(void(^)(NSError *error))failure;

/* Get Group list from server */
- (void)getAllGroups:(void(^)(NSDictionary *groupList))response
                failure:(void(^)(NSError *error))failure;


/*Get Chatroom users list */
/* parameters to be send in the dictionary as KEY are as follows: currentroom */
- (void)getGroupMembers:(NSDictionary *)groupMembersParameter
                   success:(void (^)(NSDictionary *))success
                   failure:(void (^)(NSError *))failure;


/* Create Group having specified type (INT 0 - public , 1 - password , 2 - invite only ) , name and password */
- (void)createGroup:(NSString *)name
               type:(NSInteger)type
           password:(NSString *)password
            success:(void(^)(NSDictionary *response))success
            failure:(void(^)(NSError *error))failure;

/* Rename Chatroom passing chatroomID */
- (void)renameGroup:(NSString *)groupId
          groupName:(NSString *)groupName
            success:(void(^)(NSDictionary *response))response
            failure:(void(^)(NSError *error))error;

- (void)getAnnouncementURL:(void(^)(NSDictionary *response))response;

/* Handwrite Message URL */
- (void)getHandwriteMessageURL:(NSString *)toID
                       isGroup:(BOOL)isGroup
                       success:(void(^)(NSDictionary *response))response
                       failure:(void(^)(NSError *error))failure;


/* Check if Feature you are using is Enabled from CometChat Admin panel */
- (BOOL)isFeatureEnabled:(NSInteger)featurename;

/* Get Colors */
- (NSString *)getColors:(NSInteger)colorKey;

/* Get Language */
- (NSString *)getLanguageforKey:(NSInteger)languagekey;

/* Get chatroom history for chatroom */
- (void)getGroupChatHistory:(NSString *)chatroomID
                     messageID:(NSString *)messageID
                       success:(void(^)(NSDictionary *response))success
                       failure:(void(^)(NSError *error))failure;

/* Get currently joined group id */
- (void)getCurrentJoinGroup:(void(^)(NSDictionary *response))success
                   failure:(void(^)(NSError *error))failure;

/* Delete Group passing chatroomID */
- (void)deleteGroup:(NSString *)groupId
               success:(void(^)(NSDictionary *response))response
               failure:(void(^)(NSError *error))error;

/* Invite users to specified chatroom */
- (void)inviteUser:(NSArray *)usersArray
          groupName:(NSString *)groupName
            groupId:(NSString *)groupId
            success:(void(^)(NSDictionary *))success
            failure:(void(^)(NSError *error))failure;

/* Below Methods can be used to Switch/toggle Audio or video chat */

/* Switch audio route to Ear-piece/Speaker */
- (void)switchSpeakers;

/* Toggle Audio to ON/OFF state */
-(void)muteAudio:(BOOL)flag;

/* Switch between Front/Rear camera */
- (void)toggleCamera;

/* Toggle Video to ON/OFF state */
- (void)pauseVideo:(BOOL)flag;

/* ================>>>>> Audio/Video chat Callbacks start <<<<<================== */

/* Start AVChat with given callID and within given container */
- (void)startAVChatCall:(NSString *)callId
              container:(UIView *)container
          connectedUser:(void(^)(NSDictionary *response))response
                failure:(void(^)(NSError *error))failure;

/* Send AVChat request to user */
- (void)sendAVChatRequest:(NSString *)userId
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/* Accept AVChat request from user with give callID */
- (void)acceptAVChatRequest:(NSString *)userId
                     callId:(NSString *)callId
                    success:(void(^)(NSDictionary *response))response
                    failure:(void(^)(NSError *error))failure;

/* End AVChat call with user with give callID */
- (void)endAVChatCall:(NSString *)userId
               callId:(NSString *)callId
              success:(void(^)(NSDictionary *response))response
              failure:(void(^)(NSError *error))failure;


/* Cancel AVChat call request with user */
- (void)cancelAVChatRequest:(NSString *)userId
                    success:(void(^)(NSDictionary *response))response
                    failure:(void(^)(NSError *error))failure;

/* Send Busy call request to user */
- (void)sendBusyAVChatTone:(NSString *)userId
             success:(void(^)(NSDictionary *response))response
             failure:(void(^)(NSError *error))failure;

/* Reject AVChat call request */
- (void)rejectAVChatRequest:(NSString *)userId
                     callId:(NSString *)callId
                    success:(void(^)(NSDictionary *response))response
                    failure:(void(^)(NSError *error))failure;

/* Send No answer call request */
- (void)sendNoAnswerAVChatCall:(NSString *)userId
                        callId:(NSString *)callId
                       success:(void(^)(NSDictionary *response))response
                       failure:(void(^)(NSError *error))failure;

/* ================>>>>> Audio/Video chat Callbacks End <<<<<================== */

/* ================>>>>> Audio chat Callbacks Start <<<<<================== */

/* Start AudioChat with given callID and within given container */
- (void)startAudioChatCall:(NSString *)callId
             containerView:(UIView *)container
             isGroup:(BOOL)isGroup
             connectedUser:(void(^)(NSDictionary *response))response
                   failure:(void(^)(NSError *error))failure;


/* Send AudioChat request to user */
- (void)sendAudioChatRequest:(NSString *)userId
                           isGroup:(BOOL)isGroup
                           success:(void(^)(NSDictionary *response))response
                           failure:(void(^)(NSError *error))failure;

/* Accept AudioChat request from user with give callID */
- (void)acceptAudioChatRequest:(NSString *)userId
                        callId:(NSString *)callId
                        success:(void(^)(NSDictionary *response))response
                        failure:(void(^)(NSError *error))failure;

/* End AudioChat call with user with give callID */
- (void)endAudioChatCall:(NSString *)userId
                      callId:(NSString *)callId
                     success:(void(^)(NSDictionary *response))response
                     failure:(void(^)(NSError *error))failure;

/* Cancel AudioChat call request with user */
- (void)cancelAudioChatRequest:(NSString *)userId
                       success:(void(^)(NSDictionary *response))response
                       failure:(void(^)(NSError *error))failure;

/* Send Busy call request to user */
- (void)sendBusyAudioTone:(NSString *)userId
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/* Reject AudioChat call request with give callID */
- (void)rejectAudioChatRequest:(NSString *)userId
                        callId:(NSString *)callId
                       success:(void(^)(NSDictionary *response))response
                       failure:(void(^)(NSError *error))failure;

/* Send No answer call request with give callID */
- (void)sendNoAnswerAudioChatCall:(NSString *)userId
                           callId:(NSString *)callId
                          success:(void(^)(NSDictionary *response))response
                        failure:(void(^)(NSError *error))failure;
/* ================>>>>> Audio chat Callbacks End <<<<<================== */

/* ================>>>>> Audio/Video Broadcast Callbacks Start <<<<<================== */
- (void)startAVBroadCast:(NSString *)callId
          iamBroadcaster:(BOOL)iamBroadcaster
           containerView:(UIView *)container
           connectedUser:(void (^)(NSDictionary *response))response
                 failure:(void (^)(NSError *error))failure;

- (void)sendAVBroadcastRequest:(NSString *)userId
                       isGroup:(BOOL)isGroup
                       success:(void (^)(NSDictionary *response))response
                       failure:(void (^)(NSError *error))failure;

- (void)endAVBroadcastCall:(BOOL)iamBroadcaster
                    userId:(NSString *)userId
                    callId:(NSString *)callId
                   isGroup:(BOOL)isGroup
                   success:(void (^)(NSDictionary *response))response
                   failure:(void (^)(NSError *error))failure;

- (void)acceptAVBroadcastCall:(NSString *)userId
                       callId:(NSString *)callId
                      isGroup:(BOOL)isGroup
                      success:(void (^)(NSDictionary *response))response
                      failure:(void (^)(NSError *error))failure;

- (void)inviteUsersInBroadcast:(NSArray *)usersId
                        callId:(NSString *)callId
                       success:(void (^)(NSDictionary *response))response
                       failure:(void (^)(NSError *error))failure;

/* ================>>>>> Audio/Video Broadcast Callbacks Start <<<<<================== */

/* ================>>>>> Group Audio/Video Chat Callbacks Start <<<<<================== */
/* Start AudioVideo group conference in the currently joined chatroom */
- (void)startConference:(NSString *)callId
          containerView:(UIView *)container
                success:(void(^)(NSDictionary *response))success
                failure:(void (^)(NSError *error))failure;

/* Send AudioVideo group conference request in the currently joined chatroom */
- (void)sendConferenceRequest:(NSString *)groupId
                      success:(void (^)(NSDictionary *))success
                      failure:(void (^)(NSError *))failure;

/* Join AudioVideo group conference in the currently joined chatroom */
- (void)joinConference:(NSString *)callId
               success:(void (^)(NSDictionary *response))success
               failure:(void (^)(NSError *error))failure;

/* End/Leave AudioVideo group conference in the currently joined chatroom */
- (void)endConference:(NSString *)groupId
               callId:(NSString *)callId
              success:(void (^)(NSDictionary *response))success
              failure:(void (^)(NSError *error))failure;

/* ================>>>>> Group Audio/Video Chat Callbacks End <<<<<================== */


@end
